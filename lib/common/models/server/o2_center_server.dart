import 'package:o2oa_all_platform/common/values/o2.dart';

/// o2oa 服务器信息
class CenterServerInfo {
  ModuleAssembles? assembles;
  WebServerInfo? webServer;
  String? tokenName; // tokenName是可修改的 x-token是默认值
  MockConfig? mockConfig; // put delete 代理
  bool? standalone; // 是否统一端口服务器

  CenterServerInfo.fromJson(Map<String, dynamic> jsonMap) {
    if (jsonMap['webServer'] != null) {
      webServer = WebServerInfo.fromJson(jsonMap['webServer']);
    }
    if (jsonMap['assembles'] != null) {
      assembles = ModuleAssembles.fromJson(jsonMap['assembles']);
    }
    if (jsonMap['tokenName'] != null) {
      tokenName = jsonMap['tokenName'];
    } else {
      tokenName = O2.defaultTokenName;
    }
    if (jsonMap['mockConfig'] != null) {
      mockConfig = MockConfig.fromJson(jsonMap['mockConfig']);
    }
    standalone = jsonMap['standalone'];
  }
  Map<String, dynamic> toJson() => {
        "webServer": webServer?.toJson(),
        "assembles": assembles?.toJson(),
        "tokenName": tokenName,
        "mockConfig": mockConfig?.toJson(),
        "standalone": standalone
      };
}

///
///"mockConfig": {
//      "mock": {
//        "x_processplatform_assemble_surface": {
//          "put": {
//            "to": "post",
//            "append": "mockputtopost"
//          },
//          "delete": {
//            "to": "get",
//            "append": "mockdeletetoget"
//          }
//        },
//        "x_cms_assemble_control": {
//          "put": {
//            "to": "post",
//            "append": "mockputtopost"
//          },
//          "delete": {
//            "to": "get",
//            "append": "mockdeletetoget"
//          }
//        },
//        "x_query_assemble_surface": {
//          "put": {
//            "to": "post",
//            "append": "mockputtopost"
//          },
//          "delete": {
//            "to": "get",
//            "append": "mockdeletetoget"
//          }
//        },
//        "x_organization_assemble_authentication": {
//          "put": {
//            "to": "post",
//            "append": "mockputtopost"
//          },
//          "delete": {
//            "to": "get",
//            "append": "mockdeletetoget"
//          }
//        },
//        "x_organization_assemble_personal": {
//          "put": {
//            "to": "post",
//            "append": "mockputtopost"
//          },
//          "delete": {
//            "to": "get",
//            "append": "mockdeletetoget"
//          }
//        }
//      }
//    }
///
class MockConfig {
  Map<String, dynamic>? mock;

  MockConfig.fromJson(Map<String, dynamic> jsonMap) {
    mock = jsonMap['mock'];
  }
  Map<String, dynamic> toJson() => {"mock": mock};
}

class MockMode {
  String? to; // 转化后的方法
  String? append; // 需要添加的url
  MockMode.fromJson(Map<String, dynamic> jsonMap) {
    to = jsonMap['to'];
    append = jsonMap['append'];
  }
  Map<String, dynamic> toJson() => {"to": to, "append": append};
}
/// web 服务模块
class WebServerInfo {
  String? host;
  int? port;
  String? proxyHost;
  int? proxyPort;
  String? username;
  String? password;
  int? order;
  String? name;
  bool? sslEnable;

  WebServerInfo.fromJson(Map<String, dynamic> jsonMap) {
    host = jsonMap['host'];
    port = jsonMap['port'];
    proxyHost = jsonMap['proxyHost'];
    proxyPort = jsonMap['proxyPort'];
    username = jsonMap['username'];
    password = jsonMap['password'];
    order = jsonMap['order'];
    name = jsonMap['name'];
    sslEnable = jsonMap['sslEnable'] ?? false;
  }

  Map<String, dynamic> toJson() => {
        "host": host,
        "port": port,
        "proxyHost": proxyHost,
        "proxyPort": proxyPort,
        "username": username,
        "password": password,
        "order": order,
        "name": name,
        "sslEnable": sslEnable
      };
}
/// 应用服务模块
class ModuleServerInfo {
  String? host;
  int? port;
  String? proxyHost;
  int? proxyPort;
  String? context;

  ModuleServerInfo.fromJson(Map<String, dynamic> jsonMap) {
    if (jsonMap['host'] != null) {
      host = jsonMap['host'];
    }
    if (jsonMap['port'] != null) {
      port = jsonMap['port'];
    }

    if (jsonMap['proxyHost'] != null) {
      proxyHost = jsonMap['proxyHost'];
    }

    if (jsonMap['proxyPort'] != null) {
      proxyPort = jsonMap['proxyPort'];
    }
    if (jsonMap['context'] != null) {
      context = jsonMap['context'];
    }
  }

  Map<String, dynamic> toJson() => {
        "host": host,
        "port": port,
        "proxyHost": proxyHost,
        "proxyPort": proxyPort,
        "context": context
      };
}

class ModuleAssembles {
  ModuleServerInfo? x_file_assemble_control;
  ModuleServerInfo? x_bbs_assemble_control;
  ModuleServerInfo? x_hotpic_assemble_control;
  ModuleServerInfo? x_attendance_assemble_control;
  ModuleServerInfo? x_meeting_assemble_control;
  ModuleServerInfo? x_processplatform_service_processing;
  ModuleServerInfo? x_cms_assemble_control;
  ModuleServerInfo? x_organization_assemble_control;
  ModuleServerInfo? x_organization_assemble_custom;
  ModuleServerInfo? x_processplatform_assemble_surface;
  ModuleServerInfo? x_processplatform_assemble_bam;
  ModuleServerInfo? x_organization_assemble_express;
  ModuleServerInfo? x_organization_assemble_personal;
  ModuleServerInfo? x_component_assemble_control;
  ModuleServerInfo? x_processplatform_assemble_designer;
  ModuleServerInfo? x_organization_assemble_authentication;
  ModuleServerInfo? x_portal_assemble_surface;
  ModuleServerInfo? x_calendar_assemble_control; //日程
  ModuleServerInfo? x_mind_assemble_control; //脑图
  ModuleServerInfo? x_teamwork_assemble_control; //TeamWork
  ModuleServerInfo? x_wcrm_assemble_control; // crm
  ModuleServerInfo? x_jpush_assemble_control; //极光推送服务
  ModuleServerInfo? x_message_assemble_communicate; //通信模块 消息收发 还有websocket
  ModuleServerInfo? x_organizationPermission; //  custom模块 通讯录 需要到应用市场下载安装
  ModuleServerInfo? x_pan_assemble_control; //  custom模块 V3版本云盘服务 需要到应用市场下载安装
  ModuleServerInfo? x_query_assemble_surface; 
  ModuleServerInfo? x_app_packaging_client_assemble_control; 

  ModuleAssembles.fromJson(Map<String, dynamic> jsonMap) {
    if (jsonMap['x_jpush_assemble_control'] != null) {
      x_jpush_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_jpush_assemble_control']);
    }
    if (jsonMap['x_message_assemble_communicate'] != null) {
      x_message_assemble_communicate =
          ModuleServerInfo.fromJson(jsonMap['x_message_assemble_communicate']);
    }
    if (jsonMap['x_pan_assemble_control'] != null) {
      x_pan_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_pan_assemble_control']);
    }
    if (jsonMap['x_organizationPermission'] != null) {
      x_organizationPermission =
          ModuleServerInfo.fromJson(jsonMap['x_organizationPermission']);
    }
    if (jsonMap['x_wcrm_assemble_control'] != null) {
      x_wcrm_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_wcrm_assemble_control']);
    }
    if (jsonMap['x_file_assemble_control'] != null) {
      x_file_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_file_assemble_control']);
    }
    if (jsonMap['x_bbs_assemble_control'] != null) {
      x_bbs_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_bbs_assemble_control']);
    }
    if (jsonMap['x_hotpic_assemble_control'] != null) {
      x_hotpic_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_hotpic_assemble_control']);
    }
    if (jsonMap['x_attendance_assemble_control'] != null) {
      x_attendance_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_attendance_assemble_control']);
    }
    if (jsonMap['x_meeting_assemble_control'] != null) {
      x_meeting_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_meeting_assemble_control']);
    }
    if (jsonMap['x_cms_assemble_control'] != null) {
      x_cms_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_cms_assemble_control']);
    }
    if (jsonMap['x_organization_assemble_control'] != null) {
      x_organization_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_organization_assemble_control']);
    }
    if (jsonMap['x_organization_assemble_custom'] != null) {
      x_organization_assemble_custom =
          ModuleServerInfo.fromJson(jsonMap['x_organization_assemble_custom']);
    }
    if (jsonMap['x_processplatform_assemble_surface'] != null) {
      x_processplatform_assemble_surface = ModuleServerInfo.fromJson(
          jsonMap['x_processplatform_assemble_surface']);
    }
    if (jsonMap['x_organization_assemble_express'] != null) {
      x_organization_assemble_express =
          ModuleServerInfo.fromJson(jsonMap['x_organization_assemble_express']);
    }
    if (jsonMap['x_organization_assemble_personal'] != null) {
      x_organization_assemble_personal = ModuleServerInfo.fromJson(
          jsonMap['x_organization_assemble_personal']);
    }
    if (jsonMap['x_component_assemble_control'] != null) {
      x_component_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_component_assemble_control']);
    }
    if (jsonMap['x_processplatform_service_processing'] != null) {
      x_processplatform_service_processing = ModuleServerInfo.fromJson(
          jsonMap['x_processplatform_service_processing']);
    }
    if (jsonMap['x_processplatform_assemble_designer'] != null) {
      x_processplatform_assemble_designer = ModuleServerInfo.fromJson(
          jsonMap['x_processplatform_assemble_designer']);
    }
    if (jsonMap['x_processplatform_assemble_bam'] != null) {
      x_processplatform_assemble_bam =
          ModuleServerInfo.fromJson(jsonMap['x_processplatform_assemble_bam']);
    }
    if (jsonMap['x_organization_assemble_authentication'] != null) {
      x_organization_assemble_authentication = ModuleServerInfo.fromJson(
          jsonMap['x_organization_assemble_authentication']);
    }
    if (jsonMap['x_portal_assemble_surface'] != null) {
      x_portal_assemble_surface =
          ModuleServerInfo.fromJson(jsonMap['x_portal_assemble_surface']);
    }
    if (jsonMap['x_calendar_assemble_control'] != null) {
      x_calendar_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_calendar_assemble_control']);
    }
    if (jsonMap['x_mind_assemble_control'] != null) {
      x_mind_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_mind_assemble_control']);
    }
    if (jsonMap['x_teamwork_assemble_control'] != null) {
      x_teamwork_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_teamwork_assemble_control']);
    } 
    if (jsonMap['x_query_assemble_surface'] != null) {
      x_query_assemble_surface =
          ModuleServerInfo.fromJson(jsonMap['x_query_assemble_surface']);
    }
    if (jsonMap['x_app_packaging_client_assemble_control'] != null) {
      x_app_packaging_client_assemble_control =
          ModuleServerInfo.fromJson(jsonMap['x_app_packaging_client_assemble_control']);
    }
  }

  Map<String, dynamic> toJson() => {
        "x_file_assemble_control": x_file_assemble_control?.toJson(),
        "x_bbs_assemble_control": x_bbs_assemble_control?.toJson(),
        "x_hotpic_assemble_control": x_hotpic_assemble_control?.toJson(),
        "x_attendance_assemble_control":
            x_attendance_assemble_control?.toJson(),
        "x_meeting_assemble_control": x_meeting_assemble_control?.toJson(),
        "x_processplatform_assemble_bam":
            x_processplatform_assemble_bam?.toJson(),
        "x_cms_assemble_control": x_cms_assemble_control?.toJson(),
        "x_organization_assemble_control":
            x_organization_assemble_control?.toJson(),
        "x_organization_assemble_custom":
            x_organization_assemble_custom?.toJson(),
        "x_processplatform_assemble_surface":
            x_processplatform_assemble_surface?.toJson(),
        "x_processplatform_service_processing":
            x_processplatform_service_processing?.toJson(),
        "x_organization_assemble_express":
            x_organization_assemble_express?.toJson(),
        "x_organization_assemble_personal":
            x_organization_assemble_personal?.toJson(),
        "x_component_assemble_control": x_component_assemble_control?.toJson(),
        "x_processplatform_assemble_designer":
            x_processplatform_assemble_designer?.toJson(),
        "x_organization_assemble_authentication":
            x_organization_assemble_authentication?.toJson(),
        "x_portal_assemble_surface": x_portal_assemble_surface?.toJson(),
        "x_calendar_assemble_control": x_calendar_assemble_control?.toJson(),
        "x_mind_assemble_control": x_mind_assemble_control?.toJson(),
        "x_teamwork_assemble_control": x_teamwork_assemble_control?.toJson(),
        "x_wcrm_assemble_control": x_wcrm_assemble_control?.toJson(),
        "x_jpush_assemble_control": x_jpush_assemble_control?.toJson(),
        "x_message_assemble_communicate":
            x_message_assemble_communicate?.toJson(),
        "x_organizationPermission": x_organizationPermission?.toJson(),
        "x_pan_assemble_control": x_pan_assemble_control?.toJson(),
        "x_query_assemble_surface": x_query_assemble_surface?.toJson(),
        "x_app_packaging_client_assemble_control": x_app_packaging_client_assemble_control?.toJson(),
      };
}
