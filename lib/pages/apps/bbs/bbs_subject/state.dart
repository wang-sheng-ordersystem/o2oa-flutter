import 'package:get/get.dart';

class BbsSubjectState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

    // 访问地址
  final _url = "".obs;
  set url(value) => _url.value = value;
  String get url => _url.value;


  // 是否能回帖
  final _canWriteComment = false.obs;
  set canWriteComment(value) => _canWriteComment.value = value;
  get canWriteComment => _canWriteComment.value;

  // 是否能删除帖子
  final _canDelete = false.obs;
  set canDelete(value) => _canDelete.value = value;
  get canDelete => _canDelete.value;
  
}
