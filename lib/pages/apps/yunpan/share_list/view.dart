import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/int_extension.dart';

import '../../../../common/models/index.dart';
import '../../../../common/values/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class ShareListPage extends GetView<ShareListController> {
  const ShareListPage({Key? key}) : super(key: key);

  Widget tabBar(BuildContext context) {
    return Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () =>
                  controller.changeType(CloudDiskShareListType.shareTome),
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  child: Text(
                    'cloud_disk_share_type_to_me'.tr,
                    style: TextStyle(
                        color: controller.state.shareType.value ==
                                CloudDiskShareListType.shareTome
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.secondary,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold),
                  )),
            ),
            const SizedBox(width: 15),
            GestureDetector(
              onTap: () =>
                  controller.changeType(CloudDiskShareListType.myShare),
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  child: Text(
                    'cloud_disk_share_type_my_share'.tr,
                    style: TextStyle(
                        color: controller.state.shareType.value ==
                                CloudDiskShareListType.myShare
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.secondary,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold),
                  )),
            )
          ],
        ));
  }

  Widget shareFileListView(BuildContext context) {
    return Obx(() => Container(
        color: Theme.of(context).colorScheme.background,
        child: ListView.builder(
          itemCount: controller.state.shareList.length,
          itemBuilder: ((context, index) {
            var item = controller.state.shareList[index];
            var icon = 'icon_folder.png'; // 文件夹
            var updateTime = '';
            if (item.fileType == 'folder') {
              updateTime = item.updateTime ?? '';
            } else {
              icon = FileIcon.getFileIconAssetsNameByExtension(
                  item.extension ?? '');
              String len = (item.length ?? 0).friendlyFileLength();
              updateTime = '${item.updateTime ?? ''} $len';
            }
            return ListTile(
              onTap: () => controller.onItemClick(item),
              leading: AssetsImageView(icon, width: 40, height: 40),
              title: Text(item.name ?? ''),
              subtitle: Text(updateTime),
            );
          }),
        )));
  }

  // 主视图
  Widget _buildView(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 10),
        tabBar(context),
        const SizedBox(height: 10),
        Expanded(flex: 1, child: shareFileListView(context))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ShareListController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("cloud_disk_share".tr)),
          body: SafeArea(
              child: Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: _buildView(context),
          )),
        );
      },
    );
  }
}
