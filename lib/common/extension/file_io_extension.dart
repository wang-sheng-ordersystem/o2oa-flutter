

import 'dart:io';

extension FileIoExt on File {

  /// 文件名称
  String filename() {
    if (existsSync()) {
      var index = path.lastIndexOf(Platform.pathSeparator);
      return path.substring(index + 1, path.length);
    }
    return '';
  }
}