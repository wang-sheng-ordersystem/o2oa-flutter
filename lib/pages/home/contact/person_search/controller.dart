import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/api/index.dart';
import '../../../../common/models/index.dart';
import '../../../../common/utils/index.dart';
import '../person/index.dart';
import 'index.dart';

class PersonSearchController extends GetxController {
  PersonSearchController();

  final state = PersonSearchState();

 
  final TextEditingController searchController = TextEditingController();

  final FocusNode searchNode = FocusNode();

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  ///
  /// 搜索
  /// 
  void onSearch(String key) async {
    OLogger.d("搜索关键字：$key");
    state.personList.clear();
    if (key.isNotEmpty) {
      var list = await OrganizationControlService.to.personSearch(key);
      if (list != null && list.isNotEmpty) {
        List<O2Person> newlist = [];
        for (var element in list) {
          if (!O2ContactPermissionManager.instance
              .isExcludePerson(element.distinguishedName ?? '')) {
            newlist.add(element);
          }
        }
        state.personList.addAll(newlist);
      }
    }
  }

   ///
  /// 打开个人信息
  /// 
  void clickEnterPerson(O2Person person) {
    PersonPage.openPersonInfo(person);
  }

}
