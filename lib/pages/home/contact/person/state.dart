import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class PersonState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  Rx<O2Person?> person = Rx<O2Person?>(null);

  final _departmentNames = "".obs;
  set departmentNames(String value) => _departmentNames.value = value;
  String get departmentNames => _departmentNames.value;

  final _superiorName = "".obs;
  set superiorName(String value) => _superiorName.value = value;
  String get superiorName => _superiorName.value;

  final RxList<O2PersonAttribute> personAttributeList = <O2PersonAttribute>[].obs;
}
