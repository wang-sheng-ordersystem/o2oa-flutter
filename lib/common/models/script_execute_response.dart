class ScriptExecuteResponse {
  dynamic value;

  ScriptExecuteResponse({this.value});

  factory ScriptExecuteResponse.fromJson(Map<String, dynamic>? map) =>
      ScriptExecuteResponse(value: map == null ? null : map['value']);
}

/// 脚本返回对象 启动页 推广页对象
class SplashPromotionPageScriptResponse {
  static const daily = 'daily';
  static const always = 'always';
  List<String>? images;
  String? frequency; // 展现推广页频率：默认只展现一次； 如果 值 = 'daily'，就是每天展现一次；值 ='always'，就是每次打开 app 都展现

  SplashPromotionPageScriptResponse({this.images, this.frequency});

  factory SplashPromotionPageScriptResponse.fromJson(
          Map<String, dynamic>? map) =>
      SplashPromotionPageScriptResponse(
          images: map != null && map['images'] != null
              ? List<String>.from(map['images'].map((e) => e))
              : null,
          frequency: map != null && map['frequency'] != null ?  map['frequency'] : null
          );
}
