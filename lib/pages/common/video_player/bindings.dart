import 'package:get/get.dart';

import 'controller.dart';

class VideoPlayerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VideoPlayerPageController>(() => VideoPlayerPageController());
  }
}
