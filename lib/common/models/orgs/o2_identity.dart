//{
//      "id": "6c6be218-c9ad-4eca-bbee-65db740277b1",
//      "pinyin": "chenhongjun",
//      "pinyinInitial": "chj",
//      "description": "",
//      "name": "陈红军",
//      "unique": "6c6be218-c9ad-4eca-bbee-65db740277b1",
//      "distinguishedName": "陈红军@6c6be218-c9ad-4eca-bbee-65db740277b1@I",
//      "person": "6f6d6af9-781e-4812-a6c3-a8ac2671221e",
//      "unit": "59e281d6-8cb5-4c02-82df-a5ddef9fe522",
//      "unitName": "中国联通安徽省分公司",
//      "unitLevel": 1,
//      "unitLevelName": "中国联通安徽省分公司",
//      "createTime": "2018-01-16 15:28:40",
//      "updateTime": "2018-03-28 13:44:26"
//    }

import 'o2_duty.dart';

class O2Identity {
  String? id;
  String? name;
  String? unique;
  String? distinguishedName;
  String? pinyin;
  String? pinyinInitial;
  String? description;
  String? person;
  String? unit;
  String? unitName;
  int? unitLevel;
  String? unitLevelName;
  String? createTime;
  String? updateTime;
  bool? major; // 是否主身份
  List<O2Duty>? woUnitDutyList;

  O2Identity(
      {this.id,
      this.name,
      this.unique,
      this.distinguishedName,
      this.pinyin,
      this.pinyinInitial,
      this.description,
      this.person,
      this.major,
      this.unit,
      this.unitName,
      this.unitLevel = -1,
      this.unitLevelName,
      this.woUnitDutyList,
      this.createTime,
      this.updateTime});

  @override
  bool operator ==(other) {
    return other is O2Identity && other.distinguishedName == distinguishedName;
  }

   @override
  int get hashCode => distinguishedName.hashCode;

  factory O2Identity.fromJson(Map<String, dynamic> json) {
    return O2Identity(
        id: json["id"],
        name: json["name"],
        unique: json["unique"],
        distinguishedName: json["distinguishedName"],
        pinyin: json["pinyin"],
        pinyinInitial: json["pinyinInitial"],
        description: json["description"],
        person: json["person"],
        major: json["major"],
        unit: json["unit"],
        unitName: json["unitName"],
        unitLevel: json["unitLevel"] ?? -1,
        unitLevelName: json["unitLevelName"],
        woUnitDutyList: json["woUnitDutyList"] == null ? [] : List<O2Duty>.from(json["woUnitDutyList"].map((d)=> O2Duty.fromJson(d))),
        createTime: json["createTime"],
        updateTime: json["updateTime"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "unique": unique,
      "distinguishedName": distinguishedName,
      "pinyin": pinyin,
      "major": major,
      "pinyinInitial": pinyinInitial,
      "description": description,
      "person": person,
      "unit": unit,
      "unitName": unitName,
      "unitLevel": unitLevel,
      "unitLevelName": unitLevelName,
      "woUnitDutyList": woUnitDutyList == null? null : List<dynamic>.from(woUnitDutyList!.map((e) => e.toJson())),
      "createTime": createTime,
      "updateTime": updateTime,
    };
  }
}
