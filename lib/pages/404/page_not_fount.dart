 

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';


class NotFoundPage extends StatelessWidget {
  const NotFoundPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("notfound_message".tr),
      ),
      body: ListTile(
        title: Text("notfound_back_home".tr),
        onTap: () => Get.offAllNamed(O2OARoutes.home),
        trailing: const Icon(Icons.arrow_right),
      ),
    );
  }
}