import 'package:get/get.dart';

import '../../../common/models/index.dart';

class ImState {

  // 会话列表
  final RxList<IMConversationInfo> conversationList = <IMConversationInfo>[].obs;

   // 是否 显示系统消息 设置或者数据为空都不显示
  final _showInstantMsg = false.obs;
  set showInstantMsg(bool value) => _showInstantMsg.value = value;
  bool get showInstantMsg => _showInstantMsg.value; 

   //  语音助手
  final _showSpeechAssistant = false.obs;
  set showSpeechAssistant(bool value) => _showSpeechAssistant.value = value;
  bool get showSpeechAssistant => _showSpeechAssistant.value; 

  

}
