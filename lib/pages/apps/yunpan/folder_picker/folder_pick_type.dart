enum FolderPickerType {
  person, // 个人网盘 文件夹选择
  zone // 企业网盘 某一个工作区的文件夹选择
}