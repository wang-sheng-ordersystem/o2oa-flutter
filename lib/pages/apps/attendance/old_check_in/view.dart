import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/style/index.dart';
import 'index.dart'; 

class OldCheckInPage extends GetView<OldCheckInController> {
  const OldCheckInPage({Key? key}) : super(key: key);

  // 主视图
   Widget _buildView(BuildContext context) {
    return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Padding(
            padding:
                const EdgeInsets.only(top: 20, bottom: 20, left: 15, right: 15),
            child: Card(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        // 打卡列表
                        Expanded(
                          flex: 1,
                          child: Obx(() => GridView.count(
                              crossAxisCount: 2,
                              mainAxisSpacing:
                                  10.0, //设置上下之间的间距(Axis.horizontal设置左右之间的间距)
                              crossAxisSpacing:
                                  10.0, //设置左右之间的间距(Axis.horizontal设置上下之间的间距)
                              childAspectRatio: 2,
                              children: controller.state.recordList
                                  .map((element) =>
                                      scheduleItemCheckInView(context, element))
                                  .toList())),
                        ),
                        
                        SizedBox(
                          height: 220.w,
                          child: Center(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              // 打卡按钮
                              GestureDetector(
                                  onTap: ()=> controller.clickCheckIn(),
                                  child: Obx(()=>ClipOval(
                                      child: Container(
                                    height: 180.w,
                                    width: 180.w,
                                    color: controller.state.todayNeedCheck ?
                                        Theme.of(context).colorScheme.primary
                                        :
                                        AppColor.accentColorDark,
                                    child: Center(
                                        child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text('attendance_tab_checkin'.tr,
                                            style: TextStyle(
                                                fontSize: 20.sp,
                                                color: Colors.white)),
                                        const SizedBox(height: 10),
                                        Text(
                                              controller.state.currentTime,
                                              style: TextStyle(
                                                  fontSize: 20.sp,
                                                  color: Colors.white),
                                            )
                                      ],
                                    )),
                                  )))),
                              const SizedBox(height: 10),
                              // 定位信息
                              Padding(
                                  padding:
                                      EdgeInsets.only(left: 20.w, right: 20.w),
                                  child: Obx(() => Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          controller.state
                                                  .isInCheckInPositionRange
                                              ? const Icon(Icons.done,
                                                  size: 24, color: Colors.green)
                                              : const Icon(Icons.close,
                                                  size: 24, color: Colors.red),
                                          Flexible(
                                              child: Text(
                                                  controller
                                                          .state
                                                          .currentAddress
                                                          .isEmpty
                                                      ? 'attendance_location_loading'
                                                          .tr
                                                      : controller
                                                          .state.currentAddress,
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyMedium))
                                        ],
                                      ))),
                            ],
                          )),
                        ),
                        SizedBox(height: 20.h)
                      ],
                    )))));
  }

  Widget scheduleItemCheckInView(BuildContext context, Feature info) {
    bool isRecord = false;
    var recordTime = '';
    var status = 'attendance_record_check_time'.tr;
    if (info.checkinStatus == 'attendance_record_check_time'.tr) {
      isRecord = true;
      String signTime = info.checkinTime ?? '';
      if (signTime.length > 5) {
        signTime = signTime.substring(0, 5);
      }
      recordTime = '$status $signTime';
    }
    
    return GestureDetector(
      onTap: () => controller.clickUpdateRecord(info),
      child:
    Container(
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: Theme.of(context).scaffoldBackgroundColor),
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Text(info.checkinType ?? '',
                          style: TextStyle(fontSize: 12.sp))),
                  Text(
                    info.signTime ?? '',
                    style: TextStyle(fontSize: 12.sp),
                  )
                ],
              ),
              const SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Text(
                          isRecord ? recordTime : 'attendance_uncheckin'.tr,
                          style: TextStyle(
                              color: AppColor.hintText, fontSize: 12.sp))),
                  Visibility(
                      visible: info.lastRecord != null && info.lastRecord?.id != null,
                      child: Text('attendance_update_check_time'.tr,
                          style: TextStyle(
                              color: AppColor.primaryColor, fontSize: 13.sp)))
                ],
              )
            ]),
      ),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OldCheckInController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("app_name_attendance".tr)),
          body: SafeArea(
            child: _buildView(context),
          ),
        );
      },
    );
  }
}
