

import 'package:flutter/material.dart';

class SheetMenuData {
  String title;
  GestureTapCallback onTap;
  SheetMenuData(this.title, this.onTap);

}

class DialogActionData {
  String? title;
  VoidCallback? pressedCall;
  DialogActionData({this.title, this.pressedCall});
}