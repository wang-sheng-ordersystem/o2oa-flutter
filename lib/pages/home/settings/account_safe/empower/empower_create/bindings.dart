import 'package:get/get.dart';

import 'controller.dart';

class EmpowerCreateBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EmpowerCreateController>(() => EmpowerCreateController());
  }
}
