import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/utils/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class AccountSafePage extends GetView<AccountSafeController> {
  const AccountSafePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AccountSafeController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("settings_account_safe".tr)),
          body: SafeArea(
            child: ListView(
              children: [
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Column(children: [
                      O2UI.lineWidget(
                          'account_safe_server_name'.tr,
                          Text(
                              O2ApiManager.instance.o2CloudServer?.centerHost ??
                                  '')),
                    ]),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Column(children: [
                      O2UI.lineWidget('account_safe_login_name'.tr,
                          Text(O2ApiManager.instance.o2User?.name ?? '')),
                      const Divider(height: 1),
                      O2UI.lineWidget('account_safe_login_pwd_label'.tr,
                          Text('account_safe_login_pwd_update'.tr),
                          ontap: controller.goUpdatePassword),
                    ]),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Column(children: [
                      Obx(() => Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Text(controller.state.title),
                              Switch(
                                value: controller.state.biometricAuthSwitch,
                                onChanged: (bool value) {
                                  controller.switchLocalAuth(value);
                                },
                              )
                            ],
                          ))
                    ]),
                    context),
                const SizedBox(height: 10),
                O2UI.sectionOutBox(
                    Column(children: [
                      O2UI.lineWidget(
                          'account_safe_empower'.tr, const SizedBox(),
                          ontap: controller.goEmpower),
                    ]),
                    context),
              ],
            ),
          ),
        );
      },
    );
  }
}
