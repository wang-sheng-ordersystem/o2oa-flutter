import 'package:get/get.dart';

import 'controller.dart';

class ServerInfoBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ServerInfoController>(() => ServerInfoController());
  }
}
