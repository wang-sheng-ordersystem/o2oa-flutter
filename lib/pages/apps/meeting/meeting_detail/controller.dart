import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../common/models/index.dart';
import 'index.dart';

class MeetingDetailController extends GetxController {
  MeetingDetailController();

  final state = MeetingDetailState();

   
  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    final meeting = Get.arguments;
    if (meeting != null && meeting is MeetingInfoModel) {
      state.meetingInfo.value = meeting;
    }
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  openOnlineRoomLink() async {
    if (state.meetingInfo.value?.mode == MeetingMode.online.getKey() && state.meetingInfo.value?.roomLink?.isNotEmpty == true) {
      Uri uri = Uri.parse(state.meetingInfo.value!.roomLink!);
      if (await canLaunchUrl(uri)) {
        await launchUrl(uri, mode: LaunchMode.externalApplication);
      }
    }
  }
}
