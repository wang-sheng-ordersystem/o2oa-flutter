import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import '../../../../common/api/index.dart';
import '../../../../common/models/index.dart';
import '../../../../common/utils/index.dart';
import '../../../../common/widgets/index.dart';
import '../enterprise_file_list/index.dart';
import 'index.dart';

class EnterpriseZoneController extends GetxController {
  EnterpriseZoneController();

  final state = EnterpriseZoneState();
  final TextEditingController newZoneController = TextEditingController();
  final TextEditingController zoneDescController = TextEditingController();

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    loadConfig();
    loadZoneList();
    super.onReady();
  }
  /// 是否有权限创建工作区
  loadConfig()  async {
    final result = await FileAssembleService.to.v3ZoneCanCreate();
    if (result != null && result.value == true) {
      state.canCreate = true;
    }
  }
  /// 工作区列表
  loadZoneList() async {
    Loading.show();
    state.zoneList.clear();
    final list2 = await FileAssembleService.to.v3ZoneList();
    if (list2 != null) {
      for (var item in list2) {
        item.groupTag = 'cloud_disk_v3_inner_zone'.tr;
        state.zoneList.add(item);
      }
    }
    final list1 = await FileAssembleService.to.v3FavoriteList();
    if (list1 != null) {
      for (var item in list1) {
        item.groupTag = 'cloud_disk_v3_my_favorite'.tr;
        state.zoneList.add(item);
      }
    }
    Loading.dismiss();
  }
  /// 打开工作区
  clickZone(CloudDiskZoneInfo zone) {
    EnterpriseFileListPage.open(zone.zoneId ?? '', zone.name ?? '', zone.isAdmin == true || zone.isEditor == true);
  }
  /// 工作区操作菜单
  clickZoneMenu(CloudDiskZoneInfo zone){
    final context = Get.context;
    if (context == null) {
      return;
    }
    List<SheetMenuData> menus = [];
    // 收藏的
    if (zone.groupTag == 'cloud_disk_v3_my_favorite'.tr) {
      menus.add(SheetMenuData('cloud_disk_v3_action_favorite_cancel'.tr, () => _cancelFavorite(zone)));
    } else {
      menus.add(SheetMenuData('cloud_disk_v3_action_favorite_add'.tr, () => _addFavorite(zone)));
      if (zone.isAdmin == true || zone.isEditor == true) {
        menus.add(SheetMenuData('cloud_disk_v3_action_zone_edit'.tr, () => _editZone(zone)));
        menus.add(SheetMenuData('cloud_disk_v3_action_zone_delete'.tr, () => _deleteZone(zone)));
      }
    }
    O2UI.showBottomSheetWithMenuData(context, menus);
  }
  /// 取消收藏
  _cancelFavorite(CloudDiskZoneInfo zone) async {
    final context = Get.context;
    if (context == null) {
      return;
    }
    O2UI.showConfirm(context, 'cloud_disk_v3_favorite_delete_confirm'.trArgs([zone.name??'']), okPressed: () {
      _deleteFavoriteOnline(zone.id!);
    });
  }
  _deleteFavoriteOnline(String id) async {
    final result = await FileAssembleService.to.v3DeleteFavorite(id);
    if (result != null) {
      loadZoneList();
    }
  }
  /// 加入收藏
  _addFavorite(CloudDiskZoneInfo zone) async {
    final post = CloudDiskFavoritePost(name: zone.name, folder: zone.id);
    final createResult = await FileAssembleService.to.v3CreateFavorite(post);
      if (createResult != null) {
        loadZoneList();
      }
  }
  /// 编辑
  _editZone(CloudDiskZoneInfo zone) async {
    final context = Get.context;
    if (context == null) {
      return;
    }
    newZoneController.text = zone.name ?? '';
    zoneDescController.text = zone.description ?? '';
    final content = _zoneDialogContent();
    final result = await O2UI.showCustomDialog(context, 'cloud_disk_v3_action_zone_edit'.tr, content);
    if (result != null && result == O2DialogAction.positive) {
      final name = newZoneController.text;
      if (name.isEmpty) {
        Loading.toast('cloud_disk_v3_zone_create_name_hint'.tr);
        return;
      }
      final desc = zoneDescController.text;
      final post = CloudDiskZonePost(name: name, description: desc);
      final createResult = await FileAssembleService.to.v3UpdateZone(zone.id!, post);
      if (createResult != null) {
        loadZoneList();
      }
    }
  }
  /// 删除
  _deleteZone(CloudDiskZoneInfo zone) {
    final context = Get.context;
    if (context == null) {
      return;
    }
    O2UI.showConfirm(context, 'cloud_disk_v3_zone_delete_confirm'.trArgs([zone.name??'']), okPressed: () {
      _deleteZoneOnline(zone.id!);
    });
  }
  _deleteZoneOnline(String id) async {
    final result = await FileAssembleService.to.v3DeleteZone(id);
    if (result != null) {
      loadZoneList();
    }
  }

  /// 创建工作区
  createZone() async {
    final context = Get.context;
    if (context == null) {
      return;
    }
    newZoneController.text = '';
    zoneDescController.text = '';
    final content = _zoneDialogContent();
    final result = await O2UI.showCustomDialog(context, 'cloud_disk_v3_zone_create'.tr, content);
    if (result != null && result == O2DialogAction.positive) {
      final name = newZoneController.text;
      if (name.isEmpty) {
        Loading.toast('cloud_disk_v3_zone_create_name_hint'.tr);
        return;
      }
      final desc = zoneDescController.text;
      final post = CloudDiskZonePost(name: name, description: desc);
      final createResult = await FileAssembleService.to.v3CreateZone(post);
      if (createResult != null) {
        loadZoneList();
      }
    }
  }

  Widget _zoneDialogContent() {
    return  Column(
      children: [
        TextField(
          controller: newZoneController,
          autofocus: true,
          decoration:
             InputDecoration(labelText: 'cloud_disk_v3_zone_create_name'.tr, hintText: 'cloud_disk_v3_zone_create_name_hint'.tr),
        ),
        const SizedBox(height: 10),
        TextField(
          controller: zoneDescController,
          maxLines: 3,
          decoration:
             InputDecoration(labelText: 'cloud_disk_v3_zone_create_desc'.tr, hintText: 'cloud_disk_v3_zone_create_desc_hint'.tr),
        ),
      ],
    );
  }
 
}
