import 'package:get/get.dart';
import 'package:local_auth/local_auth.dart';


class BioAuthState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  Rx<BiometricType?> bioType = Rx<BiometricType?>(null);
}
