class MeetingConfigModel {
  String? weekBegin; //周开始 0星期天 1星期一
  MeetingConfigProcessModel? process;
  List<String>? meetingViewer;
  List<String>? disableViewList;
  List<String>? typeList; // 会议类型
  bool? mobileCreateEnable; // 移动端是否可创建会议
  String? toMyMeetingViewName;
  String? toMonthViewName;
  String? toWeekViewName;
  String? toDayViewName;
  String? toListViewName;
  String? toRoomViewName;
  bool? enableOnline; // 是否启用在线会议
  String? onlineProduct; // 在线会议类型：其他 | 好视通

  MeetingConfigModel(
      {this.weekBegin,
      this.process,
      this.meetingViewer,
      this.disableViewList,
      this.typeList,
      this.mobileCreateEnable,
      this.toMyMeetingViewName,
      this.toMonthViewName,
      this.toWeekViewName,
      this.toDayViewName,
      this.toListViewName,
      this.toRoomViewName,
      this.enableOnline,
      this.onlineProduct
      });

  MeetingConfigModel.fromJson(Map<String, dynamic> json) {
    weekBegin = json['weekBegin'];
    process = json['process'] == null
        ? null
        : MeetingConfigProcessModel.fromJson(json['process']);
    meetingViewer = json['meetingViewer'] == null
        ? null
        : List<String>.from(json['meetingViewer'].map((x) => x));
    disableViewList = json['disableViewList'] == null
        ? null
        : List<String>.from(json['disableViewList'].map((x) => x));
    typeList = json['typeList'] == null
        ? null
        : List<String>.from(json['typeList'].map((x) => x));
    if (json['mobileCreateEnable'] != null &&
        json['mobileCreateEnable'] is String) {
      mobileCreateEnable = json['mobileCreateEnable'] == "true";
    } else {
      mobileCreateEnable = json['mobileCreateEnable'];
    }
    toMyMeetingViewName = json['toMyMeetingViewName'];
    toMonthViewName = json['toMonthViewName'];
    toWeekViewName = json['toWeekViewName'];
    toDayViewName = json['toDayViewName'];
    toListViewName = json['toListViewName'];
    toRoomViewName = json['toRoomViewName'];
    enableOnline = json['enableOnline'];
    onlineProduct = json['onlineProduct'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['weekBegin'] = weekBegin;
    data['process'] = process?.toJson();
    data['meetingViewer'] = meetingViewer;
    data['disableViewList'] = disableViewList;
    data['typeList'] = typeList;
    data['mobileCreateEnable'] = mobileCreateEnable;
    data['toMyMeetingViewName'] = toMyMeetingViewName;
    data['toMonthViewName'] = toMonthViewName;
    data['toWeekViewName'] = toWeekViewName;
    data['toDayViewName'] = toDayViewName;
    data['toListViewName'] = toListViewName;
    data['toRoomViewName'] = toRoomViewName;
    data['enableOnline'] = enableOnline;
    data['onlineProduct'] = onlineProduct;
    return data;
  }
}

class MeetingConfigProcessModel {
  String? name;
  String? id;
  String? application;
  String? applicationName;
  String? alias;

  MeetingConfigProcessModel(
      {this.id, this.name, this.application, this.applicationName, this.alias});

  MeetingConfigProcessModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    application = json['application'];
    applicationName = json['applicationName'];
    alias = json['alias'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['application'] = application;
    data['applicationName'] = applicationName;
    data['alias'] = alias;
    return data;
  }
}
