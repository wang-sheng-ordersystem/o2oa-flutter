import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class MainState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 默认两个标签 
  final _bottomTabs = Rx<List<BottomNavigationBarItem>>([BottomNavigationBarItem(
        icon: const Icon(Icons.location_on),
        activeIcon: const Icon(
          Icons.location_on),
        label: 'attendance_tab_checkin'.tr),
        BottomNavigationBarItem(
        icon: const Icon(Icons.pie_chart),
        activeIcon: const Icon(
          Icons.pie_chart),
        label: 'attendance_tab_statistic'.tr)]);
  set bottomTabs(List<BottomNavigationBarItem> value) => _bottomTabs.value = value;
  List<BottomNavigationBarItem> get bottomTabs => _bottomTabs.value;

  // 当前 bottombar 页码
  final _currentIndex = 0.obs;
  set currentIndex(value) => _currentIndex.value = value;
  get currentIndex => _currentIndex.value;


  // 当前时间
  final _currentTime = "".obs;
  set currentTime(value) => _currentTime.value = value;
  get currentTime => _currentTime.value;

   // 当前地址信息
  final _currentAddress = "".obs;
  set currentAddress(value) => _currentAddress.value = value;
  String get currentAddress => _currentAddress.value;
  
   // 是否在打卡地址范围内
  final _isInCheckInPositionRange = false.obs;
  set isInCheckInPositionRange(bool value) => _isInCheckInPositionRange.value = value;
  bool get isInCheckInPositionRange => _isInCheckInPositionRange.value;


   // 今天是否还需要打卡
  final _todayNeedCheck = false.obs;
  set todayNeedCheck(bool value) => _todayNeedCheck.value = value;
  bool get todayNeedCheck => _todayNeedCheck.value;

  // 打卡记录
  RxList<AttendanceV2Record> checkItemList = <AttendanceV2Record>[].obs;


  /////////////////////// statistic
  // 当前年份
  final _currentYear = "2023".obs;
  set currentYear(value) => _currentYear.value = value;
  get currentYear => _currentYear.value;
  // 当前月份
  final _currentMonth = "5月".obs;
  set currentMonth(value) => _currentMonth.value = value;
  get currentMonth => _currentMonth.value;


  Rx<AttendanceV2Statistic?> myStatistic = Rx<AttendanceV2Statistic?>(null);
  

}
