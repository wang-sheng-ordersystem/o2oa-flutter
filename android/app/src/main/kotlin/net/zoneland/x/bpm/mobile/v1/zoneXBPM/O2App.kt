package net.zoneland.x.bpm.mobile.v1.zoneXBPM

import android.app.Application
import android.content.Context




/**
 * Created by fancyLou on 2022-06-30.
 * Copyright © 2022 android. All rights reserved.
 */
class O2App : Application() {


    companion object {
        lateinit var instance: O2App
    }



    var mContext: Context? = null

    override fun onCreate() {
        super.onCreate()
        instance = this
        mContext = applicationContext
    }
}