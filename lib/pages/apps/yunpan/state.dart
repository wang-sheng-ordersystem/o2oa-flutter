import 'package:get/get.dart';

import '../../../common/models/index.dart';

class YunpanState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  
  //面包屑
  RxList<FolderBreadcrumbBean> breadcrumbBeans = <FolderBreadcrumbBean>[].obs;

  // 文档列表 包含文件夹和文件
  RxList<CloudDiskItem> cloudDiskItemList = <CloudDiskItem>[].obs;

  RxList<CloudDiskItem> cloudDiskItemCheckedList = <CloudDiskItem>[].obs;

  // 
  final _visibleTop = true.obs;
  set visibleTop(value) => _visibleTop.value = value;
  get visibleTop => _visibleTop.value;


  /// 动画图标是否显示
  final _visibleAnimIcon = false.obs;
  set visibleAnimIcon(bool value) => _visibleAnimIcon.value = value;
  bool get visibleAnimIcon => _visibleAnimIcon.value;
  final _animTop = 0.0.obs;
  set animTop(double value) => _animTop.value = value;
  double get animTop => _animTop.value;
  final _animRight = 0.0.obs;
  set animRight(double  value) => _animRight.value = value;
  double get animRight => _animRight.value;
   final _animWidth = 0.0.obs;
  set animWidth(double value) => _animWidth.value = value;
  double get animWidth => _animWidth.value;


}
