import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/models/index.dart';
import '../../../common/routers/index.dart';
import '../../../common/style/index.dart';
import '../../../common/utils/log_util.dart';
import '../../../common/widgets/index.dart';
import '../../apps/cms/cms_document/index.dart';
import '../process_webview/index.dart';
import 'index.dart';

typedef StartProcessCallbackJob = void Function(String jobId);

class CreateFormPage extends GetView<CreateFormController> {
  const CreateFormPage({Key? key}) : super(key: key);

  /// 启动cms创建工作
  static void startCmsDoc(bool ignoreTitle,
      {@required CmsCategoryData? category}) async {
    var result = await Get.toNamed(O2OARoutes.commonCreateForm,
        arguments: {"ignoreTitle": ignoreTitle, "category": category});
    if (result != null && result is CreateFormBackResult) {
      OLogger.d('返回 cms ${result.toJson()}');
      var cmsDoc = result.cmsDoc;
      if (result.resultType == CreateFormBackResultType.cmsDoc &&
          cmsDoc != null) {
        var docid = cmsDoc.id;
        var doctitle = cmsDoc.title;
        if (docid != null && docid.isNotEmpty) {
          CmsDocumentPage.open(docid,
              title: doctitle ?? '', options: {"readonly": false});
        } else {
          OLogger.e('创建文档后doc id 没有获取到！！！！！');
        }
      } else {
        OLogger.e('创建文档后返回结果不正确！！！！！');
      }
    } else {
      OLogger.e('创建文档后 没有返回结果！！！！！');
    }
  }

  ///  启动流程工作
  static void startProcess(bool ignoreTitle,
      {@required ProcessData? process,
      StartProcessCallbackJob? callback,
      dynamic workData}) async {
    var result = await Get.toNamed(O2OARoutes.commonCreateForm, arguments: {
      "ignoreTitle": ignoreTitle,
      "process": process,
      "workData": workData,
      "callback": callback
    });
    if (result != null && result is CreateFormBackResult) {
       OLogger.d('返回 process ${result.toJson()}');
      var processWork = result.processWork;
      var processDraft = result.processDraft;
      if (result.resultType == CreateFormBackResultType.processWork &&
          processWork != null &&
          processWork.work != null) {
        ProcessWebviewPage.open(processWork.work!,
            title: processWork.title ?? '');
      } else if (result.resultType == CreateFormBackResultType.processDraft &&
          processDraft != null) {
        ProcessWebviewPage.openDraft(processDraft);
      } else {
        OLogger.e('创建工作后返回结果不正确 ！！！！！');
      }
    } else {
      OLogger.e('创建工作后 没有返回结果！！！！！');
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CreateFormController>(
      builder: (_) {
        return Scaffold(
          appBar:
              AppBar(title: Obx(() => Text(controller.state.title)), actions: [
            TextButton(
                onPressed: () => controller.clickSave(),
                child:
                    Text('positive'.tr, style: AppTheme.whitePrimaryTextStyle))
          ]),
          body: SafeArea(
            child: Container(
              color: Theme.of(context).colorScheme.background,
              child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    children: [
                      Obx(() => MyIdentityChooseView(
                            identityList:
                                controller.state.identityList.toList(),
                            onChange: (identityDn) =>
                                controller.chooseIdentity(identityDn),
                          )),
                      Obx(() => Visibility(
                          visible: !controller.state.ignoreTitle,
                          child: TextField(
                            maxLines: 1,
                            style: Theme.of(context).textTheme.bodyMedium,
                            autofocus: true,
                            controller: controller.inputController,
                            textInputAction: TextInputAction.done,
                            decoration: InputDecoration(
                              labelText: 'input_form_placeholder'.trArgs(
                                  ['cms_create_document_title_label'.tr]),
                            ),
                          )))
                    ],
                  )),
            ),
          ),
        );
      },
    );
  }
}
