import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/api/index.dart';
import '../../../common/routers/index.dart';
import '../../../common/style/index.dart';
import '../../../common/widgets/index.dart';
import 'index.dart';

class SettingsPage extends GetView<SettingsController> {
  const SettingsPage({Key? key}) : super(key: key);

  // 主视图
  Widget _headerView(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, bottom: 10, left: 15, right: 15),
      child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: ListTile(
          onTap: () => Get.toNamed(O2OARoutes.homeSettingsMyProfile),
          leading: Obx(() => SizedBox(
                width: 50.w,
                height: 50.w,
                child: O2UI.personAvatar(controller.state.dn, 25.w),
              )),
          title: Obx(() {
            return Text(
              controller.state.personName,
              style: AppTheme.textBodyLarge(context),
            );
          }),
          subtitle: Obx(() {
            return Text(
              controller.state.personSign,
              style:  AppTheme.textBodySmall(context),
            );
          }),
          trailing: const Icon(
            Icons.keyboard_arrow_right,
            color: AppColor.hintText,
            size: 22,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SettingsController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text('home_tab_settings'.tr)),
          body: SafeArea(
            child: Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: ListView(children: [
                  // 当前用户卡片
                  _headerView(context),
                  Column(
                    children: [
                      //账号安全
                      outBox(
                          ListTile(
                            onTap: () =>
                                Get.toNamed(O2OARoutes.homeSettingsAccountSafe),
                            leading: AssetsImageView(
                              'icon_settings_safe.png',
                              width: 22.w,
                              height: 22.w,
                            ),
                            title: Text(
                              'settings_account_safe'.tr,
                              style: AppTheme.textBodyLarge(context),
                            ),
                            trailing: const Icon(
                              Icons.keyboard_arrow_right,
                              color: AppColor.hintText,
                              size: 22,
                            ),
                          ),
                          context),

                      outBox(otherSettings(context), context),

                      //关于
                      outBox(
                          ListTile(
                              onTap: controller.clickAbout,
                              onLongPress: controller.longPressAbout,
                              leading:  ProgramCenterService.to.setupAboutImageView(),
                              title: Text(
                                'settings_about'.tr,
                                style: AppTheme.textBodyLarge(context),
                              ),
                              trailing: const Icon(
                                Icons.keyboard_arrow_right,
                                color: AppColor.hintText,
                                size: 22,
                              )),
                          context),

                      // 退出登录按钮
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 40, bottom: 10, left: 15, right: 15),
                        child: SizedBox(
                            width: double.infinity,
                            height: 44,
                            child: O2ElevatedButton(() {
                              // 点击
                              controller.clickLogout();
                            },
                                Text(
                                  'settings_logout'.tr,
                                  style: const TextStyle(fontSize: 18),
                                ))),
                      )
                    ],
                  )
                ])),
          ),
        );
      },
    );
  }

  Widget otherSettings(BuildContext context) {
    return Column(
      children: [
        // 外观
        ListTile(
            onTap: () => Get.toNamed(O2OARoutes.homeSettingsSkin),
            leading: AssetsImageView(
              'icon_settings_skin.png',
              width: 22.w,
              height: 22.w,
            ),
            title: Text(
              'settings_skin_change'.tr,
              style: AppTheme.textBodyLarge(context),
            ),
            trailing: const Icon(
              Icons.keyboard_arrow_right,
              color: AppColor.hintText,
              size: 22,
            )),
        const Divider(height: 1),
        // 消息
        ListTile(
            onTap: controller.clickOpenNotifySettings,
            leading: AssetsImageView(
              'icon_settings_notification.png',
              width: 22.w,
              height: 22.w,
            ),
            title: Text(
              'settings_notification'.tr,
              style: AppTheme.textBodyLarge(context),
            ),
            trailing: const Icon(
              Icons.keyboard_arrow_right,
              color: AppColor.hintText,
              size: 22,
            )),
        const Divider(height: 1),
        // 通用
        ListTile(
            onTap: controller.gotoCommon,
            leading: AssetsImageView(
              'icon_settings_clear_cache.png',
              width: 22.w,
              height: 22.w,
            ),
            title: Text(
              'settings_common'.tr,
              style: AppTheme.textBodyLarge(context),
            ),
            trailing: const Icon(
                  Icons.keyboard_arrow_right,
                  color: AppColor.hintText,
                  size: 22,
                )),
        // const Divider(height: 1),
        // // 系统日志
        // ListTile(
        //     onTap: controller.clickLogs,
        //     leading: AssetsImageView(
        //       'icon_settings_logs.png',
        //       width: 22.w,
        //       height: 22.w,
        //     ),
        //     title: Text(
        //       'settings_logs'.tr,
        //       style: AppTheme.textBodyLarge(context),
        //     ),
        //     trailing: const Icon(
        //       Icons.keyboard_arrow_right,
        //       color: AppColor.hintText,
        //       size: 22,
        //     )),
      ],
    );
  }

  Widget outBox(Widget child, BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 10, left: 15, right: 15),
        child: Card(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: child));
  }
}
