import 'package:get/get.dart';

import 'controller.dart';

class BbsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BbsController>(() => BbsController());
  }
}
