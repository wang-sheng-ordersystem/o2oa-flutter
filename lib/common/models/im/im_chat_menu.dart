enum IMChatMenuType { report, showGroupInfo, updateTitle, updateMembers, clearAllMessage, deleteConv }

class IMChatMenu {
  IMChatMenu(
    this.name,
    this.type,
  );

  String name;
  IMChatMenuType type;
}
