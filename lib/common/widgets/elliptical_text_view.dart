import 'package:flutter/material.dart';

import '../style/index.dart';

/// 椭圆形背景的 Text
class EllipticalText extends StatelessWidget {
  final String text;
  final double circular;
  final double textSize;
  final Color textColor;
  final Color backgroundColor;

  const EllipticalText(this.text, 
      {super.key,
      this.textColor = Colors.white,
      this.textSize = 16.0,
      this.backgroundColor = AppColor.primaryColor,
      this.circular = 30.0});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(circular), // 调整圆角半径以改变椭圆形状
        color: backgroundColor, // 设置背景颜色
      ),
      margin: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 2.0),
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: Text(
          text,
          style: TextStyle(
            color: textColor,
            fontSize: textSize,
          ),
      ),
    );
  }
}
