import 'package:get/get.dart';

import 'controller.dart';

class EmpowerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EmpowerController>(() => EmpowerController());
  }
}
