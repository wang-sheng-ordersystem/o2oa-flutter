


import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';
import '../values/index.dart';

class JpushAssembleControlService extends GetxService {
  
  static JpushAssembleControlService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_jpush_assemble_control) ?? '';
  }

  ///
  /// 绑定设备号
  ///
  Future<ValueBoolData?> bindDevice(String deviceName) async {
    try {
      JPushDeviceData deviceData = JPushDeviceData(deviceName: deviceName, pushType: O2.jPushType, deviceType: GetPlatform.isAndroid ? 'android' : 'ios');
      ApiResponse response = await O2HttpClient.instance
          .post('${baseUrl()}jaxrs/device/bind', deviceData.toJson());
      return ValueBoolData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('绑定设备失败', err, stackTrace);
    }
    return null;
  }

  ///
  /// 解除绑定
  /// 
  Future<ValueBoolData?> unBindDevice(String deviceName) async {
    try {
      var deviceType =  GetPlatform.isAndroid ? 'android' : 'ios';
      ApiResponse response = await O2HttpClient.instance
          .get('${baseUrl()}jaxrs/device/unbind/new/$deviceName/$deviceType/${O2.jPushType}');
      return ValueBoolData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('解绑设备失败', err, stackTrace);
    }
    return null;
  }

  

}