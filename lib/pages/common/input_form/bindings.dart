import 'package:get/get.dart';

import 'controller.dart';

class InputFormBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InputFormController>(() => InputFormController());
  }
}
