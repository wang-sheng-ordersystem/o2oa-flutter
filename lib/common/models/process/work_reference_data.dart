import 'index.dart';

class WorkReference {

  List<Work>? workList;
  List<Work>? workCompletedList;

  List<WorkLog>? workLogList;

  WorkReference({this.workCompletedList, this.workList, this.workLogList});

  factory WorkReference.fromJson(Map<String, dynamic> json) => WorkReference(
    workList: json['workList'] == null ? null : List<Work>.from(json['workList'].map((e) => Work.fromJson(e))), 
    workCompletedList: json['workCompletedList'] == null ? null : List<Work>.from(json['workCompletedList'].map((e) => Work.fromJson(e))), 
    workLogList: json['workLogList'] == null ? null : List<WorkLog>.from(json['workLogList'].map((e) => WorkLog.fromJson(e))), 
  );

  Map<String, dynamic> toJson() => {
    'workList': workList,
    'workCompletedList': workCompletedList,
    'workLogList': workLogList
  };

}