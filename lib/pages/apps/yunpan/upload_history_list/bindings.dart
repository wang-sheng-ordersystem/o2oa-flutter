import 'package:get/get.dart';

import 'controller.dart';

class UploadHistoryListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UploadHistoryListController>(() => UploadHistoryListController());
  }
}
