
class AttendanceV2RequestStatistic {
  String? startDate;
  String? endDate;
  AttendanceV2RequestStatistic({this.startDate, this.endDate});

  factory AttendanceV2RequestStatistic.fromJson(Map<String, dynamic> json) => AttendanceV2RequestStatistic(
        startDate: json["startDate"],
        endDate: json["endDate"],
         
    );

    Map<String, dynamic> toJson() => {
        "startDate": startDate,
        "endDate": endDate,
    };
}


class AttendanceV2Statistic {
    String? userId;
    int? workTimeDuration;
    String? averageWorkTimeDuration;
    int? attendance;
    int? rest;
    int? absenteeismDays;
    int? lateTimes;
    int? leaveEarlierTimes;
    int? absenceTimes;
    int? fieldWorkTimes;
    int? leaveDays;
    int? appealNums;

    AttendanceV2Statistic({
        this.userId,
        this.workTimeDuration,
        this.averageWorkTimeDuration,
        this.attendance,
        this.rest,
        this.absenteeismDays,
        this.lateTimes,
        this.leaveEarlierTimes,
        this.absenceTimes,
        this.fieldWorkTimes,
        this.leaveDays,
        this.appealNums,
    });

    factory AttendanceV2Statistic.fromJson(Map<String, dynamic> json) => AttendanceV2Statistic(
        userId: json["userId"],
        workTimeDuration: json["workTimeDuration"],
        averageWorkTimeDuration: json["averageWorkTimeDuration"],
        attendance: json["attendance"],
        rest: json["rest"],
        absenteeismDays: json["absenteeismDays"],
        lateTimes: json["lateTimes"],
        leaveEarlierTimes: json["leaveEarlierTimes"],
        absenceTimes: json["absenceTimes"],
        fieldWorkTimes: json["fieldWorkTimes"],
        leaveDays: json["leaveDays"],
        appealNums: json["appealNums"],
    );

    Map<String, dynamic> toJson() => {
        "userId": userId,
        "workTimeDuration": workTimeDuration,
        "averageWorkTimeDuration": averageWorkTimeDuration,
        "attendance": attendance,
        "rest": rest,
        "absenteeismDays": absenteeismDays,
        "lateTimes": lateTimes,
        "leaveEarlierTimes": leaveEarlierTimes,
        "absenceTimes": absenceTimes,
        "fieldWorkTimes": fieldWorkTimes,
        "leaveDays": leaveDays,
        "appealNums": appealNums,
    };
}