import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';

import '../controller.dart';

/// 
/// 表情列表
/// 
class EmojiListWidget extends GetView<ImChatController> {
  const EmojiListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
          padding: const EdgeInsets.all(10),
          child: GridView.count(
              shrinkWrap: true,
              crossAxisCount: 10,
              mainAxisSpacing: 5.0, //设置上下之间的间距
              crossAxisSpacing: 2.0, //设置左右之间的间距
              childAspectRatio: 1, //设置宽高比
              children: O2Emoji.emojiList.keys.map((element) {
                return InkWell(
                  onTap: ()=> controller.onSendEmoji(element),
                  child: Image.asset(O2Emoji.getEmojiPath(element)),
                );
              }).toList()),
    );
  }
}
