
import 'dart:math' as math; // import this
import 'package:flutter/material.dart';

import '../../../../../common/models/index.dart';
import '../../../../../common/widgets/index.dart';
import 'msg_body.dart';

class ReceivedMessageWidget extends StatelessWidget {
  final IMMessageBody msgBody;
  final String msgId;
  const ReceivedMessageWidget({
    Key? key,
    required this.msgBody,
    required this.msgId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final messageTextGroup = Flexible(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationY(math.pi),
              child: CustomPaint(
                painter: ImBubbleShape(Colors.grey[300]!),
              ),
            ),
            Flexible(
              child: Container(
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(18),
                    bottomLeft: Radius.circular(18),
                    bottomRight: Radius.circular(18),
                  ),
                ),
                child: MsgBodyWidget(msgBody: msgBody, isSender: false, msgId: msgId),
              ),
            ),
          ],
        ));

    return Padding(
      padding: const EdgeInsets.only(right: 0, left: 10, top: 10, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          const SizedBox(height: 30),
          messageTextGroup,
        ],
      ),
    );
  }
}