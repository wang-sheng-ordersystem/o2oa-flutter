

class RSAPublicKeyData {
  String? publicKey;
  bool? rsaEnable;

  RSAPublicKeyData({
    this.publicKey,
    this.rsaEnable
  });
  RSAPublicKeyData.fromJson(Map<String, dynamic> jsonMap) {
    publicKey = jsonMap['publicKey'];
    rsaEnable = jsonMap['rsaEnable'];
  }

  Map<String, dynamic> toJson() {
    return {
      'publicKey': publicKey,
      'rsaEnable': rsaEnable
    };
  }
}