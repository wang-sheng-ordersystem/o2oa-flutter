import 'package:get/get.dart';

class AccountSafeState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  final _biometricAuthSwitch = false.obs;
  set biometricAuthSwitch(bool value) => _biometricAuthSwitch.value = value;
  bool get biometricAuthSwitch => _biometricAuthSwitch.value;
}
