import 'dart:async';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:o2oa_all_platform/common/models/index.dart';

import '../../../common/api/index.dart';
import '../../../common/models/bbs/index.dart';
import '../../../common/utils/index.dart';

/// 论坛通用工具类
mixin BBSCommons {

    // 拍照 图片选择
  final ImagePicker _imagePicker = ImagePicker();
  // 设备信息读取
  final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  /// 把文本内容 转成 html 文档
  String formatToHtml(String content) {
    String ret = "";
    if (content.isNotEmpty) {
      if (content.contains('\n')) {
        var list = content.split('\n');
        for (var i in list) {
          ret += '<p>$i</p>';
        }
      } else {
        ret = content;
      }
    }
    return ret;
  }

  /// 添加图片到 html 内容中
  String addImagesToContent(List<ReplyImageItem> images) {
    String content = "";
    for (var v in images) {
      String imageUrl =
          BBSAssembleControlService.to.getBBSAttachmentURL(v.fileId!);
      String path =
          '<p><img src="$imageUrl"  style="display: block; margin: auto; width:${v.showWidth}px; max-width:100%;" /></p>';
      content += path;
    }
    return content;
  }

  /// 选择图片 或者 拍照
  Future<XFile?> pickImageByType(ImageSource source) async {
    return  await _imagePicker.pickImage(source: source);
  }

  /// 获取本地图片的宽高信息
  Future<Size> getLocalImageSize(String localImagePath) async {
    Completer<Size> completer = Completer();
    File file = File.fromUri(Uri.parse(localImagePath));
    Image image = Image.file(file);
    image.image
        .resolve(const ImageConfiguration())
        .addListener(ImageStreamListener((imageInfo, o) {
      completer.complete(Size(
          imageInfo.image.width.toDouble(), imageInfo.image.height.toDouble()));
    }));
    return completer.future;
  }



  Future<String> machineInfo() async {
    String deviceData = '';
    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      } else if (Platform.isLinux) {
        deviceData = _readLinuxDeviceInfo(await deviceInfoPlugin.linuxInfo);
      } else if (Platform.isMacOS) {
        deviceData = _readMacOsDeviceInfo(await deviceInfoPlugin.macOsInfo);
      } else if (Platform.isWindows) {
        deviceData = _readWindowsDeviceInfo(await deviceInfoPlugin.windowsInfo);
      }
      // ignore: empty_catches
    } on PlatformException {}
    return deviceData;
  }

  String _readAndroidBuildData(AndroidDeviceInfo build) {
    OLogger.d('Android 信息: $build');
    return '${build.brand}-${build.model}';
  }

  String _readIosDeviceInfo(IosDeviceInfo data) {
    OLogger.d('ios 信息: $data');
    return '${data.systemName}-${data.model}';
  }

  String _readLinuxDeviceInfo(LinuxDeviceInfo data) {
    OLogger.d('linux 信息: $data');
    return '${data.prettyName}-${data.buildId}';
  }

  String _readMacOsDeviceInfo(MacOsDeviceInfo data) {
    OLogger.d('macos 信息: $data');
    return '${data.arch}-${data.model}';
  }

  String _readWindowsDeviceInfo(WindowsDeviceInfo data) {
    OLogger.d('windows 信息: $data');
    return '${data.computerName}-${data.numberOfCores}';
  }

}
