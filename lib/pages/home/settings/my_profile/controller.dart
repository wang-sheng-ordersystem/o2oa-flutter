import 'dart:io';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../common/api/index.dart';
import '../../../../common/utils/index.dart';
import '../../../common/input_form/index.dart';
import 'edit_avatar/index.dart';
import 'index.dart';

class MyProfileController extends GetxController {
  MyProfileController();

  final state = MyProfileState();

  // 拍照 图片选择
  final ImagePicker _imagePicker = ImagePicker();

  final eventBus = EventBus();

 

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    state.email = O2ApiManager.instance.o2User?.mail ?? '';
    state.mobile = O2ApiManager.instance.o2User?.mobile ?? '';
    state.officePhone = O2ApiManager.instance.o2User?.officePhone ?? '';
    state.signature = O2ApiManager.instance.o2User?.signature ?? '';
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  
  void editAvatar(int type) async {
    if (GetPlatform.isDesktop) {
      Loading.toast('my_profile_avatar_not_desktop'.tr);
      return;
    }
    XFile? file;
    if (type == 0) {
      // 相册
      file = await _imagePicker.pickImage(source: ImageSource.gallery);
    } else if (type == 1) {
      // 拍照
      if (!await O2Utils.cameraPermission()) {
        return;
      }
      file = await _imagePicker.pickImage(source: ImageSource.camera);
    }
    if (file != null) {
      var cropImage = await EditAvatarPage.openEditAvatar(file.path);
      if (cropImage != null && cropImage is File) {
        Loading.show();
        await OrganizationPersonalService.to.updateAvatar(cropImage);
        state.dn = O2ApiManager.instance.o2User?.distinguishedName ?? '';
        Loading.dismiss();
        _emitToUpdateAvatar();
      }
    }
  }

  ///
  /// 修改个人信息
  /// 
  void goEdit(MyProfileEditType type) async {
    String title;
    String value;
    switch(type) {
      case MyProfileEditType.mail:
        title = 'my_profile_email'.tr;
        value = state.email;
        break;
      case MyProfileEditType.mobile:
        title = 'my_profile_mobile'.tr;
        value = state.mobile;
        break;
      case MyProfileEditType.officePhone:
        title = 'my_profile_office_phone'.tr;
        value = state.officePhone;
        break;
      case MyProfileEditType.signature:
        title = 'my_profile_signature'.tr;
        value = state.signature;
        break;
    }
    var result = await InputFormPage.openInputForm(title, value);
    if (result != null && result is String && result.isNotEmpty) {
        var user = O2ApiManager.instance.o2User;
        if (user != null) {
          switch(type) {
            case MyProfileEditType.mail:
              state.email = result;
              user.mail = result;
              break;
            case MyProfileEditType.mobile:
              state.mobile = result;
              user.mobile = result;
              break;
            case MyProfileEditType.officePhone:
              state.officePhone = result;
              user.officePhone = result;
              break;
            case MyProfileEditType.signature:
              state.signature = result;
              user.signature = result;
              _emitToUpdateSignature();
              break;
          }
          final s = await OrganizationPersonalService.to.personEdit(user);
          OLogger.d('更新结果：$s');
        }
    }
  }


  /// 刷新设置页面的签名
  void _emitToUpdateSignature() {
    eventBus.emit(EventBus.mySignatureUpdateMsg, 'signature');
  }
  /// 刷新设置页面的头像
  void _emitToUpdateAvatar() {
    eventBus.emit(EventBus.avatarUpdateMsg, 'avatarUpdateMsg');
  }

  
}

enum MyProfileEditType {
   mail, mobile, officePhone, signature
}