import 'package:get/get.dart';

import 'controller.dart';

class GroupMembersBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<GroupMembersController>(() => GroupMembersController());
  }
}
