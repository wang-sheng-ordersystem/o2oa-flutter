class ProcessDraftWorkData {
  String? id;
  String? title;
  String? creatorPerson;
  String? creatorIdentity;
  String? creatorUnit;
  String? application;
  String? applicationName;
  String? applicationAlias;
  String? process;
  String? processName;
  String? processAlias;
  String? workCreateType;
  String? form;

  ProcessDraftWorkData(
      {this.id,
      this.title,
      this.creatorPerson,
      this.creatorIdentity,
      this.creatorUnit,
      this.application,
      this.applicationName,
      this.applicationAlias,
      this.process,
      this.processName,
      this.processAlias,
      this.workCreateType,
      this.form});

  factory ProcessDraftWorkData.fromJson(Map<String, dynamic> json) =>
      ProcessDraftWorkData(
        id: json['id'],
        title: json['title'],
        creatorPerson: json['creatorPerson'],
        creatorIdentity: json['creatorIdentity'],
        creatorUnit: json['creatorUnit'],
        application: json['application'],
        applicationName: json['applicationName'],
        applicationAlias: json['applicationAlias'],
        process: json['process'],
        processName: json['processName'],
        processAlias: json['processAlias'],
        workCreateType: json['workCreateType'],
        form: json['form'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'creatorPerson': creatorPerson,
        'creatorIdentity': creatorIdentity,
        'creatorUnit': creatorUnit,
        'application': application,
        'applicationName': applicationName,
        'applicationAlias': applicationAlias,
        'processName': processName,
        'process': process,
        'processAlias': processAlias,
        'workCreateType': workCreateType,
        'form': form,
      };
}

class ProcessDraftData {
  ProcessDraftWorkData? work;

  ProcessDraftData({this.work});

  factory ProcessDraftData.fromJson(Map<String, dynamic> json) =>
      ProcessDraftData(
        work: json['work'] == null ? null : ProcessDraftWorkData.fromJson(json['work'] ),
      );

  Map<String, dynamic> toJson() => {
        'work': work?.toJson(),
      };
}
