import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/int_extension.dart';

import '../../../../../common/style/index.dart';
import '../../../../../common/values/index.dart';
import '../../../../../common/widgets/index.dart';
import '../index.dart';

/// 文件列表
class EnterpriseFileListWidget extends GetView<EnterpriseFileListController> {
  const EnterpriseFileListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Theme.of(context).colorScheme.background,
        child: Column(
          children: [
            const SizedBox(height: 10),
            breadcrumbView(context),
            const Divider(height: 1, color: AppColor.dividerColor),
            const SizedBox(height: 10),
            Expanded(
                flex: 1,
                child: Obx(() => controller.state.cloudDiskItemList.length > 0
                    ? ListView.builder(
                        itemCount: controller.state.cloudDiskItemList.length,
                        itemBuilder: ((context, index) {
                          var item = controller.state.cloudDiskItemList[index];
                          var icon = 'icon_folder.png'; // 文件夹
                          var updateTime = '';
                          if (!item.isFolder) {
                            icon = FileIcon.getFileIconAssetsNameByExtension(
                                item.file?.extension ?? '');
                            String len =
                                (item.file?.length ?? 0).friendlyFileLength();
                            updateTime = '${item.file?.updateTime ?? ''} $len';
                          } else {
                            updateTime = item.folder?.updateTime ?? '';
                          }

                          return ListTile(
                            onTap: () => controller.onItemClick(item),
                            leading:
                                AssetsImageView(icon, width: 40, height: 40),
                            title: Text(item.displayName),
                            subtitle: Text(updateTime),
                            trailing: Obx(() {
                              var checkedList =
                                  controller.state.cloudDiskItemCheckedList;
                              var checkedItem = checkedList.firstWhereOrNull(
                                  (element) => element == item);
                              /// 是否有编辑权限
                              var isEdit = false;
                              if (item.isFolder) {
                                isEdit = item.folder?.isAdmin == true ||
                                    item.folder?.isEditor == true;
                              } else {
                                isEdit = item.file?.isAdmin == true ||
                                    item.file?.isEditor == true;
                              }
                              if (!isEdit) {
                                return const SizedBox(width: 1, height: 1);
                              }
                              return Checkbox(
                                value: checkedItem != null,
                                onChanged: (value) =>
                                    controller.onItemCheckedChange(item, value),
                              );
                            }),
                          );
                        }),
                      )
                    : O2UI.noResultView(context))),
            bottomToolBar()
          ],
        ));
  }

  Widget breadcrumbView(BuildContext context) {
    return SizedBox(
      height: 48.h,
      width: double.infinity,
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Obx(
            () => Row(
                children: controller.state.breadcrumbBeans.map((element) {
              var isLast = (element.folderId ==
                  controller.state.breadcrumbBeans.last.folderId);
              if (isLast) {
                return TextButton(
                    onPressed: () {},
                    child: Text(element.displayName,
                        style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                            color: Theme.of(context).colorScheme.primary)));
              }
              return Row(mainAxisSize: MainAxisSize.min, children: [
                TextButton(
                    onPressed: () => controller.clickBreadcrumb(element),
                    child: Text(element.displayName,
                        style: Theme.of(context).textTheme.bodyLarge)),
                Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child:
                        Text(">", style: Theme.of(context).textTheme.bodyLarge))
              ]);
            }).toList()),
          )),
    );
  }

  Widget bottomToolBar() {
    return Obx(() => Visibility(
          visible: controller.state.cloudDiskItemCheckedList.isNotEmpty,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: bottomButtons(
                  controller.state.cloudDiskItemCheckedList.length),
            ),
          ),
        ));
  }

  List<Widget> bottomButtons(int itemNumber) {
    List<Widget> buttons = [];
    if (itemNumber == 1) {
      buttons.add(Expanded(
          flex: 1,
          child: TextButton(
            child: Text('cloud_disk_buttons_rename'.tr),
            onPressed: () => controller.rename(),
          )));
    }
    buttons.add(Expanded(
        flex: 1,
        child: TextButton(
          child: Text('cloud_disk_buttons_delete'.tr),
          onPressed: () => controller.delete(),
        )));
    buttons.add(Expanded(
        flex: 1,
        child: TextButton(
          child: Text('cloud_disk_buttons_move'.tr),
          onPressed: () => controller.move(),
        )));
    buttons.add(Expanded(
        flex: 1,
        child: TextButton(
          child: Text('cloud_disk_share_action_save_to_pan'.tr),
          onPressed: () => controller.saveToPan(),
        )));
    return buttons;
  }
}
