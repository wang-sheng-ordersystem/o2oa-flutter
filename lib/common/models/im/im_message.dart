
import '../../utils/index.dart';
import 'im_msg_body.dart';

class IMMessagePageData {
  int page = 1; //当前页面
  int totalPage = 1; //总页数
  List<IMMessage> list = [];

  IMMessagePageData({
    this.page = 1,
    this.totalPage = 1,
    this.list = const <IMMessage>[],
  });

}

class IMMessage {
  IMMessage(
      {this.id,
      this.conversationId,
      this.body,
      this.createPerson,
      this.createTime,
      this.sendStatus});

  String? id;
  String? conversationId;
  String? body;
  String? createPerson;
  String? createTime;
  //消息发送状态 0正常 1发送中 2发送失败要重试
  int? sendStatus;


  /// 
  /// 重写 == 界面上remove对象的时候使用
  @override
  bool operator ==(other) {
    return other is IMMessage && other.id == id;
  }

   @override
  int get hashCode => id.hashCode;

  factory IMMessage.fromJson(Map<String, dynamic> json) => IMMessage(
        id: json['id'],
        conversationId: json['conversationId'],
        body: json['body'],
        createPerson: json['createPerson'],
        createTime: json['createTime'],
        sendStatus: json['sendStatus'],
      );
  Map<String, dynamic> toJson() => {
        "id": id,
        "conversationId": conversationId,
        "body": body,
        "createPerson": createPerson,
        "createTime": createTime,
        "sendStatus": sendStatus
      };

  IMMessageBody? toBody() {
    if (body != null && body!.isNotEmpty) {
      return IMMessageBody.fromJson(O2Utils.parseStringToJson(body!));
    }
    return null;
  }
}
