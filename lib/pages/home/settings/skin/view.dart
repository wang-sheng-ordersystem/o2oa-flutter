import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';

import 'index.dart';

class SkinPage extends GetView<SkinController> {
  const SkinPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SkinController>(
      init: SkinController(),
      id: "skin",
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("settings_skin_change".tr)),
          body: SafeArea(
              child: Obx(() => Container(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    child: ListView(children: [
                      const SizedBox(height: 10),
                      outBox(
                          ListTile(
                            leading: Text(
                              'settings_skin_with_system'.tr,
                              style: Theme.of(context).textTheme.bodyLarge,
                            ),
                            trailing: Switch(
                              value: controller.themeMode.value ==
                                  ThemeMode.system,
                              onChanged: (bool value) {
                                controller.changeWithSystem(value);
                              },
                            ),
                          ),
                          context),
                      const SizedBox(height: 10),
                      Visibility(
                          visible:
                              controller.themeMode.value != ThemeMode.system,
                          child: outBox(
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  // 亮色
                                  Expanded(
                                      flex: 1,
                                      child: InkWell(
                                          onTap: controller.clickLightMode,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              const SizedBox(height: 10),
                                              Icon(Icons.light_mode,
                                                  size: 36.w),
                                              const SizedBox(height: 5),
                                              Text('settings_skin_light'.tr,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyMedium),
                                              const SizedBox(height: 5),
                                              O2RadioView(
                                                  isSelected: ThemeMode.light ==
                                                      controller
                                                          .themeMode.value)
                                            ],
                                          ))),
                                  //暗色
                                  Expanded(
                                      flex: 1,
                                      child: InkWell(
                                          onTap: controller.clickDarkMode,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              const SizedBox(height: 10),
                                              Icon(Icons.dark_mode, size: 36.w),
                                              const SizedBox(height: 5),
                                              Text('settings_skin_dark'.tr,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyMedium),
                                              const SizedBox(height: 5),
                                              O2RadioView(
                                                  isSelected: ThemeMode.dark ==
                                                      controller
                                                          .themeMode.value)
                                            ],
                                          )))
                                ],
                              ),
                              context)),
                      const SizedBox(height: 10),
                      skinList(context),
                    ]),
                  ))),
        );
      },
    );
  }

  /// 皮肤选择
  Widget skinList(BuildContext context) {
    return Obx(() => Column(
          children: O2Skin.values.map((s) {
            return InkWell(
                onTap: () => controller.clickChangeSkin(s),
                child: outBox(
                    ListTile(
                        leading: Text(
                          s.displayName,
                          style: AppTheme.whitePrimaryTextStyle,
                        ),
                        trailing: Visibility(
                          visible: controller.skin.value == s,
                          child: const SizedBox(
                            width: 22,
                            height: 22,
                            child: Icon(O2IconFont.ok, color: Colors.white),
                          ),
                        )),
                    context, color:s.getColor()));
          }).toList(),
        ));
  }

  Widget outBox(Widget child, BuildContext context, {Color? color}) {
    return Padding(
        padding: const EdgeInsets.only(bottom: 10, left: 15, right: 15),
        child: Card(
            color: color,
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: child));
  }
}
