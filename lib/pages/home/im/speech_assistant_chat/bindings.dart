import 'package:get/get.dart';

import 'controller.dart';

class SpeechAssistantChatBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SpeechAssistantChatController>(() => SpeechAssistantChatController());
  }
}
