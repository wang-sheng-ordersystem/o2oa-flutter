import 'package:get/get.dart';

import 'controller.dart';

class BioAuthBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BioAuthController>(() => BioAuthController());
  }
}
