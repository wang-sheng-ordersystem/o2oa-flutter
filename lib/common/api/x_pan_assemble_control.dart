



import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';

///
/// v3版本网盘服务
///
class PanAssembleControlService extends GetxService {
  static PanAssembleControlService get to => Get.find();

  String baseUrl() {
    return O2ApiManager.instance
            .getModuleBaseUrl(O2DistributeModuleEnum.x_pan_assemble_control) ??
        '';
  }



  ///
  /// 服务器应答
  /// 
  Future<EchoData?> echo() async {
    try {
      ApiResponse response =
          await O2HttpClient.instance.get('${baseUrl()}jaxrs/echo');
      return EchoData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('v3版本网盘服务器应答', err, stackTrace);
    }
    return null;
  }

 

}