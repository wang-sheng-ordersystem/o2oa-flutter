
import 'package:get/get.dart';

import '../../../../common/api/index.dart';
import '../../../../common/models/index.dart';
import '../../../../common/utils/index.dart';
import 'index.dart';

class ContactPickerController extends GetxController {
  ContactPickerController();

  final state = ContactPickerState();
  ContactPickerArguments? pickerArguments;
  final eventBus = EventBus();
  final contactPickerId = 'contactPickerId';

  /// 在 widget 内存中分配后立即调用。 
  @override
  void onInit() {
    eventBus.on(EventBus.contactPickerAddPersonMsg, contactPickerId, (arg) { 
      if  (arg is O2Identity) {
        state.personPickedList.add(arg);
      }
    });
     eventBus.on(EventBus.contactPickerAddIdentityMsg, contactPickerId, (arg) { 
      if  (arg is O2Identity) {
        state.identityPickedList.add(arg);
      }
    });
    eventBus.on(EventBus.contactPickerAddUnitMsg, contactPickerId, (arg) { 
      if  (arg is O2Unit) {
        state.unitPickedList.add(arg);
      }
    });
    eventBus.on(EventBus.contactPickerAddGroupMsg, contactPickerId, (arg) { 
      if  (arg is O2Group) {
        state.groupPickedList.add(arg);
      }
    });
    eventBus.on(EventBus.contactPickerRemovePersonMsg, contactPickerId, (arg) { 
      if  (arg is O2Identity) {
        state.personPickedList.remove(arg);
      }
    });
     eventBus.on(EventBus.contactPickerRemoveIdentityMsg, contactPickerId, (arg) { 
      if  (arg is O2Identity) {
        state.identityPickedList.remove(arg);
      }
    });
    eventBus.on(EventBus.contactPickerRemoveUnitMsg, contactPickerId, (arg) { 
      if  (arg is O2Unit) {
        state.unitPickedList.remove(arg);
      }
    });
    eventBus.on(EventBus.contactPickerRemoveGroupMsg, contactPickerId, (arg) { 
      if  (arg is O2Group) {
        state.groupPickedList.remove(arg);
      }
    });
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    pickerArguments = Get.arguments;
    if (pickerArguments == null) {
      Loading.showError('args_error'.tr);
      Get.back();
    }
    state.tabs.clear();
    state.tabs.addAll(pickerArguments?.pickerModes ?? []);
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    eventBus.off(EventBus.contactPickerAddPersonMsg, contactPickerId);
    eventBus.off(EventBus.contactPickerAddIdentityMsg, contactPickerId);
    eventBus.off(EventBus.contactPickerAddUnitMsg, contactPickerId);
    eventBus.off(EventBus.contactPickerAddGroupMsg, contactPickerId);
    eventBus.off(EventBus.contactPickerRemoveIdentityMsg, contactPickerId);
    eventBus.off(EventBus.contactPickerRemovePersonMsg, contactPickerId);
    eventBus.off(EventBus.contactPickerRemoveUnitMsg, contactPickerId);
    eventBus.off(EventBus.contactPickerRemoveGroupMsg, contactPickerId);
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }
  /// tab的名称 根据选择器类型显示名称
  String tabTitle(ContactPickMode mode) {
    switch(mode) {
      case ContactPickMode.departmentPicker:
      return 'contact_picker_org'.tr;
      case ContactPickMode.identityPicker:
      return 'contact_picker_identity'.tr;
      case ContactPickMode.groupPicker:
      return 'contact_picker_group'.tr;
      case ContactPickMode.personPicker:
      return 'contact_picker_person'.tr;
    }
  }

  /// 选择
  Future<void> choose() async {
    final idList = state.identityPickedList.toList();
    final personIdList = state.personPickedList.toList();
    final unitList = state.unitPickedList.toList();
    final groupList = state.groupPickedList.toList();
    Loading.show();
    final personList = <O2Person>[];
    for (var element in personIdList) {
      final person = await OrganizationControlService.to.person(element.person ?? '');
      if (person != null) {
        personList.add(person);
      }
    }
    Loading.dismiss();
    ContactPickerResult result = ContactPickerResult(
      identities: idList,
      users: personList,
      departments: unitList,
      groups: groupList
    );
    Get.back(result: result);
  }

   //加载数据
  // void _loadOrgAndIdentityData(String id , int level) {
  //       orgLevel = level
  //       swipe_refresh_contact_complex_picker_main.isRefreshing = true
  //       mPresenter.loadUnitWithParent(id, pickMode!=ORG_PICK_MODE, topList, orgType, duty)
  //   }
  
}
