import 'package:get/get.dart';

import 'controller.dart';

class PortalBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PortalController>(() => PortalController());
  }
}
