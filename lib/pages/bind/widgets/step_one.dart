import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/style/index.dart';
import '../../../common/utils/index.dart';
import '../../../common/widgets/index.dart';
import '../controller.dart';

/// 绑定第一步
class OneStepWidget extends GetView<BindController> {
  const OneStepWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return stepOneWidget(context);
  }

  ///
  ///第一步验证手机号码的 表单界面
  ///
  Widget stepOneWidget(BuildContext context) {
    return SingleChildScrollView(
        child: Column(children: [
      Container(
        height: 96,
        color: Theme.of(context).colorScheme.primary,
        child: Padding(
            padding: const EdgeInsets.only(top: 48),
            child: Align(
                alignment: Alignment.bottomCenter,
                child: Text('bind_validate_mobile_phone'.tr,
                    style: AppTheme.whitePrimaryTextStyle
                        .copyWith(fontSize: 22)))),
      ),
      Container(
        height: 197,
        color: Theme.of(context).colorScheme.primary,
        child:  const AssetsImageView('bind_phone_step_one.png'),
      ),
      Padding(
          padding: const EdgeInsets.all(32),
          child: Form(
            key: controller.formKey,
            child: Column(
              children: [
                // 账号输入框
                TextFormField(
                    controller: controller.phoneController,
                    maxLines: 1,
                    autofocus: true,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    onEditingComplete: () {
                      FocusScope.of(context).requestFocus(controller.codeNode);
                    },
                    decoration: InputDecoration(
                      labelText: 'bind_enter_mobile_phone'.tr,
                      prefixIcon: const Icon(Icons.phone_android),
                    ),
                    validator: (value) => value == null || value.isEmpty
                        ? 'bind_mobile_phone_not_empty'.tr
                        : null),
                const SizedBox(
                  height: 16,
                ),
                //
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: TextFormField(
                        controller: controller.codeController,
                        maxLines: 1,
                        keyboardType: TextInputType.text,
                        focusNode: controller.codeNode,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                          labelText: 'bind_enter_validate_code'.tr,
                          prefixIcon: const Icon(Icons.lock),
                        ),
                        validator: (value) => value == null || value.isEmpty
                            ? 'bind_validate_code_not_empty'.tr
                            : null,
                      ),
                    ),
                    Expanded(
                        flex: 1,
                        child: CountDownButton(() {
                          return controller.onCheckMobile();
                        }, () {
                          OLogger.d('发送短信。。。。');
                          controller.sendSmsCode();
                        }))
                  ],
                ),
                const SizedBox(
                  height: 16,
                ),
                SizedBox(
                  width: double.infinity,
                  height: 42.h,
                  child: O2ElevatedButton(
                    () {
                      controller.nextStep();
                    },
                    Text('bind_next_step'.tr),
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Row(
                  children: [
                    Obx(() => Checkbox(
                          value: controller.state.isAgree,
                          onChanged: (value) => controller.changeAgree(value),
                        )),
                    Expanded(
                      flex: 1,
                      child: 
                    RichText(
                      maxLines: 2,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                          text: 'privacy_policy_bind_content_part1'.tr,
                          style: Theme.of(context).textTheme.bodySmall,
                          children: [
                            TextSpan(
                                text: 'user_agreement_book_mark'.tr,
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    fontSize: 12.sp),
                                recognizer: TapGestureRecognizer()
                                  ..onTap =
                                      () => controller.openUserAgreement()),
                            TextSpan(
                              text: 'privacy_policy_dialog_content_part2'.tr,
                              style: Theme.of(context).textTheme.bodySmall,
                            ),
                            TextSpan(
                                text: 'privacy_policy_book_mark'.tr,
                                style: TextStyle(
                                    color:
                                        Theme.of(context).colorScheme.primary,
                                    fontSize: 12.sp),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () => controller.openPrivacyPolicy()),
                          ]),
                    ))
                  ],
                )
              ],
            ),
          ))
    ]));
  }
}
