#  O2OA（翱途） 企业信息化办公平台 Flutter 源码


## 环境准备
首先得有 Flutter 开发环境，安装 Flutter SDK 、Android 或者 iOS 的开发环境。

目前源码是在 Flutter  如下版本环境下进行开发的，如果你的环境一致，请改成一样的，否则需要自行解决依赖包冲突的各种问题！

Flutter SDK 版本：
```
Flutter (Channel stable, 3.10.6)
```

环境安装后请使用 `flutter doctor` 命令，检查开发环境是否正确。

### 配置 Flutter 使用镜像站点
因为 flutter 官方的地址和包仓库 pub.dev 连接不通，所以先配置镜像地址。
你需要在运行命令之前设置两个环境变量PUB_HOSTED_URL 和 FLUTTER_STORAGE_BASE_URL

```sh
export PUB_HOSTED_URL=https://pub.flutter-io.cn
export FLUTTER_STORAGE_BASE_URL=https://storage.flutter-io.cn
```


## 项目基本信息修改

如果进行 Android 端的调试开发，请注意先填写如下参数到 `android`  目录下 `local.properties` 文件中，这些是 Android 打包的密钥文件和密码：

```
signingConfig.keyAlias=密钥别名
signingConfig.keyPassword=密码
signingConfig.storeFilePath=密钥文件
signingConfig.storePassword=存储密码
```



### 项目名称

app 内部显示的 O2OA 字样，直接修改 flutter 的 dart 文件，在多语言包里面，具体路径：
`lib/common/i18n/lang/` 这个目录下的几个 dart 文件中具体查找替换就行了。

#### iOS

iOS项目的名称就是 app 的名称，在 ios 目录下：
`ios/Runner/Info.plist` 内，找到 `CFBundleDisplayName` 和 `CFBundleName`，他们的值就是 ios 的项目名称。

#### Android

Android 项目名称修改，在Android 源码的  `strings.xml`  文件中，具体路径：
`android/app/src/main/res/values/strings.xml`。


### 项目桌面LOGO

#### iOS

iOS 的 LOGO 图标在 `Assets.xcassets` 目录下：
`ios/Runner/Assets.xcassets/AppIcon.appiconset` 目录下，请根据具体目录中图片的尺寸进行替换。

#### Android

Android 的 LOGO 图标，在Android 源码的  `res`  目录中，具体路径：
`android/app/src/main/res/mipmap-*` 这些目录下，请根据具体目录中图片的尺寸进行替换。



## 第三方SDK配置

需要配置的第三方SDK是极光推送。在 `o2.dart` 文件中可以配置这些 key。
具体路径：`lib/common/values/o2.dart`
![o2dart](./image/o2dart.png)

### Android
android 端还需要特殊配置，具体配置在`android/app/build.gradle`。
![buildgradle](./image/buildgradle.png)


## 直连版本配置
这个配置的意思就是app 不需要通过O2云，直接连接你们自己的 O2OA 服务器，服务器的地址配置在`assets/json/servers.json`，主要是修改里面的 host、protocol、port 参数，格式和参数名称不要动就行了。

![serverjson](./image/serverjson.png)

然后打包的时候记得要加参数  `--dart-define=APP_MODE=inner`

比如打包 Android，flutter 命令如下：
```shell
flutter build apk --release --dart-define=APP_MODE=inner
```


# 协议

[AGPL-3.0 开源协议。](./LICENSE)



# 关于

[![img](./image/O2OA-logo.jpg)](./image/O2OA-logo.jpg)



O2OA（翱途）开发平台是由 **浙江兰德纵横网路技术股份有限公司** 建立和维护的。O2OA（翱途） 的名字和标志是属于 **浙江兰德纵横网路技术股份有限公司** 的注册商标。

我们 ❤️ 开源软件！看一下[我们的其他开源项目](https://github.com/o2oa)，瞅一眼[我们的博客](https://my.oschina.net/o2oa)。
