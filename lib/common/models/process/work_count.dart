class WorkCount {
  // jaxrs/work/count/{credential}

  //  {
  //       "task": 133,
  //       "taskCompleted": 78,
  //       "read": 28,
  //       "readCompleted": 3,
  //       "review": 220
  //   },
    int? task;
    int? taskCompleted;
    int? read;
    int? readCompleted;
    int? review;

    WorkCount({
      this.task,
      this.taskCompleted,
      this.read,
      this.readCompleted,
      this.review
    });

    factory WorkCount.fromJson(Map<String, dynamic> json) => WorkCount(
      task: json["task"],
      taskCompleted: json["taskCompleted"],
      read: json["read"],
      readCompleted: json["readCompleted"],
      review: json["review"],
    );

    Map<String, dynamic> toJson() => {
      "task": task,
      "taskCompleted": taskCompleted,
      "read": read,
      "readCompleted": readCompleted,
      "review": review,
    };
}