import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'index.dart';
import 'widgets/widgets.dart';

class DefaultIndexPage extends GetView<DefaultIndexController> {
  const DefaultIndexPage({Key? key}) : super(key: key);

  /// 主视图
  Widget _mainBuildView(BuildContext context) {
    return Obx(() => SmartRefresher(
        enablePullDown: true,
        enablePullUp: controller.state.hasMoreData,
        controller: controller.refreshController,
        onRefresh: () => controller.refreshDataList(),
        onLoading: () => controller.loadMoreData(),
        child: ListView(
          children: [
            // 滚动大图和应用快捷列表
            const BannerAndAppsWidget(),
             const SizedBox(height: 8),
            _cmsAndTaskTabView(context),
            const SizedBox(height: 10),
            controller.state.listType == 1
                ? const TaskListWidget()
                : const CmsListWidget()
          ],
        )));
  }
  /// Tab 信息中心 办公中心  
  Widget _cmsAndTaskTabView(BuildContext context) {
    return Obx(() => Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: ()=>controller.clickChangeListType(0),
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  child: Text(
                    'home_index_cms_center'.tr,
                    style: TextStyle(
                        color: controller.state.listType == 0
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.secondary,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold),
                  )),
            ),
            const SizedBox(width: 15),
            GestureDetector(
              onTap: ()=>controller.clickChangeListType(1),
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10, right: 10, top: 5, bottom: 5),
                  child: Text(
                    'home_index_task_center'.tr,
                    style: TextStyle(
                        color: controller.state.listType == 1
                            ? Theme.of(context).colorScheme.primary
                            : Theme.of(context).colorScheme.secondary,
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold),
                  )),
            )
          ],
        ));
  }


  @override
  Widget build(BuildContext context) {
    return GetBuilder<DefaultIndexController>(
      builder: (_) {
        return Scaffold(
          body: Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: Column(
                children: [
                  // 顶部搜索栏
                  const TopBarWidget(),
                  Expanded(flex: 1, child: _mainBuildView(context))
                ],
              )),
        );
      },
    );
  }
}
