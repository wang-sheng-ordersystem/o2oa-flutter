// To parse this JSON data, do
//
//     final processApplicationData = processApplicationDataFromJson(jsonString);

import 'index.dart';
 

class ProcessApplicationData {
    ProcessApplicationData({
        this.processList,
        this.id,
        this.name,
        this.alias,
        this.description,
        this.availableIdentityList,
        this.availableUnitList,
        this.availableGroupList,
        this.applicationCategory,
        this.icon,
        this.iconHue,
        this.controllerList,
        this.creatorPerson,
        this.lastUpdateTime,
        this.lastUpdatePerson,
        this.properties,
        this.createTime,
        this.updateTime,
    });

    List<ProcessData>? processList;
    String? id;
    String? name;
    String? alias;
    String? description;
    List<String>? availableIdentityList;
    List<String>? availableUnitList;
    List<String>? availableGroupList;
    String? applicationCategory;
    String? icon;
    String? iconHue;
    List<String>? controllerList;
    String? creatorPerson;
    String? lastUpdateTime;
    String? lastUpdatePerson;
    Map<String, dynamic>? properties;
    String? createTime;
    String? updateTime;

    factory ProcessApplicationData.fromJson(Map<String, dynamic> json) => ProcessApplicationData(
        processList: json["processList"] == null ? null : List<ProcessData>.from(json["processList"].map((x) => ProcessData.fromJson(x))),
        id: json["id"] ,
        name: json["name"] ,
        alias: json["alias"] ,
        description: json["description"] ,
        availableIdentityList: json["availableIdentityList"] == null ? null : List<String>.from(json["availableIdentityList"].map((x) => x)),
        availableUnitList: json["availableUnitList"] == null ? null : List<String>.from(json["availableUnitList"].map((x) => x)),
        availableGroupList: json["availableGroupList"] == null ? null : List<String>.from(json["availableGroupList"].map((x) => x)),
        applicationCategory: json["applicationCategory"] ,
        icon: json["icon"] ,
        iconHue: json["iconHue"] ,
        controllerList: json["controllerList"] == null ? null : List<String>.from(json["controllerList"].map((x) => x)),
        creatorPerson: json["creatorPerson"] ,
        lastUpdateTime: json["lastUpdateTime"] ,
        lastUpdatePerson: json["lastUpdatePerson"] ,
        properties: json["properties"],
        createTime: json["createTime"] ,
        updateTime: json["updateTime"] ,
    );

    Map<String, dynamic> toJson() => {
        "processList": processList == null ? null : List<ProcessData>.from(processList!.map((x) => x.toJson())),
        "id": id ,
        "name": name ,
        "alias": alias ,
        "description": description ,
        "availableIdentityList": availableIdentityList == null ? null : List<String>.from(availableIdentityList!.map((x) => x)),
        "availableUnitList": availableUnitList == null ? null : List<String>.from(availableUnitList!.map((x) => x)),
        "availableGroupList": availableGroupList == null ? null : List<String>.from(availableGroupList!.map((x) => x)),
        "applicationCategory": applicationCategory,
        "icon": icon,
        "iconHue": iconHue ,
        "controllerList": controllerList == null ? null : List<String>.from(controllerList!.map((x) => x)),
        "creatorPerson": creatorPerson ,
        "lastUpdateTime": lastUpdateTime,
        "lastUpdatePerson": lastUpdatePerson ,
        "properties": properties ,
        "createTime": createTime ,
        "updateTime": updateTime ,
    };
}
  