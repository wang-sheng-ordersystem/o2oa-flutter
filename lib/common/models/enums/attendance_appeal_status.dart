import 'package:get/get.dart';

enum AttendanceV2AppealStatusEnum {
    StatusInit, 
    StatusProcessing, 
    StatusProcessAgree, 
    StatusProcessDisagree, 
    StatusAdminDeal
     
}

extension AttendanceV2AppealStatusEnumExtension on AttendanceV2AppealStatusEnum {
  String get name {
    switch(this) {
      case AttendanceV2AppealStatusEnum.StatusInit:
        return 'attendance_appeal_status_init'.tr;
     case AttendanceV2AppealStatusEnum.StatusProcessing:
        return 'attendance_appeal_status_processing'.tr;
     case AttendanceV2AppealStatusEnum.StatusProcessAgree:
        return 'attendance_appeal_status_agree'.tr;
     case AttendanceV2AppealStatusEnum.StatusProcessDisagree:
        return 'attendance_appeal_status_disAgree'.tr;
     case AttendanceV2AppealStatusEnum.StatusAdminDeal:
        return 'attendance_appeal_status_admin'.tr;
            
    }
  }

  int get key {
    switch(this) {
      case AttendanceV2AppealStatusEnum.StatusInit:
        return 0;
      case AttendanceV2AppealStatusEnum.StatusProcessing:
        return 1;
      case AttendanceV2AppealStatusEnum.StatusProcessAgree:
        return 2;
      case AttendanceV2AppealStatusEnum.StatusProcessDisagree:
        return 3;
      case AttendanceV2AppealStatusEnum.StatusAdminDeal:
        return 4;
    }
  }
}