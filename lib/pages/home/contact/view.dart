import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/models/index.dart';
import '../../../common/style/index.dart';
import '../../../common/widgets/index.dart';
import 'index.dart';

class ContactPage extends GetView<ContactController> {
  const ContactPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ContactController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text('home_tab_contact'.tr)),
          body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    searchBar(context),
                    const SizedBox(height: 10),
                    Obx(()=> deptChildren('contact_my_department'.tr,
                        controller.state.myDepartments, context)),
                    const SizedBox(height: 10),
                    Obx(()=> deptChildren('contact_structure'.tr,
                        controller.state.topUnit, context))
                  ],
                )),
          ),
        );
      },
    );
  }

  Widget deptChildren(String title, List<O2Unit> depts, BuildContext context) {
    List<Widget> list = [];
    if (depts.isNotEmpty) {
      list.add(ListTile(title: Text(title, style: AppTheme.textTitleSmall(context))));
      var dList = depts.map((e) {
        var orgName = e.name ?? '';
        var oneName = orgName;
        if (orgName.length > 1) {
          oneName = oneName.substring(0, 1);
        }
        return ListTile(
          leading: SizedBox(
            width: 32,
            height: 32,
            child: CircleAvatar(
              radius: 32,
              backgroundColor: Theme.of(context).colorScheme.primary,
              child: Text(oneName, style: const TextStyle(color: Colors.white)),
            ),
          ),
          title: Text(orgName),
          trailing: O2UI.rightArrow(),
          onTap: () => controller.openDept(e),
        );
      }).toList();
      list.addAll(dList);
    }
    return Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Column(children: list));
  }

  Widget searchBar(BuildContext context) {
    return GestureDetector(
        onTap: controller.openSearchContact,
        child: Container(
            height: 36.h,
            decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.background,
                borderRadius: BorderRadius.all(Radius.circular(18.h))),
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 10.w),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 22,
                    height: 22,
                    child: Icon(O2IconFont.search,
                        color: Theme.of(context).colorScheme.primary),
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 10.w),
                      child: Text(
                        "contact_person_search_placeholder".tr,
                        style: AppTheme.textBodySmall(context),
                      ))
                ],
              ),
            )));
  }
}
