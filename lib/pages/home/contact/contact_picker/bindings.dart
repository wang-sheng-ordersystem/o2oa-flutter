import 'package:get/get.dart';

import 'controller.dart';

class ContactPickerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ContactPickerController>(() => ContactPickerController());
  }
}
