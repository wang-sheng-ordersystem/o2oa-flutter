import 'package:get/get.dart';

import '../../../common/models/index.dart';
import '../../../common/values/index.dart';

class DefaultIndexState {

  // 待办列表
  final RxList<TaskData> taskList = <TaskData>[].obs;


  // 信息列表
  final RxList<DocumentData> cmsList = <DocumentData>[].obs;


  // 热点图片列表
  final RxList<HotpicData> hotpicList = <HotpicData>[].obs;
  // 我的首页应用
  final RxList<AppFrontData?> myAppList = <AppFrontData?>[].obs;

  // 查询分页 lastId
  final _pageLastId = O2.o2DefaultPageFirstKey.obs;
  set pageLastId(value) => _pageLastId.value = value;
  get pageLastId => _pageLastId.value;


  // 当前列表类型 0 信息列表 1 任务列表
  final _listType = 0.obs;
  set listType(int value) => _listType.value = value;
  int get listType => _listType.value;

   // 是否有更多翻页数据
  final _hasMoreData = true.obs;
  set hasMoreData(bool value) => _hasMoreData.value = value;
  bool get hasMoreData => _hasMoreData.value;
}
