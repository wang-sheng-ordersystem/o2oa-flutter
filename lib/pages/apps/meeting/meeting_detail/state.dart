import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class MeetingDetailState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  Rx<MeetingInfoModel?> meetingInfo = Rx<MeetingInfoModel?>(null);
}
