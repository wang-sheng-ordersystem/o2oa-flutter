import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/index.dart';

import 'index.dart';

class CmsPage extends GetView<CmsController> {
  const CmsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CmsController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Obx(()=>Text(controller.state.title))),
          body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.only(
                    top: 10, bottom: 10, left: 15, right: 15),
                child: Obx(() => GridView.count(
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      mainAxisSpacing:
                          10.0, //设置上下之间的间距(Axis.horizontal设置左右之间的间距)
                      crossAxisSpacing:
                          10.0, //设置左右之间的间距(Axis.horizontal设置上下之间的间距)
                      childAspectRatio: 0.8, //设置宽高比
                      children: controller.state.appList
                          .map((element) => Card(
                                elevation: 8.0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: GestureDetector(
                                    onTap: () => controller.clickApp(element),
                                    child: appItem(context, element)),
                              ))
                          .toList(),
                    ))),
          ),
        );
      },
    );
  }

  Widget appItem(BuildContext context, CmsAppData app) {
    return Stack(
      children: [
        Align(
            alignment: Alignment.topCenter,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(app.appName ?? '',
                    style: const TextStyle(fontSize: 18)))),
        Positioned(
            bottom: 0,
            left: 0,
            right: 50,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(app.description ?? 'cms_app_no_description'.tr,
                    maxLines: 1,
                    softWrap: true,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.bodyMedium))),
        Align(
            alignment: Alignment.bottomRight,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                    'cms_app_category_number'
                        .trArgs(['${app.wrapOutCategoryList?.length ?? 0}']),
                    style: Theme.of(context).textTheme.bodyMedium))),
        Align(
            alignment: Alignment.center,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: controller.iconBase64(app.appIcon) == null
                    ? AssetsImageView(
                        'unknow.png',
                        width: 100.w,
                        height: 100.w,
                      )
                    : Image.memory(
                        controller.iconBase64(app.appIcon)!,
                        width: 100.w,
                        height: 100.w,
                        fit: BoxFit.cover,
                      ))),
      ],
    );
  }
}
