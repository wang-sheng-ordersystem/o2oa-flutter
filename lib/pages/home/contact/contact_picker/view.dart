import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import 'index.dart';
import 'widgets/widgets.dart';

class ContactPickerPage extends GetView<ContactPickerController> {
  const ContactPickerPage({Key? key}) : super(key: key);

  static Future<dynamic> startPicker(List<ContactPickMode> modes,
      {List<String>? topUnitList,
      String? unitType,
      int? maxNumber = 0,
      bool? multiple = false,
      List<String>? dutyList,
      List<String>? initDeptList,
      List<String>? initIdList,
      List<String>? initGroupList,
      List<String>? initUserList}) async {
    final args = ContactPickerArguments(
        pickerModes: modes,
        topUnitList: topUnitList,
        unitType: unitType,
        maxNumber: maxNumber,
        multiple: multiple,
        dutyList: dutyList,
        initDeptList: initDeptList,
        initIdList: initIdList,
        initGroupList: initGroupList,
        initUserList: initUserList);
    return await Get.toNamed(O2OARoutes.homeContactPicker, arguments: args);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ContactPickerController>(
      builder: (_) {
        return Obx(() {
          if (controller.state.tabs.isEmpty) {
            return const CircularProgressIndicator();
          } else {
            if (controller.state.tabs.length == 1) {
              return singlePicker(context);
            }
            return multiplePickers(context);
          }
        });
      },
    );
  }

  Widget pickerContentView(ContactPickMode m) {
    switch (m) {
      case ContactPickMode.departmentPicker:
        return OrgPickerPage(initData: controller.pickerArguments!);
      case ContactPickMode.identityPicker:
        return PersonOrIdentityPickerPage(
            mode: m, initData: controller.pickerArguments!);
      case ContactPickMode.groupPicker:
        return GroupPickerPage(initData: controller.pickerArguments!);
      case ContactPickMode.personPicker:
        return PersonOrIdentityPickerPage(
            mode: m, initData: controller.pickerArguments!);
    }
  }

  Widget singlePicker(BuildContext context) {
    return Obx(() {
      final num = controller.state.identityPickedList.length +
          controller.state.personPickedList.length +
          controller.state.unitPickedList.length +
          controller.state.groupPickedList.length;
      final chooseTitle = '${'contact_picker_btn_title_choose'.tr}($num)';
      return Scaffold(
        appBar: AppBar(
          title: Text(controller.tabTitle(controller.state.tabs[0]),
              style: AppTheme.whitePrimaryTextStyle),
          actions: [
            TextButton(
                onPressed: controller.choose,
                child: Text(chooseTitle, style: AppTheme.whitePrimaryTextStyle))
          ],
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: SafeArea(child: pickerContentView(controller.state.tabs[0])),
      );
    });
  }

  Widget multiplePickers(BuildContext context) {
    return Obx(() {
      final num = controller.state.identityPickedList.length +
          controller.state.personPickedList.length +
          controller.state.unitPickedList.length + 
          controller.state.groupPickedList.length;
      final chooseTitle = '${'contact_picker_btn_title_choose'.tr}($num)';
      return DefaultTabController(
          length: controller.state.tabs.length,
          child: Scaffold(
            appBar: AppBar(
              title: Text('contact_picker_title'.tr),
              actions: [
                TextButton(
                    onPressed: controller.choose,
                    child: Text(chooseTitle, style: AppTheme.whitePrimaryTextStyle))
              ],
              bottom: TabBar(
                  //生成Tab菜单
                  tabs: controller.state.tabs
                      .map((e) => Tab(
                          child: Text(controller.tabTitle(e),
                              style: AppTheme.whitePrimaryTextStyle)))
                      .toList()),
            ),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            body: SafeArea(
              child: TabBarView(
                children: controller.state.tabs.map((m) {
                  return pickerContentView(m);
                }).toList(),
              ),
            ),
          ));
    });
  }
}
