


/// 消息文件上传后返回对象
class IMMessageFileResponse {
  IMMessageFileResponse(
      {this.id,
      this.fileExtension,
      this.fileName});

  String? id;
  String? fileExtension;
  String? fileName;
   

  factory IMMessageFileResponse.fromJson(Map<String, dynamic> json) => IMMessageFileResponse(
        id: json['id'],
        fileExtension: json['fileExtension'],
        fileName: json['fileName'],
         
      );
  Map<String, dynamic> toJson() => {
        "id": id,
        "fileExtension": fileExtension,
        "fileName": fileName,
       
      };
 
}