import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../index.dart';

/// 统计
class StatisticWidget extends GetView<MainController> {
  const StatisticWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Padding(
            padding:
                const EdgeInsets.only(top: 20, bottom: 20, left: 15, right: 15),
            child: Card(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        topMonthView(),
                        const SizedBox(height: 10),
                        const Divider(height: 1),
                        const SizedBox(height: 10),
                        Expanded(flex: 1, child: statisticView(context)),
                        TextButton(
                            onPressed: controller.clickOpenExceptionPage,
                            child: Text(
                                'attendance_statistic_exception_data'.tr,
                                style: TextStyle(
                                    color: Theme.of(context).colorScheme.primary,
                                    decoration: TextDecoration.underline,
                                    fontSize: 16.sp))),
                        const SizedBox(height: 10),
                      ],
                    )))));
  }

  /// 统计页面
  Widget statisticView(BuildContext context) {
    return GridView(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          mainAxisSpacing: 10.0,
          crossAxisSpacing: 10.0,
          childAspectRatio: 1.8),
      children: [
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.averageWorkTimeDuration ?? 0}',
            'attendance_statistic_averageWorkTimeDuration'.tr),
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.attendance ?? 0}',
            'attendance_statistic_attendance'.tr),
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.rest ?? 0}',
            'attendance_statistic_rest'.tr),
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.leaveDays ?? 0}',
            'attendance_statistic_leaveDays'.tr),
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.absenteeismDays ?? 0}',
            'attendance_statistic_absenteeismDays'.tr),
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.lateTimes ?? 0}',
            'attendance_statistic_lateTimes'.tr),
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.leaveEarlierTimes ?? 0}',
            'attendance_statistic_leaveEarlierTimes'.tr),
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.absenceTimes ?? 0}',
            'attendance_statistic_absenceTimes'.tr),
        statisticItemView(
            context,
            '${controller.state.myStatistic.value?.fieldWorkTimes ?? 0}',
            'attendance_statistic_fieldWorkTimes'.tr),
      ],
    );
  }

  Widget statisticItemView(BuildContext context, String number, String text) {
    return Column(
      children: [
        Text(number, style: TextStyle(fontSize: 16.sp)),
        const SizedBox(height: 5),
        Text(text,
            style:
                TextStyle(fontSize: 14.sp, color: Theme.of(context).hintColor)),
      ],
    );
  }

  /// 顶部显示当前月份
  Widget topMonthView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(controller.state.currentYear, style: TextStyle(fontSize: 14.sp)),
        const SizedBox(width: 10),
        Text(controller.state.currentMonth, style: TextStyle(fontSize: 16.sp)),
      ],
    );
  }
}
