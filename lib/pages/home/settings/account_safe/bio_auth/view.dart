import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:local_auth/local_auth.dart';

import '../../../../../common/widgets/index.dart';
import 'index.dart';

class BioAuthPage extends GetView<BioAuthController> {
  const BioAuthPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView(BuildContext context) {
    return Obx(() {
      String imgPath = 'bio_lock.png';
      if (controller.state.bioType.value == BiometricType.face) {
        imgPath = 'bio_faceid.png';
      } else if (controller.state.bioType.value == BiometricType.fingerprint) {
        imgPath = 'bio_touchid.png';
      }
      return InkWell(
        onTap: () => controller.bioAuth(),
        child: Center(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
              AssetsImageView(
                imgPath,
                width: 84.w,
                height: 84.w,
              ),
              SizedBox(height: 10.h),
              Text(controller.state.title,
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge
                      ?.copyWith(color: Theme.of(context).colorScheme.primary))
              // SizedBox(
              //   width: double.infinity,
              //   height: 42.h,
              //   child: O2ElevatedButton(() => controller.bioAuth(),
              //       Text(controller.state.title)),
              // )
            ])),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BioAuthController>(
      builder: (_) {
        return Scaffold(
          body: SizedBox(
            width: double.infinity,
            height: double.infinity,
            child: Stack(alignment: Alignment.center, children: [
              const Positioned.fill(
                child: AssetsImageView(
                  'launch_background.png',
                  fit: BoxFit.cover,
                ),
              ),
              Positioned.fill(child: _buildView(context))
            ]),
          ),
        );
      },
    );
  }
}
