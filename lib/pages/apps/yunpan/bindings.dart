import 'package:get/get.dart';

import 'controller.dart';

class YunpanBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<YunpanController>(() => YunpanController());
  }
}
