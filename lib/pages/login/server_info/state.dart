import 'package:get/get.dart';

class ServerInfoState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // 协议
  final _protocol = "".obs;
  set protocol(String value) => _protocol.value = value;
  String get protocol => _protocol.value;


  // 是否有不同的配置
  final _notSameConfig = false.obs;
  set notSameConfig(bool value) => _notSameConfig.value = value;
  bool get notSameConfig => _notSameConfig.value;
}
