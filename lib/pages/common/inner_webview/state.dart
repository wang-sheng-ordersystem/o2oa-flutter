import 'package:get/get.dart';

class InnerWebviewState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  final _openUri = Rx<Uri?>(null);
  set openUri(Uri? value) => _openUri.value = value;
  Uri? get openUri => _openUri.value;
}
