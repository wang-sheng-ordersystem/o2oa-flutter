import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'index.dart';
import 'widgets/widgets.dart';

class BindPage extends GetView<BindController> {
  const BindPage({Key? key}) : super(key: key);

  // 内容页
  Widget stepTwoWidget() {
    return const TwoStepWidget();
  }

  Widget _stepOneWidget() {
    return const OneStepWidget();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BindController>(
      builder: (_) {
        return Scaffold(
          body: Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: Obx(() => controller.state.isStepOne
                  ? _stepOneWidget()
                  : stepTwoWidget())),
        );
      },
    );
  }
}
