import 'package:context_menus/context_menus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';
import 'widgets/widgets.dart';

class ImChatPage extends GetView<ImChatController> {
  const ImChatPage({Key? key}) : super(key: key);

  static Future<void> open(String conversationId, {String title = ''}) async {
    await Get.toNamed(O2OARoutes.homeImChat,
        arguments: {"conversationId": conversationId, "title": title});
  }

  // 主视图
  Widget _buildView(BuildContext context) {
    return Column(
      children: [
        const Expanded(flex: 1, child: ChatMsgListWidget()),
        _bottomBarView(context)
      ],
    );
  }

  ///
  /// 底部操作栏
  Widget _bottomBarView(BuildContext context) {
    return Container(
        color: Theme.of(context).colorScheme.background,
        child: Column(
          children: [
            const Divider(height: 1, color: AppColor.borderColor),
            const SizedBox(height: 5),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(width: 8),
                // 桌面端不支持语音消息
                GetPlatform.isMobile
                    ? _iconBtn(Icons.mic, controller.clickVoiceBtn)
                    : Container(),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 5.w, right: 5.w),
                    child: TextField(
                      autofocus: false,
                      focusNode: controller.chatInputNode,
                      controller: controller.chatInputController,
                      decoration: InputDecoration(
                          isDense: true,
                          hintText: 'im_chat_input_hint'.tr,
                          hintStyle: Theme.of(context).textTheme.bodySmall,
                          filled: true,
                          fillColor: Theme.of(context).colorScheme.background,
                          border: _myBorder(),
                          enabledBorder: _myBorder(),
                          disabledBorder: _myBorder(),
                          errorBorder: _myBorder(),
                          focusedErrorBorder: _myBorder(),
                          focusedBorder: _myBorder()),
                      style: Theme.of(context).textTheme.bodyLarge,
                      textInputAction: TextInputAction.send,
                      onSubmitted: controller.onSendText,
                    ),
                  ),
                ),
                // _imgBtn('chat_emoji.png', controller.clickEmojiBtn),
                // _imgBtn('chat_more_tools.png', controller.clickMoreBtn),
                _iconBtn(Icons.sentiment_satisfied_alt, controller.clickEmojiBtn),
                _iconBtn(Icons.control_point, controller.clickMoreBtn),
                const SizedBox(width: 8),
              ],
            ),
            _toolView(),
            const SizedBox(height: 5)
          ],
        ));
  }

  /// 工具区域
  /// 显示扩展工具、表情、语音发送等工具的区域
  Widget _toolView() {
    return Obx(() {
      if (controller.state.toolBoxType == 1) {
        // 语音
        return const SizedBox(
          height: 150,
          child: ImVoiceRecordToolView(),
        );
      } else if (controller.state.toolBoxType == 2) {
        // 表情
        return const SizedBox(
          height: 250,
          child: EmojiListWidget(),
        );
      } else if (controller.state.toolBoxType == 3) {
        // 更多
        return const SizedBox(
          height: 100,
          child: ImMoreToolListWidget(),
        );
      }
      return Container();
    });
  }

  Widget _imgBtn(String assetsImageName, GestureTapCallback tap) {
    return SizedBox(
      width: 36.w,
      height: 36.w,
      child: InkWell(
        onTap: tap,
        child: AssetsImageView(assetsImageName),
      ),
    );
  }
  /// 按钮
  Widget _iconBtn(IconData icon, GestureTapCallback tap) {
    return IconButton(onPressed: tap, icon: Icon(icon, size: 36.w));
  }

  /// 输入框样式
  OutlineInputBorder _myBorder() {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(50),
        borderSide: const BorderSide(color: AppColor.borderColor));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ImChatController>(
      builder: (_) {
        return Obx(() => Scaffold(
            appBar: AppBar(
              title: Text(controller.state.title),
              actions: [
                PopupMenuButton(
                  itemBuilder: (ctx) {
                    return controller.state.menuList.map((element) {
                      return PopupMenuItem(
                        child: Text(element.name),
                        onTap: () => controller.clickActionMenu(element),
                      );
                    }).toList();
                  },
                )
              ],
            ),
            body: SafeArea(
              child: ContextMenuOverlay(
                  child: Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: GestureDetector(
                    onTap: controller.closeSoftInputAndOtherTools,
                    child: _buildView(context)),
              )),
            )));
      },
    );
  }
}
