 class CloudDiskShareForm {
         String? shareType  = "member"; //分享类型 member
         String? fileId ; //分享的文档id或者文件夹id
         List<String>? shareUserList; //分享给的用户列表
         List<String>? shareOrgList; //分享给的组织列表
    CloudDiskShareForm({
      this.shareType,
      this.fileId,
      this.shareOrgList,
      this.shareUserList
    });
 

    Map<String, dynamic> toJson() => {
      "shareType": shareType,
      "fileId": fileId,
      "shareOrgList": shareOrgList ?? [],
      "shareUserList": shareUserList ?? [],
    };

 }