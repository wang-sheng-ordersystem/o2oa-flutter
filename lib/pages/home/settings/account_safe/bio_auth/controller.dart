import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:local_auth/local_auth.dart';
import 'package:local_auth_android/local_auth_android.dart';
import 'package:local_auth_ios/local_auth_ios.dart';

import '../../../../../common/routers/index.dart';
import '../../../../../common/utils/index.dart';
import 'index.dart';

class BioAuthController extends GetxController {
  BioAuthController();

  final state = BioAuthState();
  final LocalAuthentication auth = LocalAuthentication();
 

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    state.title = 'account_safe_local_auth_btn_title'.tr;
    _init();
    super.onReady();
  }

  Future<void> _init() async {
    final typeList = await _loadBiometricTypes();
    OLogger.d(typeList.length);
    if (typeList.contains(BiometricType.face)) {
      state.title = 'account_safe_local_auth_faceid_btn_title'.tr;
      state.bioType.value = BiometricType.face;
    } else if (typeList.contains(BiometricType.fingerprint)) {
      state.title = 'account_safe_local_auth_touchid_btn_title'.tr;
      state.bioType.value = BiometricType.fingerprint;
    } else if (typeList.contains(BiometricType.strong)) {
      state.bioType.value = BiometricType.strong;
    }
  }

  Future<List<BiometricType>> _loadBiometricTypes() async {
    final deviceInfoPlugin = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      final android = await deviceInfoPlugin.androidInfo;
      OLogger.d(android);
      //  API 29 (Android Q) 
      if ((android.version.sdkInt ?? 0) >= 29 ) {
        return await auth.getAvailableBiometrics();
      }
    } else if (Platform.isIOS) {
      return await auth.getAvailableBiometrics();
    }
    return [];
  }
 
  Future<void> bioAuth() async {
    OLogger.d('点击了认证诶');
    try {
      final didAuthenticate = await auth.authenticate(
          localizedReason: 'account_safe_local_auth_open_app'.tr,
          authMessages: [
            AndroidAuthMessages(
              biometricRequiredTitle: 'account_safe_local_auth_biometric_required'.tr,
              goToSettingsDescription: 'account_safe_local_auth_android_description'.tr,
              cancelButton: 'cancel'.tr,
              goToSettingsButton: 'account_safe_local_auth_go_to_setting'.tr,
              biometricNotRecognized: 'account_safe_local_auth_not_recognized'.tr,
              biometricHint: 'account_safe_local_auth_android_bio_hint'.tr,
              signInTitle: 'account_safe_local_auth_android_bio_sign_in_title'.tr
            ),
            IOSAuthMessages(
              goToSettingsButton: 'account_safe_local_auth_go_to_setting'.tr,
              goToSettingsDescription: 'account_safe_local_auth_ios_description'.tr,
              cancelButton: 'cancel'.tr,
            )
          ],
          options: const AuthenticationOptions(biometricOnly: true));
      if (didAuthenticate) {
        Get.offNamed(O2OARoutes.home);
      }
    } on PlatformException catch (e) {
      OLogger.e('认证失败', e);
    }
  }
}
