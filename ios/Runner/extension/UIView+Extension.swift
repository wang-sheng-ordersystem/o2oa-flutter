//
//  UIView+Extension.swift
//  Runner
//
//  Created by FancyLou on 2023/5/23.
//

import Foundation
import UIKit


// MARK: Gesture Extensions
extension UIView {
    
    
    /// EZSwiftExtensions - Make sure you use  "[weak self] (gesture) in" if you are using the keyword self inside the closure or there might be a memory leak
    public func addTapGesture(tapNumber: Int = 1, action: ((UITapGestureRecognizer) -> Void)?) {
        let tap = BlockTap(tapCount: tapNumber, fingerCount: 1, action: action)
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
    
}
