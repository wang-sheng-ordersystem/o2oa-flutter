



import '../models/index.dart';
import 'log_util.dart';
import 'o2_api_manager.dart';

class O2ContactPermissionManager {
  static final O2ContactPermissionManager instance = O2ContactPermissionManager._internal();
  factory O2ContactPermissionManager() => instance;
  O2ContactPermissionManager._internal();



    ContactPermission? data;
    // 隐藏手机号码的人员列表
    List<String> hideMobilePersons  = [];
    // 不查询的人员列表
    List<String>  excludePersons = [];
    // 不查询的组织列表
    List<String>  excludeUnits = [];
    // 不允许查询通讯录的人员
    List<String>  limitAll = [];
    // 不允许查看外部门 包含人员和组织
    List<String>  limitOuter = [];


  void initData(ContactPermission permissionData) {
    data = permissionData;
    hideMobilePersons.clear();
    hideMobilePersons.addAll( permissionData.hideMobilePerson?.split(",").toList() ?? []);
    excludePersons.clear();
    excludePersons.addAll(permissionData.excludePerson?.split(",").toList() ?? []);
    excludeUnits.clear();
    excludeUnits.addAll(permissionData.excludeUnit?.split(",").toList() ?? []);
    limitAll.clear();
    limitAll.addAll(permissionData.limitQueryAll?.split(",").toList() ?? []);
    limitOuter.clear();
    limitOuter.addAll(permissionData.limitQueryOuter?.split(",").toList() ?? []);
    OLogger.d('初始化通讯录权限完成！${permissionData.toJson()}');
  }



    ///
    /// 判断 传入的人员是否需要隐藏手机号码
    /// @param person 程剑@chengjian@P
    ///
    bool isHiddenMobile(String person) {
      if (person.isEmpty) return false;
      return hideMobilePersons.contains(person);
    }

    ///
    /// 判断 传入的人员是否要排除
    /// @param person 程剑@chengjian@P
    ///
    bool isExcludePerson(String person) {
      if (person.isEmpty) return false;
      return excludePersons.contains(person);
    }

    ///
    /// 判断 传入的组织是否要排除
    ///@param unit 团队领导@b7e3a8d3-21d4-4802-babf-9fc85392333d@U
    ///
    bool isExcludeUnit(String unit ) {
      if (unit.isEmpty) return false;
      return excludeUnits.contains(unit);
    }

    ///
    /// 判断 当前用户是否不能查询通讯录
    ///
    bool isCurrentPersonCannotQueryAll() {
        var currentDN = O2ApiManager.instance.o2User?.distinguishedName;
        if (currentDN == null || currentDN.isEmpty) {
            return false;
        }
        return limitAll.contains(currentDN);
    }

    ///
    /// 判断 当前用户是否不能查询外部门
    ///
    bool isCurrentPersonCannotQueryOuter() {
        OLogger.d('不能查询外部门 $limitOuter');
        var currentPersonDN = O2ApiManager.instance.o2User?.distinguishedName;
        if (currentPersonDN == null || currentPersonDN.isEmpty) {
            return false;
        }
        OLogger.d('当前 dn $currentPersonDN' );
        if (limitOuter.contains(currentPersonDN))  { 
          return true;
        }
        // limitOuter还包含了部门数据
        final unitList = O2ApiManager.instance.myUnitList;
        OLogger.d("我的部门 ${unitList.length}");
        for (var element in unitList) {
          OLogger.d('匹配部门 ${element.distinguishedName}');
          if (limitOuter.contains(element.distinguishedName)){
            return true;
          }
        }
        return false;
    }

}