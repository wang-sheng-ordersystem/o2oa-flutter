
import 'dart:convert';

import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';



class SharedPreferenceService extends GetxService {
  static SharedPreferenceService get to => Get.find();
  late final SharedPreferences _prefs;

  //// sp key
  static const String isAgreePrivacyKey = 'isAgreePrivacyKey'; // 是否已经同意隐私政策
  static const String unitSpKey = 'O2_UNIT_KEY'; // 默认服务器信息key
  static const String directUnitSpKey = 'O2_SAMPLE_UNIT_KEY'; // 如果是直连模式 存储当前服务器信息的key
  static const String centerServerSpKey = 'O2_CENTER_SERVER_KEY'; // 中心服务器地址信息
  static const String appStyleSpKey = 'O2_APP_STYLE_KEY'; // 当前服务器appStyle对象存储key
  static const String appStyleNeedGreyKey = ''; // 是否需要全局变黑白
  static const String userSpKey = 'userSpKey';
  static const String jpushDeviceIdSpKey = 'jpushDeviceIdSpKey'; // 极光推送设备id存储key
  static const String appThemeModeKey = 'appThemeModeKey'; // 主题模式
  static const String appThemeSkinKey = 'appThemeSkinKey';// 主题皮肤
  static const String meetingConfigKey = 'meetingConfigKey'; // 会议管理配置文件存储key
  static const String currentDeviceTypeKey = 'currentDeviceTypeKey'; // 当前设备类型的key
  static const String v3PanAppIsInstallKey = 'v3PanAppKey'; // v3网盘应用是否已经安装
  static const String v2AttendanceKey = 'v2AttendanceKey'; // 新版考勤关闭旧版考勤
  static const String searchHistoryKey = 'searchHistoryKey'; // 首页 搜索历史存储 key
  static const String appUpdateTodayKey = 'appUpdateTodayKey'; // app更新每天检查一次
  static const String appUpdateUserSwitchKey = 'appUpdateUserSwitchKey'; // app更新 用户开关
  static const String webviewDebuggerKey = 'webviewDebuggerKey'; // webview 后面添加 debugger
  static const String languageSaveKey = 'languageSaveKey'; //  用户手动设置的语言
  static const String appBiometricAuthKey = 'appBiometricAuthKey'; //  生物识别认证开关
  static const String promotionPageShowDateKey = 'promotionPageShowDateKey'; //  推广页展现时间


  Future<SharedPreferenceService> init() async {
    _prefs = await SharedPreferences.getInstance();
    return this;
  }

  Future<bool> putString(String key, String? stringValue) async {
    return await _prefs.setString(key, stringValue ?? '');    
  }

  /// 把对象转成Json字符串保存在SharedPreferences中
  Future<bool> putStringToJsonStringify(String key, dynamic value) async {
    String valueStr = json.encode(value);
    return await  _prefs.setString(key, valueStr);    
  }

  Future<bool> putBool(String key, bool value) async {
    return await _prefs.setBool(key, value);
  }

  Future<bool> putList(String key, List<String> value) async {
    return await _prefs.setStringList(key, value);
  }

  String getString(String key, {String defaultValue = ''}) {
    return _prefs.getString(key) ?? defaultValue;
  }

  bool getBool(String key, {bool defaultValue = false}) {
    return _prefs.getBool(key) ?? defaultValue;
  }

  List<String> getList(String key) {
    return _prefs.getStringList(key) ?? [];
  }

  Future<bool> remove(String key) async {
    return await _prefs.remove(key);
  }
}