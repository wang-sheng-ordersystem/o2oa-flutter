
import '../../api/index.dart';
import '../../routers/index.dart';
import '../../utils/index.dart';
import '../enums/index.dart';

///
/// 应用对象
///
class AppFrontData {
  AppFrontData({
    this.name,
    this.displayName,
    this.key,
    this.type,
    this.nativeEnum,
    this.portalId,
  });
  String? name; // 应用名称
  String? displayName; // 显示名称  原生应用可以配置显示名称
  String? key; // 应用key
  O2AppTypeEnum? type; // 应用类型 原生应用 还是 门户应用
  O2NativeAppEnum? nativeEnum;
  String? portalId; // 门户应用id
  int? showNumber; // 数字 

   AppFrontData.fromJson(Map<String, dynamic> jsonMap) {
    name = jsonMap['name'];
    displayName = jsonMap['displayName'];
    key = jsonMap['key'];
    type = jsonMap['type'] == null ? null : O2AppTypeEnum.values.byName(jsonMap['type']) ;
    nativeEnum = jsonMap['nativeEnum'] == null ? null : O2NativeAppEnum.values.firstWhere((element) => element.key == jsonMap['nativeEnum']);
    portalId = jsonMap['portalId'];
    showNumber = jsonMap['showNumber'];
  }

  Map<String, dynamic> toJson() => {
      "name": name,
      "displayName": displayName,
      "key": key,
      "type": type?.name,
      "nativeEnum": nativeEnum?.key,
      "portalId": portalId,
      "showNumber": showNumber,
    };

  String? get portalIconUrl {
    if (portalId == null) {
      return null;
    }
    return O2ApiManager.instance.getPortalIconUrl(portalId!);
  }

  // flutter 内部路由地址 打开应用的地址 原生应用专属
  String? get flutterPath {
    if (type == O2AppTypeEnum.native && nativeEnum != null) {
      switch (nativeEnum) {
        case O2NativeAppEnum.task:
          return O2OARoutes.appTask;
        case O2NativeAppEnum.taskcompleted:
          return O2OARoutes.appTaskcompleted;
        case O2NativeAppEnum.read:
          return O2OARoutes.appRead;
        case O2NativeAppEnum.readcompleted:
          return O2OARoutes.appReadcompleted;
        case O2NativeAppEnum.meeting:
          return O2OARoutes.appMeeting;
        case O2NativeAppEnum.bbs:
          return O2OARoutes.appBBS;
        case O2NativeAppEnum.yunpan:
          return FileAssembleService.to.isV3() ? O2OARoutes.appCloudDiskV3 : O2OARoutes.appYunpan;
        case O2NativeAppEnum.attendance:
          return  O2OARoutes.appAttendance;
        case O2NativeAppEnum.attendanceOld:
          return  O2OARoutes.appAttendanceOld;
        case O2NativeAppEnum.cms:
          return O2OARoutes.appCms;
        case O2NativeAppEnum.calendar:
          return O2OARoutes.appCalendar;
        case O2NativeAppEnum.mindMap:
          return O2OARoutes.appMindMap;
        default:
          return null;
      }
    }
    return null;
  }
}
