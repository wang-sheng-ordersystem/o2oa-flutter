
import 'dart:math' as math; // import this
import 'package:flutter/material.dart';

import '../../../../../common/models/index.dart';
import '../../../../../common/widgets/index.dart';
import 'instant_msg_body.dart';

class InstantReceivedMessageWidget extends StatelessWidget {
  final InstantMsg msgBody;
  const InstantReceivedMessageWidget({
    Key? key,
    required this.msgBody,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final messageTextGroup = Flexible(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationY(math.pi),
              child: CustomPaint(
                painter: ImBubbleShape(Colors.grey[300]!),
              ),
            ),
            Flexible(
              child: Container(
                padding: const EdgeInsets.all(14),
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(18),
                    bottomLeft: Radius.circular(18),
                    bottomRight: Radius.circular(18),
                  ),
                ),
                child: InstantMsgBodyWidget(msg: msgBody),
              ),
            ),
          ],
        ));

    return Padding(
      padding: const EdgeInsets.only(right: 0, left: 10, top: 10, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          const SizedBox(height: 30),
          messageTextGroup,
        ],
      ),
    );
  }
}