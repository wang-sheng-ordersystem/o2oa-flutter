
class WorkAttachmentInfo{
  WorkAttachmentInfo({
        this.id,
        this.createTime,
        this.updateTime,
        this.name,
        this.extension,
        this.storage,
        this.length,
        this.workCreateTime,
        this.application,
        this.process,
        this.job,
        this.person,
        this.lastUpdateTime,
        this.lastUpdatePerson,
        this.activity,
        this.activityName,
        this.activityType,
        this.activityToken,
        this.site
    });
    String? id;
    String? createTime;
    String? updateTime;
    String? name;
    String? extension;
    String? storage;
    int? length;
    String? workCreateTime;
    String? application;
    String? process;
    String? job;
    String? person;
    String? lastUpdateTime;
    String? lastUpdatePerson;
    String? activity;
    String? activityName;
    String? activityType;
    String? activityToken;
    String? site;
    factory WorkAttachmentInfo.fromJson(Map<String, dynamic> json) => WorkAttachmentInfo(
        id: json["id"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        name: json["name"],
        extension: json["extension"],
        storage: json["storage"],
        length: json["length"],
        workCreateTime: json["workCreateTime"],
        application: json["application"],
        process: json["process"],
        job: json["job"],
        person: json["person"],
        lastUpdateTime: json["lastUpdateTime"],
        lastUpdatePerson: json["lastUpdatePerson"],
        activity: json["activity"],
        activityType: json["activityType"],
        activityName: json["activityName"],
        activityToken: json["activityToken"],
        site: json["site"]
    );
}