import '../../../common/models/process/index.dart';

enum CreateFormBackResultType { cmsDoc, processWork, processDraft }

class CreateFormBackCmsDoc {
  String? id;
  String? title;

  CreateFormBackCmsDoc({this.id, this.title});
  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
      };
}

class CreateFormBackProcessWork {
  String? work;
  String? job;
  String? title;

  CreateFormBackProcessWork({this.work, this.job, this.title});

  Map<String, dynamic> toJson() => {
        "work": work,
        "job": job,
        "title": title,
      };
}

/// 表单返回结果对象
class CreateFormBackResult {
  CreateFormBackResultType resultType;

  CreateFormBackCmsDoc? cmsDoc;

  CreateFormBackProcessWork? processWork;

  ProcessDraftWorkData? processDraft;

  CreateFormBackResult(this.resultType,
      {this.cmsDoc, this.processWork, this.processDraft});

  Map<String, dynamic> toJson() => {
        "resultType": resultType.name,
        "cmsDoc": cmsDoc?.toJson(),
        "processWork": processWork?.toJson(),
        "processDraft": processDraft?.toJson(),
      };
}
