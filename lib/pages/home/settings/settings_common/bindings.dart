import 'package:get/get.dart';

import 'controller.dart';

class SettingsCommonBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SettingsCommonController>(() => SettingsCommonController());
  }
}
