import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../common/api/index.dart';
import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/utils/index.dart';
import '../../../../common/values/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class MeetingDetailPage extends GetView<MeetingDetailController> {
  const MeetingDetailPage({Key? key}) : super(key: key);

  ///
  /// 打开会议详情
  /// 如果是在线会议 在外部浏览器链接
  /// 
  static Future<void> openMeeting(String id) async {
    final meeting = await MeetingAssembleService.to.getMeeting(id);
    if (meeting == null) {
      Loading.toast('meeting_detail_null_object_error'.tr);
      return;
    }
    // if (meeting.mode == 'online' && meeting.roomLink?.isNotEmpty == true) {
    //   Uri uri = Uri.parse(meeting.roomLink!);
    //   if (await canLaunchUrl(uri)) {
    //     await launchUrl(uri, mode: LaunchMode.externalApplication);
    //   }
    // } else {
    openMeetingInfo(meeting);
    // }
  }

  /// 
  /// 打开内部的会议详情
  /// 
  static void openMeetingInfo(MeetingInfoModel meeting) {
    Get.toNamed(O2OARoutes.appMeetingDetail, arguments: meeting);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MeetingDetailController>(
      builder: (_) {
        return Scaffold(
          appBar:
              AppBar(title: Text("meeting_menu_view_label".tr)),
          body: SafeArea(
            child: ListView(children: [
              const SizedBox(height: 8),
              O2UI.sectionOutBox(Obx(() {
                final time =
                    '${controller.state.meetingInfo.value?.startTime?.substring(11, 16)} 至 ${controller.state.meetingInfo.value?.completedTime?.substring(11, 16)}';
                return Column(children: [
                  O2UI.lineWidget('meeting_detail_title_label'.tr,
                      Text(controller.state.meetingInfo.value?.subject ?? '')),
                  const Divider(height: 1),
                  O2UI.lineWidget(
                      'meeting_detail_date_label'.tr,
                      Text(controller.state.meetingInfo.value?.startTime
                              ?.substring(0, 10) ??
                          '')),
                  const Divider(height: 1),
                  O2UI.lineWidget('meeting_detail_time_label'.tr, Text(time)),
                  const Divider(height: 1),
                  O2UI.lineWidget(
                      'meeting_detail_room_label'.tr,
                      Text(controller.state.meetingInfo.value?.woRoom?.name ??
                          '')),
                ]);
              }), context),
              const SizedBox(height: 8),
              O2UI.sectionOutBox(Obx(() {
                final hostPerson = controller
                        .state.meetingInfo.value?.hostPerson
                        ?.o2NameCut() ??
                    '';
                final hostUnit = controller.state.meetingInfo.value?.hostUnit?.o2NameCut() ??
                    '';
                return Column(children: [
                  O2UI.lineWidget('meeting_detail_type_label'.tr,
                      Text(controller.state.meetingInfo.value?.type ?? '')),
                  const Divider(height: 1),
                  O2UI.lineWidget('meeting_detail_mode_label'.tr,
                      Text(controller.state.meetingInfo.value?.mode == MeetingMode.online.getKey() ? MeetingMode.online.getName() : MeetingMode.offline.getName())),
                  const Divider(height: 1), 
                  
                  Obx(()=>Visibility(
                    visible: controller.state.meetingInfo.value?.mode == MeetingMode.online.getKey(),
                    child: O2UI.lineWidget('meeting_detail_online_link_label'.tr,
                                Text(controller.state.meetingInfo.value?.roomLink ?? '' ,style: const TextStyle(decoration: TextDecoration.underline),), ontap: () => controller.openOnlineRoomLink(),))),
                  Obx(()=>Visibility(
                    visible: controller.state.meetingInfo.value?.mode == MeetingMode.online.getKey(),
                    child:  const Divider(height: 1) )),

                  Obx(()=>Visibility(
                    visible: controller.state.meetingInfo.value?.mode == MeetingMode.online.getKey(),
                    child: O2UI.lineWidget('meeting_detail_online_room_label'.tr,
                                Text(controller.state.meetingInfo.value?.roomId ?? '' )))),
                  Obx(()=>Visibility(
                    visible: controller.state.meetingInfo.value?.mode == MeetingMode.online.getKey(),
                    child:  const Divider(height: 1) )),
                  
                  O2UI.lineWidget(
                      'meeting_detail_host_label'.tr, Text(hostPerson)),
                  const Divider(height: 1),
                  O2UI.lineWidget(
                      'meeting_detail_host_unit_label'.tr, Text(hostUnit)),
                  const Divider(height: 1),
                  O2UI.lineWidget('meeting_detail_describe_label'.tr,
                      Flexible(child: Text(controller.state.meetingInfo.value?.summary ?? '' ,textAlign: TextAlign.end,))),
                ]);
              }), context),
              const SizedBox(height: 8),
              O2UI.sectionOutBox(Obx(() {
                final members =
                    controller.state.meetingInfo.value?.inviteMemberList ?? [];
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('meeting_detail_member_label'.tr),
                    const SizedBox(height: 5),
                    Padding(
                        padding: const EdgeInsets.all(10),
                        child: GridView.count(
                            shrinkWrap: true,
                            crossAxisCount: 4,
                            mainAxisSpacing:
                                8.0, //设置上下之间的间距(Axis.horizontal设置左右之间的间距)
                            crossAxisSpacing:
                                8.0, //设置左右之间的间距(Axis.horizontal设置上下之间的间距)
                            childAspectRatio: 1.8, //设置宽高比
                            children: members.map((element) {
                              final name = element.o2NameCut();
                              return Center(child: Text(
                                name,
                                style: const TextStyle(fontSize: 16),
                              ));
                            }).toList()))
                  ],
                );
              }), context),
              const SizedBox(height: 8),
              O2UI.sectionOutBox(Obx(() {
                final attachments =
                    controller.state.meetingInfo.value?.attachmentList ?? [];
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('meeting_detail_attachment_label'.tr),
                    const SizedBox(height: 5),
                    Padding(
                        padding: const EdgeInsets.all(10),
                        child: GridView.count(
                            shrinkWrap: true,
                            crossAxisCount: 4,
                            mainAxisSpacing:
                                8.0, //设置上下之间的间距(Axis.horizontal设置左右之间的间距)
                            crossAxisSpacing:
                                8.0, //设置左右之间的间距(Axis.horizontal设置上下之间的间距)
                            childAspectRatio: 0.8, //设置宽高比
                            children: attachments.map((element) {
                              final name = element.name ?? '';
                              String fileIcon =
                                  FileIcon.getFileIconAssetsNameByExtension(
                                      element.extension ?? '');
                              return Column(
                                children: [
                                  AssetsImageView(fileIcon,
                                      width: 40, height: 40),
                                  Expanded(
                                      flex: 1,
                                      child: Center(
                                          child: Text(
                                        name,
                                        style: const TextStyle(fontSize: 12),
                                      )))
                                ],
                              );
                            }).toList()))
                  ],
                );
              }), context),
            ]),
          ),
        );
      },
    );
  }
}
