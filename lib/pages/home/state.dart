
import 'package:get/get.dart';

import '../../common/models/index.dart';


class HomeState {
  // title
  final _title = "首页".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

 
  // 是否显示bottomBar
  final _showBottomBar = false.obs;
  set showBottomBar(value) => _showBottomBar.value = value;
  get showBottomBar => _showBottomBar.value;

  // 当前 bottombar 页码
  final _currentIndex = 0.obs;
  set currentIndex(int value) => _currentIndex.value = value;
  int get currentIndex => _currentIndex.value;
  
 
  // 未读消息数量
  final _unReadNumber = 0.obs;
  set unReadNumber(int v) => _unReadNumber.value = v;
  int get unReadNumber =>  _unReadNumber.value;

  // 底部
  final homeTabList = <AppIndexModule>[].obs;

}
