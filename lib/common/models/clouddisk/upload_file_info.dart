
import 'dart:io';

import '../../extension/index.dart';

class CloudDiskUploadFileInfo {
  File? file;
  String? folderId; // 上传的文件夹
  bool? isV3; // 目前有两个上传接口 是原来的 file 还是  pan 

  String? id; // file.path + folderId  md5
  String? name; // 文件名称
  int? length; // 文件大小
  double? progress; // 上传进度
  DateTime? createTime; //
  DateTime? finishTime; // 上传结束时间

  CloudDiskUploadFileInfo({
    this.file,
    this.folderId,
    this.isV3
  }): name = file?.filename() ?? '' , length = file?.lengthSync() ?? 0, progress = 0, createTime = DateTime.now();


  @override
  String toString() {
    return 'CloudDiskUploadFileInfo(id: $id, file: ${file?.path ?? ''} folderId: $folderId, isV3: $isV3, name: $name, length: $length, progress: $progress, createTime: $createTime finishTime: $finishTime)';
  }
}