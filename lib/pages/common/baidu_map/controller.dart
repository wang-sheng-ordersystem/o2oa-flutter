
// import 'package:flutter/material.dart';
// import 'package:flutter_baidu_mapapi_base/flutter_baidu_mapapi_base.dart';
// import 'package:flutter_baidu_mapapi_map/flutter_baidu_mapapi_map.dart';
// import 'package:flutter_baidu_mapapi_search/flutter_baidu_mapapi_search.dart';
// import 'package:flutter_bmflocation/flutter_bmflocation.dart';
// import 'package:get/get.dart';

// import '../../../common/models/index.dart';
// import '../../../common/utils/index.dart';
// import 'index.dart';

// class BaiduMapController extends GetxController {
//   BaiduMapController();

//   final state = BaiduMapState();

//   // 百度地图定位插件
//   BaiduLocationHelper? _helper;
//   // 百度地图控制器
//   BMFMapController? _bmfMapController;
//   LocationData? locationData;
//   // 地图标记 发送位置使用
//   BMFMarker? _marker;

//   /// 在 widget 内存中分配后立即调用。
//   @override
//   void onInit() {
//     super.onInit();
//   }

//   /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
//   @override
//   void onReady() {
   
//     locationData = Get.arguments;
//     OLogger.d("百度地图查看： ${locationData?.mode} ${locationData?.address} ${locationData?.latitude} ${locationData?.longitude} ");
//     if (locationData != null) {
//       state.mode = locationData?.mode ?? LocationData.locationDataModePicker;
//     }
//     if (state.mode == LocationData.locationDataModeShow) {
//       // 查看地图
//       if (locationData?.latitude == null || locationData?.longitude == null) {
//         Loading.showError('bmap_error_no_location'.tr);
//       }
//     }
//     super.onReady();
//   }

//   /// 在 [onDelete] 方法之前调用。
//   @override
//   void onClose() {
//     _helper?.stopLocation();
//     super.onClose();
//   }

//   /// dispose 释放内存
//   @override
//   void dispose() {
//     super.dispose();
//   }

//   /// 
//   /// 发送位置
//   void clickSendLocation() {
//     if (locationData == null) {
//       Loading.showError('bmap_error_not_choose_location_yet'.tr);
//       return;
//     }
//     if (locationData?.latitude == null || locationData?.longitude == null) {
//        Loading.showError('bmap_error_not_choose_location_yet'.tr);
//       return;
//     }
//     Get.back(result: locationData!); // 返回结果
//   }
//   /// 打开当前位置信息 用安装的地图
//   void clickOpenLocation () {
//     if (locationData == null || locationData?.latitude == null || locationData?.longitude == null) {
//       Loading.showError('bmap_open_iosmap_nofound'.tr);
//       return;
//     }
//     MapUtils.showMapListMenu('${locationData?.longitude}', '${locationData?.latitude}');
//   }

//   /// 设置地图参数
//   BMFMapOptions initMapOptions() {
//     BMFMapOptions mapOptions = BMFMapOptions(
//       center: BMFCoordinate(39.917215, 116.380341),
//       zoomLevel: 17,
//       showZoomControl: false,
//     );
//     return mapOptions;
//   }

//   /// 地图控制器
//   void setBMFMapController(BMFMapController controller) {
//     _bmfMapController = controller;
//     _bmfMapController?.setMapDidLoadCallback(callback: () {
//       OLogger.d('地图加载完成');
//     });
//     if (state.mode == LocationData.locationDataModePicker) {
//       _bmfMapController?.showUserLocation(true);
//       //点击地图 设置位置数据 画图钉
//       _bmfMapController?.setMapOnClickedMapBlankCallback(callback: (p) {
//         _addMarkToMap(p);
//         _searchGeoAddress(p);
//       });
//       _bmfMapController?.setMapOnClickedMapPoiCallback(callback: (poi) {
//         if (poi.pt != null) {
//           _addMarkToMap(poi.pt!);
//           _searchGeoAddress(poi.pt!);
//         }
//       });
//       // 开始定位
//       _helper ??= BaiduLocationHelper(callback: (result) {
//         _locationToMap(result);
//       }, isSingleLocation: true);
//       _helper?.initLocationAndStartRequest();
//     } else if (state.mode == LocationData.locationDataModeShow) {
//       // 查看地图
//       if (locationData != null && locationData?.latitude != null && locationData?.longitude != null) {
//         var coordinate = BMFCoordinate(locationData!.latitude!, locationData!.longitude!);
//          // 必须更新中心点。。。。。
//         _bmfMapController?.updateMapOptions(BMFMapOptions(center: coordinate));
//          /// 创建BMFMarker
//         var  marker = BMFMarker.icon(icon: 'assets/images/icon_map_location.png', position: coordinate);
//         /// 添加Marker
//         _bmfMapController?.addMarker(marker);
//         state.address = locationData?.address ?? "";
//       }
      
//     }
//   }

//   /// 添加标记
//   void _addMarkToMap(BMFCoordinate coordinate) async {
//     if (_marker != null) {
//       // 先清除
//       await _bmfMapController?.removeMarker(_marker!);
//     }

//     /// 创建BMFMarker
//     _marker = BMFMarker.icon(
//         icon: 'assets/images/icon_map_location.png', position: coordinate);

//     /// 添加Marker
//     var result = await _bmfMapController?.addMarker(_marker!);
//     OLogger.d("创建地图标记： $result");
//   }

//   /// 逆地理编码检索 坐标搜索地址信息
//   void _searchGeoAddress(BMFCoordinate coordinate) async {
//     // 构造检索参数
//     BMFReverseGeoCodeSearchOption reverseGeoCodeSearchOption =
//         BMFReverseGeoCodeSearchOption(location: coordinate);
//     // 检索实例
//     BMFReverseGeoCodeSearch reverseGeoCodeSearch = BMFReverseGeoCodeSearch();
//     // 逆地理编码回调
//     reverseGeoCodeSearch.onGetReverseGeoCodeSearchResult(callback:
//         (BMFReverseGeoCodeSearchResult result, BMFSearchErrorCode errorCode) {
//       OLogger.d('逆地理编码  errorCode = $errorCode  \n result = ${result.toMap()}');
//       state.address = result.address;
//       locationData ??= LocationData(LocationData.locationDataModePicker);
//       locationData?.address = result.address;
//       locationData?.addressDetail = result.sematicDescription;
//       locationData?.latitude = result.location?.latitude;
//       locationData?.longitude = result.location?.longitude;
//     });
//     // 发起检索
//     bool flag = await reverseGeoCodeSearch
//         .reverseGeoCodeSearch(reverseGeoCodeSearchOption);
//     OLogger.d('搜索结果： $flag');
//   }

    
//   /// 定位结果显示到地图上
//   void _locationToMap(BaiduLocation loc) {
//     OLogger.d(
//         "定位信息, latitude: ${loc.latitude}, longitude: ${loc.longitude} , address:${loc.address}, locationDetail:${loc.locationDetail}");
//     if (loc.latitude != null && loc.longitude != null) {
//       var coordinate = BMFCoordinate(loc.latitude!, loc.longitude!);
//       BMFLocation location = BMFLocation(
//           coordinate: coordinate,
//           altitude: 0,
//           horizontalAccuracy: 5,
//           verticalAccuracy: -1.0,
//           speed: -1.0,
//           course: -1.0);
//       BMFUserLocation userLocation = BMFUserLocation(
//         location: location,
//       );
//       _bmfMapController?.updateLocationData(userLocation);
//       BMFUserLocationDisplayParam displayParam = BMFUserLocationDisplayParam(
//           locationViewOffsetX: 0,
//           locationViewOffsetY: 0,
//           accuracyCircleFillColor: Colors.red,
//           accuracyCircleStrokeColor: Colors.blue,
//           isAccuracyCircleShow: true,
//           locationViewImage: 'assets/images/icon_map_my_location.png',
//           locationViewHierarchy:
//               BMFLocationViewHierarchy.LOCATION_VIEW_HIERARCHY_BOTTOM);
//       _bmfMapController?.updateLocationViewWithParam(displayParam);
//       // 必须更新中心点。。。。。
//       _bmfMapController?.updateMapOptions(BMFMapOptions(center: coordinate));
//     }
//   }
 
// }
