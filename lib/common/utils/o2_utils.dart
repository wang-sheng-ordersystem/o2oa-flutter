import 'dart:convert';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../environment_config.dart';
import '../api/index.dart';
import '../routers/index.dart';
import '../services/index.dart';
import '../widgets/index.dart';
import 'loading.dart';
import 'log_util.dart';
import 'o2_api_manager.dart';
import 'webscoket_util.dart';

typedef OnFilePicker = void Function(List<String?> paths);

class O2Utils {


  static Future<void> logout({bool hasToken = true}) async {
    Loading.show();
    // 解绑设备
    // websocket 断开
    O2WebsocketClient().stop();
    if (GetPlatform.isMobile && hasToken) {
      await _unbindDeviceForJpush();
    }
    // 登出
    await OrgAuthenticationService.to.logout();
    // 清除保存的用户信息
    await O2ApiManager.instance.cleanUser();
    // 关闭极速打卡
    FastCheckInService.instance.stop();
    Loading.dismiss();
    if (EnvironmentConfig.o2AppIsDemo) {
      Get.offAllNamed(O2OARoutes.demoLogin);
    } else {
      Get.offAllNamed(O2OARoutes.login);
    }
  }

  static Future<void> _unbindDeviceForJpush() async {
    var deviceId = O2ApiManager.instance.deviceID;
    if (deviceId.isNotEmpty) {
      var result = await JpushAssembleControlService.to.unBindDevice(deviceId);
      if (result == null) {
        OLogger.e('设备号解除绑定失败！！！');
      }
    } else {
      OLogger.e('没有获取到极光推送的设备号！！！！！');
    }
  }

  static Map<String, dynamic> parseStringToJson(String jsonString) {
    try {
      final Map<String, dynamic> parsedJson = json.decode(jsonString);
      return parsedJson;
    } catch (e) {
      // JSON解析异常处理
      OLogger.e('解析JSON时出现异常: $e');
      return {};
    }
  }

  ///
  ///判断是否手机号码
  ///中国的11位手机号码 和 香港澳门的 手机号码
  static bool isMobilePhoneNumber(String phoneNumber) {
    if (phoneNumber.isEmpty) {
      return false;
    }
    return GetUtils.hasMatch(phoneNumber, r'^(1)\d{10}$') ||
        GetUtils.hasMatch(phoneNumber, r'^((\+00)?(852|853)\d{8})$');
  }

  /// 判断存储权限
  ///
  static Future<bool> storagePermission() async {
    if (Platform.isAndroid) {
      final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      AndroidDeviceInfo info = await deviceInfoPlugin.androidInfo;
      final sdk = info.version.sdkInt;
      if (sdk >= 33) {
        // Android sdk 33 以后没有storage权限了
        return true;
      }
    }
    var status = await Permission.storage.status;
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      if (Platform.isAndroid) {
        O2OverlayEntryDialog.instance.openAlertDialog(
            'common_permission_storage_request_title'.tr,
            'common_permission_storage_request_content'.tr);
      }
      status = await Permission.storage.request();
      if (Platform.isAndroid) {
        O2OverlayEntryDialog.instance.closeDiloag();
      }
      if (status == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  /// 摄像头权限判断
  static Future<bool> cameraPermission() async {
    var status = await Permission.camera.status;
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      if (Platform.isAndroid) {
        O2OverlayEntryDialog.instance.openAlertDialog(
            'common_permission_camera_request_title'.tr,
            'common_permission_camera_request_content'.tr);
      }
      status = await Permission.camera.request();
      if (Platform.isAndroid) {
        O2OverlayEntryDialog.instance.closeDiloag();
      }
      if (status == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  /// 定位权限判断
  static Future<bool> locationPermission() async {
    var status = await Permission.location.status;
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      if (Platform.isAndroid) {
        O2OverlayEntryDialog.instance.openAlertDialog(
            'common_permission_location_request_title'.tr,
            'common_permission_location_request_content'.tr);
      }
      status = await Permission.location.request();
      if (Platform.isAndroid) {
        O2OverlayEntryDialog.instance.closeDiloag();
      }
      if (status == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  /// 麦克风权限判断
  static Future<bool> microphonePermission() async {
    var status = await Permission.microphone.status;
    if (status == PermissionStatus.granted) {
      return true;
    } else {
      if (Platform.isAndroid) {
        O2OverlayEntryDialog.instance.openAlertDialog(
            'common_permission_microphone_request_title'.tr,
            'common_permission_microphone_request_content'.tr);
      }
      status = await Permission.microphone.request();
      if (Platform.isAndroid) {
        O2OverlayEntryDialog.instance.closeDiloag();
      }
      if (status == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }

  /// 文件选择器
  /// @param callback  返回选择的文件结果
  static void pickerFileOrImage(OnFilePicker callback,
      {allowMultiple = false}) {
    // ios 可以打开相册
    final context = Get.context;
    if (context == null) {
      callback([]);
      return;
    }
    List<Widget> menus = [];
    if (Platform.isIOS) {
      menus = [
        ListTile(
          onTap: () {
            Navigator.pop(context);
            _takePhoto(callback);
          },
          title: Align(
            alignment: Alignment.center,
            child: Text('take_photo'.tr,
                style: Theme.of(context).textTheme.bodyMedium),
          ),
        ),
        const Divider(height: 1),
        ListTile(
          onTap: () {
            Navigator.pop(context);
            _pickImage(callback);
          },
          title: Align(
            alignment: Alignment.center,
            child:
                Text('album'.tr, style: Theme.of(context).textTheme.bodyMedium),
          ),
        ),
        const Divider(height: 1),
        ListTile(
          onTap: () {
            Navigator.pop(context);
            _pickFiles(callback, allowMultiple);
          },
          title: Align(
            alignment: Alignment.center,
            child: Text('pick_file'.tr,
                style: Theme.of(context).textTheme.bodyMedium),
          ),
        ),
      ];
    } else {
      menus = [
        ListTile(
          onTap: () {
            Navigator.pop(context);
            _takePhoto(callback);
          },
          title: Align(
            alignment: Alignment.center,
            child: Text('take_photo'.tr,
                style: Theme.of(context).textTheme.bodyMedium),
          ),
        ),
        const Divider(height: 1),
        ListTile(
          onTap: () {
            Navigator.pop(context);
            _pickFiles(callback, allowMultiple);
          },
          title: Align(
            alignment: Alignment.center,
            child: Text('pick_file'.tr,
                style: Theme.of(context).textTheme.bodyMedium),
          ),
        ),
      ];
    }
    O2UI.showBottomSheetWithCancel(context, menus);
  }

  /// 直接拍照
  static void _takePhoto(OnFilePicker callback) async {
    if (!await O2Utils.cameraPermission()) {
      callback([]);
      return;
    }
    XFile? file = await ImagePicker().pickImage(source: ImageSource.camera);
    if (file != null) {
      callback([file.path]);
    } else {
      callback([]);
    }
  }

  /// 相册
  static void _pickImage(OnFilePicker callback) async {
    XFile? file = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (file != null) {
      callback([file.path]);
    } else {
      callback([]);
    }
  }

  /// 文件选择
  static void _pickFiles(OnFilePicker callback, bool allowMultiple) async {
    if (!await O2Utils.storagePermission()) {
      callback([]);
      return;
    }
    FilePickerResult? result =
        await FilePicker.platform.pickFiles(allowMultiple: allowMultiple);
    if (result != null && result.paths.isNotEmpty) {
      callback(result.paths);
    } else {
      callback([]);
    }
  }
}
