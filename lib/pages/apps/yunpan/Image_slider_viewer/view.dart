import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';
import 'widgets/image_loader/index.dart';

class ImageSliderViewerPage extends GetView<ImageSliderViewerController> {
  const ImageSliderViewerPage({Key? key}) : super(key: key);

  /// 打开图片查看器
  /// [imageList] list 中的 FileInfo 对象需要传入  isV3 参数 才能正确的进行图片下载
  static Future<void> openViewer(List<FileInfo> imageList,
      {String? currentFileId}) async {
    await Get.toNamed(O2OARoutes.appCloudDiskImageSlideViewer,
        arguments: {'imageList': imageList, 'currentFileId': currentFileId});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ImageSliderViewerController>(
      builder: (_) {
        return Scaffold(
            body: SafeArea(
                child: Obx(() => Container(
                    constraints: BoxConstraints.expand(
                      height: MediaQuery.of(context).size.height,
                    ),
                    child: Stack(children: <Widget>[
                      Positioned(
                        top: 0,
                        left: 0,
                        bottom: 0,
                        right: 0,
                        child: PageView.builder(
                        controller: controller.pageController,
                        itemCount: controller.state.imageFileList.length,
                        onPageChanged: (value) => controller.pageChanged(value),
                        itemBuilder: ((context, index) {
                          final item = controller.state.imageFileList[index];
                          return ImageLoaderPage(fileInfo: item, tag: item.id);
                        }),
                      )),
                      Positioned( // 中间数字
                        left: 10.w,
                        right: 10.w,
                        top: 10.w,
                        child: Obx(()=> Center(child: O2UI.blackTransparentCard(
                          Center(child:Text(controller.state.title, style: AppTheme.whitePrimaryTextStyle,)),
                          width: 32.w,
                          height: 24.w
                        ))),
                      ),
                      Positioned( //关闭按钮
                        left: 10.w,
                        top: 10.w,
                        child: O2UI.blackTransparentCard(
                          Center(child: IconButton(
                            icon: Icon(Icons.close,size: 30.w,color: Colors.white,),
                            onPressed: (){
                                Get.back();
                            },
                        )), width: 48.w, height: 48.w),
                      )
                    ])))));
      },
    );
  }
 
}
