/// O2OA 后端功能模块
enum O2DistributeModuleEnum {
  x_file_assemble_control,//云文件
  x_meeting_assemble_control,//会议管理
  x_attendance_assemble_control,//考勤管理
  x_bbs_assemble_control,//BBS
  x_hotpic_assemble_control,//热图展现
  x_cms_assemble_control,
  x_organization_assemble_control,//新组织人员管理
  x_organization_assemble_custom,
  x_processplatform_service_processing, // 流程服务
  x_processplatform_assemble_surface, // 流程
  x_processplatform_assemble_designer, // 流程设计
  x_processplatform_assemble_bam, // 流程监控
  x_organization_assemble_express,
  x_organization_assemble_personal,
  x_component_assemble_control,
  x_organization_assemble_authentication,
  x_portal_assemble_surface,   //门户模块
  x_calendar_assemble_control, //日程
  x_mind_assemble_control, //脑图
  x_teamwork_assemble_control, //TeamWork
  x_wcrm_assemble_control, // CRM
  x_jpush_assemble_control, // 极光推送服务
  x_message_assemble_communicate, // 通信模块 消息收发 还有websocket
  x_organizationPermission, // custom模块 通讯录 需要到应用市场下载安装
  x_pan_assemble_control, // custom模块 V3版本云盘服务 需要到应用市场下载安装
  x_query_assemble_surface, //  查询模块
  x_app_packaging_client_assemble_control, //   自助打包模块
}