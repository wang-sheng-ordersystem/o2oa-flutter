import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'index.dart';

class LogListPage extends GetView<LogListController> {
  const LogListPage({Key? key}) : super(key: key);
 
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LogListController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("settings_logs".tr)),
          body: SafeArea(
            child: Obx(()=>ListView.separated(
              itemBuilder: ((context, index) {
              var item = controller.state.logList[index];
              String filename = item.path.split('/').last;
              return ListTile(
                onTap: () => controller.openLogFile(item.path, filename),
                title: Text(filename),
              );
            }),
            separatorBuilder: ((context, index) => const Divider(height: 10)),
            itemCount: controller.state.logList.length,),
          ),
        ));
      },
    );
  }
}
