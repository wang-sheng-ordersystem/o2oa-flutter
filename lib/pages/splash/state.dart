import 'package:get/get.dart';

class SplashState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  /// tips
  final _tips = 'splash_getReady'.tr.obs;
  set tips(value) => _tips.value = value;
  get tips => _tips.value;

  /// bottom copy
  final _copyright = ''.obs;
  set copyright(String value) => _copyright.value = value;
  String get copyright => _copyright.value;

/// showLaunchLogo
  final _showLaunchLogo = false.obs;
  set showLaunchLogo(value) => _showLaunchLogo.value = value;
  get showLaunchLogo => _showLaunchLogo.value;


  /// 推广页
  final RxList<String> promotionPageList = <String>[].obs;

  /// 当前推广页码
  final _currentPromotionPage = 0.obs;
  set currentPromotionPage(int value) => _currentPromotionPage.value = value;
  int get currentPromotionPage => _currentPromotionPage.value;

}
