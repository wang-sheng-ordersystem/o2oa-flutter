import 'dart:io';

import 'package:flutter/services.dart';
import 'package:o2oa_all_platform/common/index.dart';

class O2FlutterMethodChannelUtils {
  // 通信通道名称
  final String flutterPCNotifyChannelName = "pulgin.pc.o2oa.net/notify";

  // Android ios  通道
  final String channelName = "o2oa.net/flutter_inner";
  
  // 通信方法名称
  final cmdNameNotify = "sendNotify";

  // macos 通道
  MethodChannel? _methodChannelMacos;

  MethodChannel? _methodChannel;

  //私有构造函数
  O2FlutterMethodChannelUtils._internal();

  //保存单例
  static final O2FlutterMethodChannelUtils _singleton =
      O2FlutterMethodChannelUtils._internal();

  //工厂构造函数
  factory O2FlutterMethodChannelUtils() => _singleton;

  /// 发送 系统通知 目前实现 windows和macos
  /// 
  Future<void> sendIMMsgToPCNotify(String? id, String title, String msgBody) async {
    OLogger.d('发送IM消息，title: $title , body: $msgBody');
    _methodChannelMacos ??= MethodChannel(flutterPCNotifyChannelName);
    final Map<String, String> params = {"title": title, "msgBody": msgBody};
    if (id != null) {
      params["msgId"] = id;
    }
    try {
      final answer = await _methodChannelMacos?.invokeMethod(
        cmdNameNotify, params); // .invokeListMethod<String>(CMD_OPEN_DIALOG, params);
      OLogger.i(answer);
    } on PlatformException catch (e) {
      OLogger.e("发送IM消息出错，${e.message}");
    } on MissingPluginException catch (e) {
      OLogger.e("发送IM消息出错，${e.message}");
    }
  }

  ///
  /// 打开本地文件
  /// @param filePath 文件路径
  /// 
  Future<void> openLocalFile(String filePath, {bool shareBtnShow = true}) async {
    OLogger.d("打开文件：$filePath");
    final Map<String, dynamic> params = {"filePath": filePath, "shareBtnShow": shareBtnShow};
    _methodChannel ??= MethodChannel(channelName);
    try {
      String? result = await _methodChannel?.invokeMethod("openFile", params);
      OLogger.i("文件打开完成：$result");
    } on PlatformException catch (e) {
      OLogger.e("打开文件出错，${e.message}");
    } on MissingPluginException catch (e) {
      OLogger.e("打开文件出错，${e.message}");
    }
  }

  /// Android 安装 apk
  Future<void> installApk(String filePath) async {
    if (!Platform.isAndroid) {
      OLogger.e('该方法只支持 Android 端');
      return;
    }
    final Map<String, String> params = {"filePath": filePath};
    _methodChannel ??= MethodChannel(channelName);
    try {
      String? result = await _methodChannel?.invokeMethod("installAPK", params);
      OLogger.i("安装 apk 完成：$result");
    } on PlatformException catch (e) {
      OLogger.e("安装 apk 出错，${e.message}");
    } on MissingPluginException catch (e) {
      OLogger.e("安装 apk 出错，${e.message}");
    }
  }

  /// 原生 清除webview缓存
  Future<void> clearCacheNative() async {
    OLogger.d('清除缓存，原生方法！');
    if (!Platform.isAndroid && !Platform.isIOS) {
      OLogger.e('该方法只支持 移动端');
      return;
    }
    _methodChannel ??= MethodChannel(channelName);
    try {
      String? result = await _methodChannel?.invokeMethod("clearCache", {});
      OLogger.i("清除缓存 完成：$result");
    } on PlatformException catch (e) {
      OLogger.e("清除缓存 出错，${e.message}");
    } on MissingPluginException catch (e) {
      OLogger.e("清除缓存 出错，${e.message}");
    }
  }

  /// 检查是否 root  或 是否越狱
  Future<bool> checkRootNative() async {
    OLogger.d('检查是否已经 root 或 越狱！');
    if (!Platform.isAndroid && !Platform.isIOS) {
      OLogger.e('该方法只支持 移动端');
      return false;
    }
    _methodChannel ??= MethodChannel(channelName);
    try {
      bool? result = await _methodChannel?.invokeMethod("checkRoot", {});
      OLogger.i("checkRoot 结果：$result");
      return result == true;
    } on PlatformException catch (e) {
      OLogger.e("checkRoot 出错，${e.message}");
    } on MissingPluginException catch (e) {
      OLogger.e("checkRoot 出错，${e.message}");
    }
    return false;
  }

 

  Future<bool> saveToAlbum(String filePath) async {
    OLogger.d('保存到相册，原生方法！');
    if (!Platform.isAndroid && !Platform.isIOS) {
      OLogger.e('该方法只支持 移动端');
      return false;
    }
    _methodChannel ??= MethodChannel(channelName);
    try {
      bool? result = await _methodChannel?.invokeMethod("saveToAlbum", {"filePath": filePath});
      OLogger.i("保存到相册 结果：$result");
      return result == true;
    } on PlatformException catch (e) {
      OLogger.e("保存到相册 出错，${e.message}");
    } on MissingPluginException catch (e) {
      OLogger.e("保存到相册 出错，${e.message}");
    }
    return false;
  }

}
