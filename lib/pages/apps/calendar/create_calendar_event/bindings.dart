import 'package:get/get.dart';

import 'controller.dart';

class CreateCalendarEventBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CreateCalendarEventController>(() => CreateCalendarEventController());
  }
}
