import 'package:get/get.dart';

import '../../../../common/utils/index.dart';

class MyProfileState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  // 邮箱
  final _email = "".obs;
  set email(String value) => _email.value = value;
  String get email => _email.value;

  // 手机号码
  final _mobile = "".obs;
  set mobile(String value) => _mobile.value = value;
  String get mobile => _mobile.value;

  // 办公电话
  final _telePhone = "".obs;
  set officePhone(String value) => _telePhone.value = value;
  String get officePhone => _telePhone.value;

  // 个人签名
  final _signature = "".obs;
  set signature(String value) => _signature.value = value;
  String get signature => _signature.value;

  /// 刷新头像
  final _dn = (O2ApiManager.instance.o2User?.distinguishedName ?? '').obs;
  set dn(String value) => _dn.value = value;
  String get dn => _dn.value;
}
