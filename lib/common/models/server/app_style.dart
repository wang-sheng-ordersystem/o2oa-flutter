// To parse this JSON data, do
//
//     final appStyle = appStyleFromJson(jsonString);


class AppStyle {
    AppStyle({
        this.portalList,
        this.indexType,
        this.indexPortal,
        this.simpleMode,
        this.indexCentered,
        this.appIndexPages,
        this.processFilterList,
        this.cmsCategoryFilterList,
        this.contactPermissionView,
        this.nativeAppList,
        this.images,
        this.needGray,
        this.appExitAlert,
        this.systemMessageCanClick,
        this.systemMessageSwitch,
        this.speechScript,
        this.promotionPageScript,
        this.extendParam
    });

    
    String? indexType; // 首页展现类型,default是移动端原来的首页,portal是门户.	default
    String? indexPortal;
    bool? simpleMode; // 移动端简易模式
    bool? indexCentered; // 首页居中 首页居中 下面的 appIndexPages 就无效了，页面个数就固定了
    List<String>? appIndexPages; // 首页展现的 tab 列表
    bool? systemMessageSwitch; // 消息列表中 是否显示系统通知
    bool? systemMessageCanClick; // 系统消息是否可点击打开
    bool? needGray; // 是否启用灰色 默哀
    String? appExitAlert; // app退出提示，有提示信息就弹出窗
    String? contactPermissionView; // 移动App通讯录权限视图	addressPowerView
    List<NativeAppList>? nativeAppList;
    List<PortalList>? portalList;
    List<AppStyleImage>? images;
    List<String>? processFilterList; // 首页办公中心，流程过滤条件.
    List<String>? cmsCategoryFilterList; // 首页信息中心，分类过滤条件.
    String? speechScript; // 语音助手服务脚本名，如果没有不显示语音助手
    String? promotionPageScript; // 推广页
    Map<String, dynamic>? extendParam; // 扩展属性


    factory AppStyle.fromJson(Map<String, dynamic> json) => AppStyle(
        portalList: json["portalList"] == null ? null : List<PortalList>.from(json["portalList"].map((x) => PortalList.fromJson(x))),
        indexType: json["indexType"],
        indexPortal: json["indexPortal"],
        simpleMode: json["simpleMode"],
        indexCentered: json["indexCentered"],
        appIndexPages: json["appIndexPages"] == null ? null : List<String>.from(json['appIndexPages'].map((e)=> e)),
        needGray: json["needGray"],
        appExitAlert: json["appExitAlert"],
        systemMessageSwitch: json["systemMessageSwitch"],
        systemMessageCanClick: json["systemMessageCanClick"],
        contactPermissionView: json["contactPermissionView"],
        speechScript: json["speechScript"],
        promotionPageScript: json["promotionPageScript"],
        nativeAppList: json["nativeAppList"] == null ? null : List<NativeAppList>.from(json["nativeAppList"].map((x) => NativeAppList.fromJson(x))),
        images: json["images"] == null ? null : List<AppStyleImage>.from(json["images"].map((x) => AppStyleImage.fromJson(x))),
        processFilterList: json["processFilterList"] == null ? null : List<String>.from(json['processFilterList'].map((e)=> e)),
        cmsCategoryFilterList: json["cmsCategoryFilterList"] == null ? null : List<String>.from(json['cmsCategoryFilterList'].map((e)=> e)),
        extendParam: json["extendParam"]
    );

    Map<String, dynamic> toJson() => {
        "portalList": portalList == null ? null : List<dynamic>.from(portalList!.map((x) => x.toJson())),
        "indexType": indexType,
        "indexPortal": indexPortal,
        "appIndexPages": appIndexPages,
        "simpleMode": simpleMode,
        "indexCentered": indexCentered,
        "needGray": needGray,
        "appExitAlert": appExitAlert,
        "systemMessageSwitch": systemMessageSwitch,
        "systemMessageCanClick": systemMessageCanClick,
        "contactPermissionView": contactPermissionView,
        "speechScript": speechScript,
        "promotionPageScript": promotionPageScript,
        "nativeAppList": nativeAppList == null ? null : List<dynamic>.from(nativeAppList!.map((x) => x.toJson())),
        "images": images == null ? null : List<dynamic>.from(images!.map((x) => x.toJson())),
        "processFilterList": processFilterList,
        "cmsCategoryFilterList": cmsCategoryFilterList,
        "extendParam": extendParam
    };
}

class AppStyleImage {
   AppStyleImage({
        this.name,
        this.value,
        this.path
    });

    String? name;
    String? value;
    String? path; // 在线图片 相对地址 如  x_desktop/img/abc.png

    factory AppStyleImage.fromJson(Map<String, dynamic> json) => AppStyleImage(
        name: json["name"],
        value: json["value"],
        path: json["path"],
    );

    Map<String, dynamic> toJson() => {
        "name": name,
        "value": value,
        "path": path,
    };
}

class NativeAppList {
    NativeAppList({
        this.id,
        this.key,
        this.name,
        this.displayName,
        this.enable,
        this.iOs,
    });

    int? id;
    String? key;
    String? name;
    String? displayName; // 显示名 如果没有内容就用源码中的国际化名称
    bool? enable;
    IOs? iOs;

    factory NativeAppList.fromJson(Map<String, dynamic> json) => NativeAppList(
        id: json["id"],
        key: json["key"],
        name: json["name"],
        displayName: json["displayName"],
        enable: json["enable"],
        iOs: json["iOS"] == null ? null : IOs.fromJson(json["iOS"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "key": key,
        "name": name,
        "displayName": displayName,
        "enable": enable,
        "iOS": iOs == null ? null : iOs!.toJson(),
    };
}

class IOs {
    IOs({
        this.category,
        this.subcategory,
        this.storyboard,
        this.vcname,
    });

    Category? category;
    Subcategory? subcategory;
    String? storyboard;
    Vcname? vcname;

    factory IOs.fromJson(Map<String, dynamic> json) => IOs(
        category: json["category"] == null ? null : categoryValues.map[json["category"]],
        subcategory: json["subcategory"] == null ? null : subcategoryValues.map[json["subcategory"]],
        storyboard: json["storyboard"],
        vcname: json["vcname"] == null ? null : vcnameValues.map[json["vcname"]],
    );

    Map<String, dynamic> toJson() => {
        "category": category == null ? null : categoryValues.reverse[category],
        "subcategory": subcategory == null ? null : subcategoryValues.reverse[subcategory],
        "storyboard": storyboard,
        "vcname": vcname == null ? null : vcnameValues.reverse[vcname],
    };
}

enum Category { NATIVE }

final categoryValues = EnumValues({
    "native": Category.NATIVE
});

enum Subcategory { CODING }

final subcategoryValues = EnumValues({
    "coding": Subcategory.CODING
});

enum Vcname { TODO_TASK, EMPTY }

final vcnameValues = EnumValues({
    "": Vcname.EMPTY,
    "todoTask": Vcname.TODO_TASK
});




class PortalList {
    PortalList({
        this.id,
        this.name,
        this.alias,
        this.description,
        this.portalCategory,
        this.firstPage,
        this.creatorPerson,
        this.lastUpdatePerson,
        this.pcClient,
        this.mobileClient,
    });

    String? id;
    String? name;
    String? alias;
    String? description;
    String? portalCategory;
    String? firstPage;
    String? creatorPerson;
    String? lastUpdatePerson;
    bool? pcClient;
    bool? mobileClient;
   

    factory PortalList.fromJson(Map<String, dynamic> json) => PortalList(
        id: json["id"],
        name: json["name"] ,
        alias: json["alias"],
        description: json["description"],
        portalCategory: json["portalCategory"],
        firstPage: json["firstPage"],
        creatorPerson: json["creatorPerson"],
        lastUpdatePerson: json["lastUpdatePerson"],
        pcClient: json["pcClient"],
        mobileClient: json["mobileClient"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "alias": alias,
        "description": description,
        "portalCategory": portalCategory,
        "firstPage": firstPage,
        "creatorPerson": creatorPerson,
        "lastUpdatePerson": lastUpdatePerson,
        "pcClient": pcClient,
        "mobileClient": mobileClient,
       
    };
}

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String>? reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => MapEntry(v, k));
        }
        return reverseMap!;
    }
}
