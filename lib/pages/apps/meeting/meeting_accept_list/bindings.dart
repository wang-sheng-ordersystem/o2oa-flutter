import 'package:get/get.dart';

import 'controller.dart';

class MeetingAcceptListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MeetingAcceptListController>(() => MeetingAcceptListController());
  }
}
