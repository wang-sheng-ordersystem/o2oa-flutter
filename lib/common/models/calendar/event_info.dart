class EventFilterList{
  List<EventInfo>? wholeDayEvents;
  List<EventDayInfo>? inOneDayEvents;
   
  EventFilterList({this.wholeDayEvents, this.inOneDayEvents});

   factory EventFilterList.fromJson(Map<String, dynamic> json) => EventFilterList(
    wholeDayEvents: json["wholeDayEvents"] == null ? [] : List<EventInfo>.from(json["wholeDayEvents"]!.map((x) => EventInfo.fromJson(x))),
    inOneDayEvents: json["inOneDayEvents"] == null ? [] : List<EventDayInfo>.from(json["inOneDayEvents"]!.map((x) => EventDayInfo.fromJson(x))),
   );
}

class EventDayInfo{
  String? eventDate;
  List<EventInfo>? inOneDayEvents;
  List<EventInfo>? wholeDayEvents;

  EventDayInfo({this.eventDate, this.inOneDayEvents, this.wholeDayEvents});

  factory EventDayInfo.fromJson(Map<String, dynamic> json) => EventDayInfo(
    eventDate: json["eventDate"]  ,
    inOneDayEvents: json["inOneDayEvents"] == null ? [] : List<EventInfo>.from(json["inOneDayEvents"]!.map((x) => EventInfo.fromJson(x))),
    wholeDayEvents: json["wholeDayEvents"] == null ? [] : List<EventInfo>.from(json["wholeDayEvents"]!.map((x) => EventInfo.fromJson(x)))
   );

}


class EventInfo {
    String? id;
    String? calendarId;
    String? eventType;
    String? title;
    String? color;
    String? startTime;
    String? startTimeStr;
    String? endTime;
    String? endTimeStr;
    String? recurrenceRule;
    bool? alarm;
    bool? alarmAlready;
    String? valarmTimeConfig;
    bool? isAllDayEvent;
    int? daysOfDuration;
    bool? isPublic;
    String? source;
    String? createPerson;
    String? updatePerson;
    String? targetType;
    List<String>? participants;
    List<String>? manageablePersonList;
    List<String>? viewablePersonList;
    List<String>? viewableUnitList;
    List<String>? viewableGroupList;
    String? createTime;
    String? updateTime;
    String? comment;

    EventInfo({
        this.id,
        this.calendarId,
        this.eventType,
        this.title,
        this.color,
        this.startTime,
        this.startTimeStr,
        this.endTime,
        this.endTimeStr,
        this.recurrenceRule,
        this.alarm,
        this.alarmAlready,
        this.valarmTimeConfig,
        this.isAllDayEvent,
        this.daysOfDuration,
        this.isPublic,
        this.source,
        this.createPerson,
        this.updatePerson,
        this.targetType,
        this.participants,
        this.manageablePersonList,
        this.viewablePersonList,
        this.viewableUnitList,
        this.viewableGroupList,
        this.createTime,
        this.updateTime,
        this.comment,
    });

    factory EventInfo.fromJson(Map<String, dynamic> json) => EventInfo(
        id: json["id"],
        calendarId: json["calendarId"],
        eventType: json["eventType"],
        title: json["title"],
        color: json["color"],
        startTime: json["startTime"] ,
        startTimeStr: json["startTimeStr"] ,
        endTime: json["endTime"] ,
        endTimeStr: json["endTimeStr"],
        recurrenceRule: json["recurrenceRule"],
        alarm: json["alarm"],
        alarmAlready: json["alarmAlready"],
        valarmTimeConfig: json["valarmTime_config"],
        isAllDayEvent: json["isAllDayEvent"],
        daysOfDuration: json["daysOfDuration"],
        isPublic: json["isPublic"],
        source: json["source"],
        createPerson: json["createPerson"],
        updatePerson: json["updatePerson"],
        targetType: json["targetType"],
        participants: json["participants"] == null ? [] : List<String>.from(json["participants"]!.map((x) => x)),
        manageablePersonList: json["manageablePersonList"] == null ? [] : List<String>.from(json["manageablePersonList"]!.map((x) => x)),
        viewablePersonList: json["viewablePersonList"] == null ? [] : List<String>.from(json["viewablePersonList"]!.map((x) => x)),
        viewableUnitList: json["viewableUnitList"] == null ? [] : List<String>.from(json["viewableUnitList"]!.map((x) => x)),
        viewableGroupList: json["viewableGroupList"] == null ? [] : List<String>.from(json["viewableGroupList"]!.map((x) => x)),
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        comment: json["comment"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "calendarId": calendarId,
        "eventType": eventType,
        "title": title,
        "color": color,
        "startTime": startTime,
        "startTimeStr": startTimeStr,
        "endTime": endTime,
        "endTimeStr": endTimeStr,
        "recurrenceRule": recurrenceRule,
        "alarm": alarm,
        "alarmAlready": alarmAlready,
        "valarmTime_config": valarmTimeConfig,
        "isAllDayEvent": isAllDayEvent,
        "daysOfDuration": daysOfDuration,
        "isPublic": isPublic,
        "source": source,
        "createPerson": createPerson,
        "updatePerson": updatePerson,
        "targetType": targetType,
        "participants": participants == null ? [] : List<String>.from(participants!.map((x) => x)),
        "manageablePersonList": manageablePersonList == null ? [] : List<String>.from(manageablePersonList!.map((x) => x)),
        "viewablePersonList": viewablePersonList == null ? [] : List<String>.from(viewablePersonList!.map((x) => x)),
        "viewableUnitList": viewableUnitList == null ? [] : List<String>.from(viewableUnitList!.map((x) => x)),
        "viewableGroupList": viewableGroupList == null ? [] : List<String>.from(viewableGroupList!.map((x) => x)),
        "createTime": createTime,
        "updateTime": updateTime,
        "comment": comment,
    };
}