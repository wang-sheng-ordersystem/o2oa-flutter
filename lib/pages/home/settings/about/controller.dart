import 'dart:io';

import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../../../common/services/index.dart';
import '../../../../common/utils/index.dart';
import '../../../../common/values/index.dart';
import '../../../../environment_config.dart';
import '../../../common/inner_webview/index.dart';
import 'index.dart';

class AboutController extends GetxController {
  AboutController();

  final state = AboutState();
 
  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    if (EnvironmentConfig.isDirectConnectMode()) {
      state.isInner = true;
    }
    if (Platform.isAndroid) {
      state.isAndroid = true;
    }
    loadPackInfo();
    loadappUpdateUserSwitchKey();
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

  /// 用户是否开启  app 检查更新
  loadappUpdateUserSwitchKey() {
    state.isOpenUpdate = SharedPreferenceService.to.getBool(SharedPreferenceService.appUpdateUserSwitchKey, defaultValue: true);
  }
 
  void loadPackInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
  // String appName = packageInfo.appName;
  // String packageName = packageInfo.packageName;
  // String version = packageInfo.version;
  // String buildNumber = packageInfo.buildNumber;
    state.versionName = packageInfo.version;
  }

  void openUserAgreement() {
    InnerWebviewPage.open(O2.userAgreementUrl);
  }

  void openPrivacyPolicy() {
    InnerWebviewPage.open(O2.privacyPolicyUrl);
  }
  
  void checkUpdate() {
    O2AndroidAppUpdateManager().checkUpdate(needToast: true);
  }
  
  void switchUpdateCheck(bool value) {
     SharedPreferenceService.to.putBool(SharedPreferenceService.appUpdateUserSwitchKey, value);
     state.isOpenUpdate = value;
  }
}
