import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_crop/image_crop.dart';

import '../../../../../common/routers/index.dart';
import '../../../../../common/style/index.dart';
import 'index.dart';

class EditAvatarPage extends GetView<EditAvatarController> {
  const EditAvatarPage({Key? key}) : super(key: key);

  static Future<dynamic> openEditAvatar(String file) async {
    if (file.isEmpty) {
      return null;
    }
    return await Get.toNamed(O2OARoutes.homeSettingsEditAvatar,
        arguments: {'file': file});
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<EditAvatarController>(
      init: EditAvatarController(),
      id: "edit_avatar",
      builder: (_) {
        return Scaffold(
            appBar: AppBar(
              title: Text('my_profile_avatar_edit'.tr),
              actions: [
                TextButton(
                    onPressed: () => controller.clickSave(),
                    child: Text('positive'.tr,
                        style: AppTheme.whitePrimaryTextStyle))
              ],
            ),
            body: SafeArea(
              child: Obx(
                () => Container(
                  color: Colors.black,
                  padding: const EdgeInsets.all(20.0),
                  child: controller.imageFile.value == null
                      ? Center(
                          child: Text(
                            'edit_avatar_no_file'.tr,
                            style: AppTheme.whitePrimaryTextStyle,
                          ),
                        )
                      : Crop.file(
                          controller.imageFile.value!,
                          key: controller.cropKey,
                          aspectRatio: 1,
                        ),
                ),
              ),
            ));
      },
    );
  }
}
