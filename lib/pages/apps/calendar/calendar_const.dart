import 'package:get/get.dart';

class O2Calendar {
  static const deepColors = [
    "#428ffc",
    "#5bcc61",
    "#f9bf24",
    "#f75f59",
    "#f180f7",
    "#9072f1",
    "#909090",
    "#1462be"
  ];

  static final calendarTypes = {
    "PERSON": "calendar_edit_form_type_person".tr,
    "UNIT": "calendar_edit_form_type_unit".tr,
  };

  static final remindOptions = {
    "NONE": "calendar_event_form_remind_none".tr,
    "-5_s": "calendar_event_form_remind_begin".tr,
    "-5_m": "calendar_event_form_remind_before_five".tr,
    "-10_m": "calendar_event_form_remind_before_ten".tr,
    "-15_m": "calendar_event_form_remind_before_fifteen".tr,
    "-30_m": "calendar_event_form_remind_before_third".tr,
    "-1_h": "calendar_event_form_remind_before_one_hour".tr,
    "-2_h": "calendar_event_form_remind_before_two_hour".tr
  };
  static final repeatOptions = {
    "NONE": "calendar_event_form_repeat_none".tr,
    "DAILY": "calendar_event_form_repeat_day".tr,
    "WEEKLY": "calendar_event_form_repeat_week".tr,
    "MONTHLY": "calendar_event_form_repeat_month".tr,
    "YEARLY": "calendar_event_form_repeat_year".tr
  };
  //SU,MO,TU,WE,TH,FR,SA
  static final weekDays = {
    "SU": "common_week_sunday".tr,
    "MO": "common_week_monday".tr,
    "TU": "common_week_tuesday".tr,
    "WE": "common_week_wednesday".tr,
    "TH": "common_week_thursday".tr,
    "FR": "common_week_friday".tr,
    "SA": "common_week_saturday".tr
  };
}
