import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../common/models/index.dart';
import '../../../../common/style/index.dart';
import 'index.dart';

class OfficeCenterPage extends GetView<OfficeCenterController> {
  const OfficeCenterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OfficeCenterController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: Obx(() => Text(controller.state.title)),
            actions: [
              Obx(() {
                final len = controller.state.taskSelectedList.length;
                return Visibility(
                  visible: len > 0,
                  child: TextButton(
                      onPressed: () => controller.gotoQuickProcess(),
                      child: Text('process_task_quick_process'.tr, style: AppTheme.whitePrimaryTextStyle)),
                );
              })
            ],
          ),
          body: SafeArea(
              child: Obx(
            () => Column(children: [
              searchBar(context),
              const SizedBox(height: 5),
              Expanded(
                  flex: 1,
                  child: SmartRefresher(
                      enablePullDown: true,
                      enablePullUp: controller.state.hasMoreData,
                      controller: controller.refreshController,
                      onRefresh: controller.onRefresh,
                      onLoading: controller.onLoadMore,
                      child: ListView.separated(
                        separatorBuilder: ((context, index) =>
                            const Divider(height: 1)),
                        itemCount: controller.state.dataList.length,
                        itemBuilder: (context, index) {
                          final item = controller.state.dataList[index];
                          if (item is TaskData) {
                            return taskItem(item, context);
                          } else if (item is TaskCompletedData) {
                            return taskCompletedItem(item, context);
                          } else if (item is ReadData) {
                            return readItem(item, context);
                          } else if (item is ReadCompletedData) {
                            return readCompletedItem(item, context);
                          }
                          return Container();
                        },
                      )))
            ]),
          )),
        );
      },
    );
  }

  Widget searchBar(BuildContext context) {
    return Padding(
        padding:
            EdgeInsets.only(left: 12.w, right: 12.w, top: 8.w, bottom: 8.w),
        child: Container(
          height: 36.w,
          decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.background,
              borderRadius: BorderRadius.all(Radius.circular(18.w))),
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 10.w),
            child: Row(
              children: [
                SizedBox(
                  width: 22.w,
                  height: 22.w,
                  child: Icon(Icons.search,
                      color: Theme.of(context).colorScheme.primary),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 5.w),
                    child: TextField(
                      autofocus: false,
                      controller: controller.searchController,
                      decoration: InputDecoration(
                        isDense: true,
                        border: InputBorder.none,
                        hintText: 'process_office_center_search_placeholder'.tr,
                        hintStyle: Theme.of(context).textTheme.bodySmall,
                      ),
                      style: Theme.of(context).textTheme.bodyMedium,
                      textInputAction: TextInputAction.search,
                      onSubmitted: controller.onSearch,
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }

  Widget taskItem(TaskData task, BuildContext context) {
    String title = task.title ?? '';
    if (title.isEmpty) {
      title = 'process_work_no_title_no_process'.tr;
    }
    String time = task.startTime ?? '';
    if (time.isNotEmpty) {
      time = time.substring(0, 10);
    }
    return Obx(() {
      final len = controller.state.taskSelectedList.length;
      // 是否可快速处理
      bool rapid = task.allowRapid ?? false;
      // 只能选择相同的活动上的待办
      if (rapid &&
          (len == 0 ||
              (len > 0 &&
                  controller.state.taskSelectedList[0].activity ==
                      task.activity
                      && controller.state.taskSelectedList[0].process ==
                      task.process))) {
        return CheckboxListTile(
          // onTap: () => controller.clickTask(task),
          title: Text('[${task.processName}] $title'),
          subtitle: Text(task.activityName ?? ''),
          secondary: TextButton(onPressed: (() => controller.clickTask(task)), child: Text('process_task_open_work'.tr)),
          //  GestureDetector(
          //   onTap: () => controller.clickTask(task),
          //   child: Row(
          //       mainAxisAlignment: MainAxisAlignment.end,
          //       mainAxisSize: MainAxisSize.min,
          //       children: [
          //         Text(time, style: Theme.of(context).textTheme.bodySmall),
          //         const Icon(
          //           Icons.keyboard_arrow_right,
          //           color: AppColor.hintText,
          //           size: 22,
          //         )
          //       ]),
          // ),
          controlAffinity: ListTileControlAffinity.leading,
          value: controller.isItemChecked(task),
          onChanged: (value) => controller.onSelectChange(value, task),
        );
      } else {
        return ListTile(
          onTap: () => controller.clickTask(task),
          title: Text('[${task.processName}] $title'),
          subtitle: Text(task.activityName ?? ''),
          trailing: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(time, style: Theme.of(context).textTheme.bodySmall),
                const Icon(
                  Icons.keyboard_arrow_right,
                  color: AppColor.hintText,
                  size: 22,
                )
              ]),
        );
      }
    });
  }

  Widget taskCompletedItem(
      TaskCompletedData taskCompleted, BuildContext context) {
    String title = taskCompleted.title ?? '';
    if (title.isEmpty) {
      title = 'process_work_no_title_no_process'.tr;
    }
    String time = taskCompleted.startTime ?? '';
    if (time.isNotEmpty) {
      time = time.substring(0, 10);
    }
    return ListTile(
        onTap: () => controller.clickTaskCompleted(taskCompleted),
        title: Text('[${taskCompleted.processName}] $title'),
        subtitle: Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Text(taskCompleted.activityName ?? '')),
        trailing: Column(
          children: [
            taskCompleted.completed == true
                ? Container(
                    width: 64,
                    height: 18,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(9)),
                        color: AppColor.o2Blue),
                    child: Center(
                        child: Text('process_work_completed_tag'.tr,
                            style: TextStyle(
                                color: Colors.white, fontSize: 13.sp))),
                  )
                : const SizedBox(height: 18),
            const SizedBox(height: 5),
            Expanded(
                flex: 1,
                child: Text(time,
                    style: Theme.of(context).textTheme.bodySmall,
                    textAlign: TextAlign.center)),
          ],
        ));
  }

  Widget readItem(ReadData read, BuildContext context) {
    String title = read.title ?? '';
    if (title.isEmpty) {
      title = 'process_work_no_title_no_process'.tr;
    }
    String time = read.startTime ?? '';
    if (time.isNotEmpty) {
      time = time.substring(0, 10);
    }
    return ListTile(
      onTap: () => controller.clickRead(read),
      title: Text('[${read.processName}] $title'),
      subtitle: Text(read.activityName ?? ''),
      trailing: Column(
        children: [
          read.completed == true
              ? Container(
                  width: 64,
                  height: 18,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(9)),
                      color: AppColor.o2Blue),
                  child: Center(
                      child: Text('process_work_completed_tag'.tr,
                          style:
                              TextStyle(color: Colors.white, fontSize: 13.sp))),
                )
              : const SizedBox(height: 18),
          const SizedBox(height: 5),
          Expanded(
              flex: 1,
              child: Text(time,
                  style: Theme.of(context).textTheme.bodySmall,
                  textAlign: TextAlign.center)),
        ],
      ),
    );
  }

  Widget readCompletedItem(
      ReadCompletedData readCompleted, BuildContext context) {
    String title = readCompleted.title ?? '';
    if (title.isEmpty) {
      title = 'process_work_no_title_no_process'.tr;
    }
    String time = readCompleted.startTime ?? '';
    if (time.isNotEmpty) {
      time = time.substring(0, 10);
    }
    return ListTile(
      onTap: () => controller.clickReadCompleted(readCompleted),
      title: Text('[${readCompleted.processName}] $title'),
      subtitle: Text(readCompleted.activityName ?? ''),
      trailing: Column(
        children: [
          readCompleted.completed == true
              ? Container(
                  width: 64,
                  height: 18,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(9)),
                      color: AppColor.o2Blue),
                  child: Center(
                      child: Text('process_work_completed_tag'.tr,
                          style:
                              TextStyle(color: Colors.white, fontSize: 13.sp))),
                )
              : const SizedBox(height: 18),
          const SizedBox(height: 5),
          Expanded(
              flex: 1,
              child: Text(time,
                  style: Theme.of(context).textTheme.bodySmall,
                  textAlign: TextAlign.center)),
        ],
      ),
    );
  }
}
