import 'package:get/get.dart';

import 'controller.dart';

class InstantChatBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InstantChatController>(() => InstantChatController());
  }
}
