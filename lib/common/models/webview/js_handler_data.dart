

import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class O2JavascriptHandlerData {
  final String key;
  final JavaScriptHandlerCallback callback;
  O2JavascriptHandlerData({required this.key, required this.callback});
}