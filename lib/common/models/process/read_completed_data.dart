// To parse this JSON data, do
//
//     final taskData = taskDataFromJson(jsonString);


class ReadCompletedData {
    ReadCompletedData({
        this.rank,
        this.id,
        this.job,
        this.read,
        this.workCompleted,
        this.completed,
        this.title,
        this.startTime,
        this.startTimeMonth,
        this.work,
        this.completedTime,
        this.completedTimeMonth,
        this.application,
        this.applicationName,
        this.applicationAlias,
        this.process,
        this.processName,
        this.processAlias,
        this.serial,
        this.person,
        this.identity,
        this.unit,
        this.activity,
        this.activityName,
        this.activityAlias,
        this.activityDescription,
        this.activityType,
        this.activityToken,
        this.creatorPerson,
        this.creatorIdentity,
        this.creatorUnit,
        this.expired,
        this.urged,
        this.opinion,
        this.modified,
        this.viewed,
        this.allowRapid,
        this.first,
        this.series,
        this.workCreateType,
        this.createTime,
        this.updateTime,
    });

    int? rank;
    String? id;
    String? job;
    String? work; 
    String? read; //read	String	single	待阅Id.
    String? workCompleted	;// work completed id
    bool? completed; //	整个job是否已经完成.
    String? title;
    String? startTime;
    String? startTimeMonth;
    String? completedTime;
    String? completedTimeMonth;
    String? application;
    String? applicationName;
    String? applicationAlias;
    String? process;
    String? processName;
    String? processAlias;
    String? serial;
    String? person;
    String? identity;
    String? unit;
    String? activity;
    String? activityName;
    String? activityAlias;
    String? activityDescription;
    String? activityType;
    String? activityToken;
    String? creatorPerson;
    String? creatorIdentity;
    String? creatorUnit;
    bool? expired;
    bool? urged;
    String? opinion;
    bool? modified;
    bool? viewed;
    bool? allowRapid;
    bool? first;
    String? series;
    String? workCreateType;
    String? createTime;
    String? updateTime;

    factory ReadCompletedData.fromJson(Map<String, dynamic> json) => ReadCompletedData(
        rank: json["rank"],
        id: json["id"],
        job: json["job"],
        read: json["read"],
        workCompleted: json["workCompleted"],
        completed: json["completed"],
        title: json["title"],
        startTime: json["startTime"],
        startTimeMonth: json["startTimeMonth"],
        work: json["work"],
        completedTime: json["completedTime"],
        completedTimeMonth: json["completedTimeMonth"],
        application: json["application"],
        applicationName: json["applicationName"],
        applicationAlias: json["applicationAlias"],
        process: json["process"],
        processName: json["processName"],
        processAlias: json["processAlias"],
        serial: json["serial"],
        person: json["person"],
        identity: json["identity"],
        unit: json["unit"],
        activity: json["activity"],
        activityName: json["activityName"],
        activityAlias: json["activityAlias"],
        activityDescription: json["activityDescription"],
        activityType: json["activityType"],
        activityToken: json["activityToken"],
        creatorPerson: json["creatorPerson"],
        creatorIdentity: json["creatorIdentity"],
        creatorUnit: json["creatorUnit"],
        expired: json["expired"],
        urged: json["urged"],
        opinion: json["opinion"],
        modified: json["modified"],
        viewed: json["viewed"],
        allowRapid: json["allowRapid"],
        first: json["first"],
        series: json["series"],
        workCreateType: json["workCreateType"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "rank": rank,
        "id": id,
        "job": job,
        "read": read,
        "workCompleted": workCompleted,
        "completed": completed,
        "title": title,
        "startTime": startTime,
        "startTimeMonth": startTimeMonth,
        "work": work,
        "completedTime": completedTime,
        "completedTimeMonth": completedTimeMonth,
        "application": application,
        "applicationName": applicationName,
        "applicationAlias": applicationAlias,
        "process": process,
        "processName": processName,
        "processAlias": processAlias,
        "serial": serial,
        "person": person,
        "identity": identity,
        "unit": unit,
        "activity": activity,
        "activityName": activityName,
        "activityAlias": activityAlias,
        "activityDescription": activityDescription,
        "activityType": activityType,
        "activityToken": activityToken,
        "creatorPerson": creatorPerson,
        "creatorIdentity": creatorIdentity,
        "creatorUnit": creatorUnit,
        "expired": expired,
        "urged": urged,
        "opinion": opinion,
        "modified": modified,
        "viewed": viewed,
        "allowRapid": allowRapid,
        "first": first,
        "series": series,
        "workCreateType": workCreateType,
        "createTime": createTime,
        "updateTime": updateTime,
    };
}
 