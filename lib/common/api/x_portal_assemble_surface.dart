


import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';

///
/// 门户服务
///
class PortalSurfaceService extends GetxService {
  
  static PortalSurfaceService get to => Get.find();


  String baseUrl() {
    return O2ApiManager.instance.getModuleBaseUrl(
        O2DistributeModuleEnum.x_portal_assemble_surface) ?? '';
  }

  ///
  /// 当前用户可访问的移动端门户列表
  ///
  Future<List<PortalList>?> portalMobileList() async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/portal/list/mobile');
      var list = response.data == null ? [] : response.data as List;
      return list.map((group) => PortalList.fromJson(group)).toList();
    } catch (err, stackTrace) {
      OLogger.e('查询用户可访问移动门户失败', err, stackTrace);
    }
    return null;
  }

  /// 红色角标，根据门户的脚本配置数量，显示在门户应用的角标位置
  Future<PortalCornerMarkData?> portalCornerMarkNumber(String portalId) async {
    try {
      ApiResponse response =
      await O2HttpClient.instance.get('${baseUrl()}jaxrs/portal/$portalId/corner/mark');
      return PortalCornerMarkData.fromJson(response.data);
    } catch (err, stackTrace) {
      OLogger.e('门户角标查询失败', err, stackTrace);
    }
    return null;
  }

}