class FolderBreadcrumbBean {
  String displayName; //  显示文件夹名称
  String folderId; // 文件夹id 查询下级文件使用
  int level; // 当前导航是第几级
  FolderBreadcrumbBean(this.displayName, this.folderId, this.level);
}
