import 'package:get/get.dart';

///
/// 原生应用枚举
/// 当前app已经存在的是应用
///
enum O2NativeAppEnum {
  task,
  taskcompleted,
  read,
  readcompleted,
  meeting,
  yunpan,
  bbs,
  cms,
  attendance,
  attendanceOld,
  calendar,
  mindMap,
}

extension O2NativeAppEnumExtension on O2NativeAppEnum {
  String get name {
    switch (this) {
      case O2NativeAppEnum.task:
        return "app_name_task".tr;
      case O2NativeAppEnum.taskcompleted:
        return "app_name_taskcompleted".tr;
      case O2NativeAppEnum.read:
        return "app_name_read".tr;
      case O2NativeAppEnum.readcompleted:
        return "app_name_readcompleted".tr;
      case O2NativeAppEnum.bbs:
        return "app_name_bbs".tr;
      case O2NativeAppEnum.calendar:
        return "app_name_calendar".tr;
      case O2NativeAppEnum.cms:
        return "app_name_cms".tr;
      case O2NativeAppEnum.meeting:
        return "app_name_meeting".tr;
      case O2NativeAppEnum.mindMap:
        return "app_name_mindMap".tr;
      case O2NativeAppEnum.attendance:
        return "app_name_attendance".tr;
      case O2NativeAppEnum.attendanceOld:
        return "app_name_attendance_old".tr;
      case O2NativeAppEnum.yunpan:
        return "app_name_yunpan".tr;
    }
  }

  String get key {
    switch (this) {
      case O2NativeAppEnum.task:
        return "task";
      case O2NativeAppEnum.taskcompleted:
        return "taskcompleted";
      case O2NativeAppEnum.read:
        return "read";
      case O2NativeAppEnum.readcompleted:
        return "readcompleted";
      case O2NativeAppEnum.bbs:
        return "bbs";
      case O2NativeAppEnum.calendar:
        return "calendar";
      case O2NativeAppEnum.cms:
        return "cms";
      case O2NativeAppEnum.meeting:
        return "meeting";
      case O2NativeAppEnum.mindMap:
        return "mindMap";
      case O2NativeAppEnum.attendance:
        return "attendance";
      case O2NativeAppEnum.attendanceOld:
        return "attendanceOld";
      case O2NativeAppEnum.yunpan:
        return "yunpan";
    }
  }

  // 应用图标地址
  String get assetPath {
    switch (this) {
      case O2NativeAppEnum.task:
        return "app_task.png";
      case O2NativeAppEnum.taskcompleted:
        return "app_task_completed.png";
      case O2NativeAppEnum.read:
        return "app_read.png";
      case O2NativeAppEnum.readcompleted:
        return "app_read_completed.png";
      case O2NativeAppEnum.meeting:
        return "app_meeting.png";
      case O2NativeAppEnum.bbs:
        return "app_bbs.png";
      case O2NativeAppEnum.yunpan:
        return "app_yunpan.png";
      case O2NativeAppEnum.attendance:
        return "app_attendance.png";
      case O2NativeAppEnum.attendanceOld:
        return "app_attendance.png";
      case O2NativeAppEnum.cms:
        return "app_cms.png";
      case O2NativeAppEnum.calendar:
        return "app_calendar.png";
      case O2NativeAppEnum.mindMap:
        return "app_mind_map.png";
    }
  }
}
