import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';

import '../../../../../../common/models/index.dart';
import '../../../../../../common/utils/index.dart';
import '../../../../../../common/widgets/index.dart';
import '../../../../../home/im/im_chat/widgets/widgets.dart';
import 'index.dart';

class CmsCategoryPage extends GetView<CmsCategoryController> {
  CmsCategoryPage(this.categoryData, {Key? key}) : super(key: key);

  CmsCategoryData categoryData;

  @override
  Widget build(BuildContext context) {
    OLogger.d('CmsCategoryPage 的 build ，category：${categoryData.categoryName}');
    return GetBuilder<CmsCategoryController>(
      init: CmsCategoryController(
        categoryData,
      ),
      id: "cms_category_${categoryData.id!}",
      global: false,
      builder: (c) {
        return Obx(() => ScrollConfiguration(
              behavior: const ChatScrollBehavior(),
              child: ListView.separated(
                  padding: const EdgeInsets.only(bottom: 10),
                  controller: c.scrollController,
                  itemCount: c.docList.length + 1,
                  separatorBuilder: ((context, index) =>
                      const Divider(height: 1)),
                  itemBuilder: (context, index) {
                    if (index == c.docList.length) {
                      return c.hasMore.value
                          ? const Center(
                              child: SizedBox(
                                height: 24,
                                width: 24,
                                child: CircularProgressIndicator(
                                  strokeWidth: 3,
                                ),
                              ),
                            )
                          : O2UI.noResultView(context);
                    }
                    return Slidable(
                        endActionPane: ActionPane(
                          motion: const ScrollMotion(),
                          children: [
                            SlidableAction(
                              onPressed: (_) => c.clickEditDoc(index),
                              backgroundColor: Colors.red,
                              foregroundColor: Colors.white,
                              icon: Icons.edit,
                              label: 'cms_document_edit_open'.tr,
                            )
                          ],
                        ),
                        child: ListTile(
                          onTap: () => c.clickDoc(index),
                          title: Text(c.docList[index].title ?? ''),
                          trailing: O2UI.rightArrow(),
                        ));
                  }),
            ));
      },
    );
  }
}
