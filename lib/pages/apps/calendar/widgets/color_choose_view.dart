import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../common/utils/index.dart';
import '../calendar_const.dart';

typedef ColorChooseViewOnChange = Function(String color);

class ColorChooseView extends StatelessWidget {
  const ColorChooseView({super.key, required this.color, required this.onChange});
  final String color;
  final ColorChooseViewOnChange onChange;

  @override
  Widget build(BuildContext context) {
    OLogger.d('重建。。。color $color ');
    return Padding(
        padding: const EdgeInsets.all(10),
        child: Wrap(
          spacing: 8.0, // 水平间距
          runSpacing: 4.0, // 垂直间距
          children: O2Calendar.deepColors
              .map((c) => InkWell(
                  onTap: () => onChange(c),
                  child: Stack(children: colorItemView(c))))
              .toList(),
        ));
  }

  List<Widget> colorItemView(String c) {
     OLogger.d('colorItemView $color  $c');
    if (color == c) {
      return <Widget>[
        Container(
            width: 24.w,
            height: 24.w,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(12), // 调整圆角半径以改变椭圆形状
              color: c.hexToColor(), // 设置背景颜色
            )),
        Icon(Icons.check, size: 24.w, color: Colors.white)
      ];
    } else {
      return <Widget>[
        Container(
            width: 24.w,
            height: 24.w,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(12.w), // 调整圆角半径以改变椭圆形状
              color: c.hexToColor(), // 设置背景颜色
            ))
      ];
    }
  }
 
}
