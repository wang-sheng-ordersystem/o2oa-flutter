

import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';

import 'log_util.dart';

typedef NetworkCheckHelperDelegate = void Function(ConnectivityResult result);

class NetworkCheckHelper {

  factory NetworkCheckHelper() => _instance;

  static final NetworkCheckHelper _instance =
      NetworkCheckHelper._internal();

  NetworkCheckHelper._internal();


  final Connectivity _connectivity = Connectivity();

  NetworkCheckHelperDelegate? delegate;
  StreamSubscription<ConnectivityResult>? _connectivitySubscription;

  void setDelegate(NetworkCheckHelperDelegate d) {
    delegate = d;
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen((event) {
      if (delegate != null) {
        delegate!(event);
      }
    });
  }

  void clearDelegate() {
    delegate = null;
    _connectivitySubscription?.cancel();
  }


  Future<bool> checkConnectivity() async {
    try {
      final result = await _connectivity.checkConnectivity();
      return  !(result == ConnectivityResult.none);
    } on PlatformException catch (e) {
      OLogger.e('Couldn\'t check connectivity status' , e);
      return false;
    }
  }

}