
class ValueBoolData {
  bool? value;

   ValueBoolData({this.value});
   ValueBoolData.fromJson(Map<String, dynamic> jsonMap) {
     value = jsonMap['value'];
   }
   Map<String, dynamic> toJson() => {
        "value": value,
      };
}