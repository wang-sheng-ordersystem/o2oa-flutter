import 'package:get/get.dart';

import 'controller.dart';

class FolderPickerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FolderPickerController>(() => FolderPickerController());
  }
}
