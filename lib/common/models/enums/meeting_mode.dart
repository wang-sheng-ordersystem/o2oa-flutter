import 'package:get/get.dart';

enum MeetingMode { online, offline }

extension MeetingModeExt on MeetingMode {
  String getName() {
    switch (this) {
      case MeetingMode.online:
        return 'meeting_detail_mode_online'.tr;
      case MeetingMode.offline:
        return 'meeting_detail_mode_offline'.tr;
    }
  }

  String getKey() {
    switch (this) {
      case MeetingMode.online:
        return 'online';
      case MeetingMode.offline:
        return 'offline';
    }
  }
}
