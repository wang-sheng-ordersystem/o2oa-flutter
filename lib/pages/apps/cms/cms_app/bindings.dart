import 'package:get/get.dart';

import 'controller.dart';

class CmsAppBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CmsAppController>(() => CmsAppController());
  }
}
