import 'package:get/get.dart';

import '../api/index.dart';
import '../models/index.dart';
import '../routers/index.dart';
import 'o2_api_manager.dart';

/// 演示版本
class SampleEditionManger {
  static final SampleEditionManger instance = SampleEditionManger._internal();
  factory SampleEditionManger() => instance;
  SampleEditionManger._internal();

  O2CloudServer? _currentO2Server;
  List<O2CloudServer> _serverList = [];

  List<O2CloudServer> getServerList() => _serverList;


  /// 获取演示服务器列表
  Future<void> loadServerListFromWwwService() async {
    final list = await O2OAWwwService.to.executeSampleServerListShell();
    _serverList = list ?? [];
    if (_serverList.isNotEmpty && _currentO2Server == null) {
      _currentO2Server = _serverList[0];
    }
  }


  /// 获取当前连接的 o2oa服务器
  O2CloudServer getCurrent() {
    _currentO2Server ??= O2CloudServer(
        id: 'sample',
        name: '企业通用办公平台',
        centerHost: 'sample.o2oa.net',
        centerContext: '/x_program_center',
        centerPort: 443,
        httpProtocol: 'https');
    return _currentO2Server!;
  }

  /// 切换连接的 o2oa服务器
  Future<void> setCurrentAndReloadApp(O2CloudServer current, {needReload = false}) async {
    _currentO2Server = current;
    await O2ApiManager.instance.putO2UnitJson2SP(current);
    if (needReload) {
      Get.offNamed(O2OARoutes.splash);
    }
  }
}
