///
///登录模式 
///是否开启短信验证码登录
///是否开启图片验证码登录
///等

class LoginModeData {
  bool? captchaLogin; //图片验证码登录
  bool? codeLogin; //短信验证码登录
  bool? bindLogin;
  bool? faceLogin;

  LoginModeData({
    this.captchaLogin = false,
    this.codeLogin = false,
    this.bindLogin = false,
    this.faceLogin = false,
  });

  LoginModeData.fromJson(Map<String, dynamic>? jsonMap) {
    if (jsonMap != null) {
      captchaLogin = jsonMap['captchaLogin'] ?? false;
      codeLogin = jsonMap['codeLogin'] ?? false;
      bindLogin = jsonMap['bindLogin'] ?? false;
      faceLogin = jsonMap['faceLogin'] ?? false;
    }
  }

   Map<String, dynamic> toJson() => {
     'captchaLogin': captchaLogin,
     'codeLogin': codeLogin,
     'bindLogin': bindLogin,
     'faceLogin': faceLogin,
   };
}
