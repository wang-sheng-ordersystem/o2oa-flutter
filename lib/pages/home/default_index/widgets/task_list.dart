import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../index.dart';

/// List
class TaskListWidget extends GetView<DefaultIndexController> {
  const TaskListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Column(
          mainAxisSize: MainAxisSize.max,
          children: controller.state.taskList.map((task) {
            return Container(
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(Radius.circular(10)),
                  color: Theme.of(context).colorScheme.background,
                ),
                margin: EdgeInsets.only(bottom: 10.w, left: 10.w, right: 10.w),
                child: ListTile(
                  title: Text(
                    (task.title == null || task.title?.isEmpty == true)
                        ? 'process_work_no_title'
                            .trArgs(['${task.processName}'])
                        : task.title!,
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  subtitle: Text(
                    task.processName ?? '',
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  trailing: Text(task.startTime?.substring(0, 10) ?? '',
                      style: Theme.of(context).textTheme.bodySmall),
                  onTap: () => controller.tapTask(task),
                ));
          }).toList(),
        ));
  }
}
