import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class ContactPickerState {
 
  final RxList<ContactPickMode> tabs = <ContactPickMode>[].obs;

  /// 已经选中的身份列表
  final RxList<O2Identity> identityPickedList = <O2Identity>[].obs;
  /// 已经选中的人员列表 身份对象需要转人员对象
  final RxList<O2Identity> personPickedList = <O2Identity>[].obs;
  /// 已经选中的组织列表
  final RxList<O2Unit> unitPickedList = <O2Unit>[].obs;
  /// 已经选中的群组列表
  final RxList<O2Group> groupPickedList = <O2Group>[].obs;

}
