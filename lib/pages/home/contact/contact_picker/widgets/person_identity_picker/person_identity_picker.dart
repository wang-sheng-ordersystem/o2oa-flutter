import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../../common/models/index.dart';
import '../../../../../../common/widgets/index.dart';
import 'index.dart';

class PersonOrIdentityPickerPage extends StatefulWidget {
  const PersonOrIdentityPickerPage(
      {Key? key, required this.mode, required this.initData})
      : super(key: key);
  final ContactPickMode mode;
  final ContactPickerArguments initData;
  @override
  _PersonOrIdentityPickerPageState createState() =>
      _PersonOrIdentityPickerPageState();
}

class _PersonOrIdentityPickerPageState extends State<PersonOrIdentityPickerPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return _PersonOrIdentityPickerViewGetX(
        mode: widget.mode, initData: widget.initData);
  }
}

class _PersonOrIdentityPickerViewGetX
    extends GetView<PersonOrIdentityPickerController> {
  const _PersonOrIdentityPickerViewGetX(
      {Key? key, required this.mode, required this.initData})
      : super(key: key);
  final ContactPickMode mode;
  final ContactPickerArguments initData;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PersonOrIdentityPickerController>(
      init: PersonOrIdentityPickerController(mode: mode, initData: initData),
      id: "person_identity",
      builder: (_) {
        return Padding(
            padding: EdgeInsets.only(top: 10.h, left: 10.w, right: 10.w),
            child: ListView(
              children: [
                breadcrumbView(context),
                const SizedBox(height: 10),
                contactListView(context)
              ],
            ));
      },
    );
  }

  Widget contactListView(BuildContext context) {
    return Column(
      children: [
        Obx(() => listBoxView(context, Column(
              children: controller.orgList.map((element) {
                var orgName = element.name ?? '';
                var oneName = orgName;
                if (orgName.length > 1) {
                  oneName = oneName.substring(0, 1);
                }
                return ListTile(
                  leading: SizedBox(
                    width: 32,
                    height: 32,
                    child: CircleAvatar(
                      radius: 32,
                      backgroundColor: Theme.of(context).colorScheme.primary,
                      child: Text(oneName,
                          style: const TextStyle(color: Colors.white)),
                    ),
                  ),
                  title: Text(orgName),
                  trailing: O2UI.rightArrow(),
                  onTap: () => controller.clickEnterOrg(element),
                );
              }).toList(),
            ))),
        const SizedBox(height: 10),
        Obx(() => listBoxView(context, Column(
              children: controller.identityList.map((element) {
                String? personIcon = controller.personIconUrl(element.person!);
                return CheckboxListTile(
                  secondary: SizedBox(
                    width: 32,
                    height: 32,
                    child: CircleAvatar(
                      radius: 32,
                      backgroundColor: Colors.white,
                      backgroundImage:
                          NetworkImage(personIcon, headers: controller.headers),
                    ),
                  ),
                  title: Text(element.name ?? ''),
                  value: controller.isItemChecked(element),
                  onChanged: (id)=>controller.onItemCheckedChange(element, id),
                );
              }).toList(),
            )))
      ],
    );
  }

  Widget listBoxView(BuildContext context, Widget body) {
    return Container(
        width: double.infinity,
        decoration:  BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            color: Theme.of(context).colorScheme.background),
        child: body);
  }

  Widget breadcrumbView(BuildContext context) {
    return Container(
      height: 48.h,
      width: double.infinity,
      decoration:  BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: Theme.of(context).colorScheme.background),
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Obx(
            () => Row(
                children: controller.breadcrumbBeans.map((element) {
              var isLast = (element.key == controller.breadcrumbBeans.last.key);
              if (isLast) {
                return TextButton(
                    onPressed: () {},
                    child: Text(element.name,
                        style: Theme.of(context).textTheme.bodyLarge?.copyWith(color: Theme.of(context).colorScheme.primary)
                             ));
              }
              return Row(children: [
                TextButton(
                    onPressed: () => controller.clickBreadcrumb(element),
                    child: Text(element.name,
                        style: Theme.of(context).textTheme.bodyLarge)),
                Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(">",
                        style: Theme.of(context).textTheme.bodyLarge))
              ]);
            }).toList()),
          )),
    );
  }
}
