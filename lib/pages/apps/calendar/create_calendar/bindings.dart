import 'package:get/get.dart';

import 'controller.dart';

class CreateCalendarBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CreateCalendarController>(() => CreateCalendarController());
  }
}
