import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/models/index.dart';
import '../../../common/widgets/index.dart';
import 'index.dart';

class BbsPage extends GetView<BbsController> {
  const BbsPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView(BuildContext context) {
    return Obx(() => Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: ListView.builder(
            itemCount: controller.state.forumList.length,
            itemBuilder: (context, index) {
              var forumInfo = controller.state.forumList[index];
              var sectionList = forumInfo.sectionInfoList ?? [];
              return Column(children: [
                Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(forumInfo.forumName ?? '')),
                GridView.count(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  crossAxisCount: 2,
                  mainAxisSpacing: 10.0, //设置上下之间的间距(Axis.horizontal设置左右之间的间距)
                  crossAxisSpacing: 10.0, //设置左右之间的间距(Axis.horizontal设置上下之间的间距)
                  childAspectRatio: 0.8, //设置宽高比
                  children: sectionList
                      .map((element) => Card(
                            elevation: 8.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: GestureDetector(
                                onTap: () => controller.clickSection(element),
                                child: appItem(context, element)),
                          ))
                      .toList(),
                )
              ]);
            })));
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BbsController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title:  Obx(()=>Text(controller.state.title))),
          body: SafeArea(
              child: Padding(
            padding:
                const EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 15),
            child: _buildView(context),
          )),
        );
      },
    );
  }

  Widget appItem(BuildContext context, SectionInfoList app) {
    var time = app.updateTime ?? '';
    if (time.length >= 10) {
      time = time.substring(5, 10);
    }
    return Stack(
      children: [
        Align(
            alignment: Alignment.topCenter,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(app.sectionName ?? '',
                    style: const TextStyle(fontSize: 18)))),
        Positioned(
            bottom: 0,
            left: 0,
            right: 50,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(time,
                    maxLines: 1,
                    softWrap: true,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.bodyMedium))),
        Align(
            alignment: Alignment.bottomRight,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                    'bbs_subject_number'.trArgs(['${app.subjectTotal ?? 0}']),
                    style: Theme.of(context).textTheme.bodyMedium))),
        Align(
            alignment: Alignment.center,
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: controller.iconBase64(app.icon) == null
                    ? AssetsImageView(
                        'unknow.png',
                        width: 100.w,
                        height: 100.w,
                      )
                    : Image.memory(
                        controller.iconBase64(app.icon)!,
                        width: 100.w,
                        height: 100.w,
                        fit: BoxFit.cover,
                      ))),
      ],
    );
  }
}
