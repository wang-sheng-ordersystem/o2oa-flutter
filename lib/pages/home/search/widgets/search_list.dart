import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:o2oa_all_platform/common/extension/index.dart';

import '../../../../common/models/index.dart';
import '../controller.dart' as search;

/// List
class SearchListView extends GetView<search.SearchController> {
  const SearchListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Obx(() => Padding(
              padding: EdgeInsets.only(top: 10.w, left: 14.w, right: 14.w),
              child: SmartRefresher(
                  enablePullDown: true,
                  enablePullUp: controller.state.hasMoreData,
                  controller: controller.refreshController,
                  onRefresh: () => controller.refreshSearchList(),
                  onLoading: () => controller.loadMoreSearchList(),
                  child: ListView.separated(
                      itemBuilder: (context, index) {
                        O2SearchV2Entry entry =
                            controller.state.resultList[index];
                        var time = entry.updateTime ?? '';
                        if (time.length > 10) {
                          time = time.substring(0, 10);
                        }
                        return InkWell(
                            onTap: () => controller.tapEntry(entry),
                            child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(10)),
                                  color:
                                      Theme.of(context).colorScheme.background,
                                ),
                                padding: const EdgeInsets.all(10),
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          flex: 2,
                                          child: Text(
                                            entry.title ?? '',
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodyLarge?.copyWith(
                                                  fontWeight: FontWeight.bold
                                                ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Text(
                                            time,
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .hintColor),
                                            textAlign: TextAlign.end,
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(height: 10),
                                    Text.rich(
                                        highlightText(
                                            entry.summary ?? '',
                                            controller.state.searchKey,
                                            Colors.red,
                                            defaultStyle: Theme.of(context)
                                                .textTheme
                                                .bodyMedium),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2),
                                    const SizedBox(height: 10),
                                    const Divider(height: 1),
                                    const SizedBox(height: 10),
                                    Row(
                                      children: [
                                        Expanded(
                                          flex: 1,
                                          child: Text(
                                            entry.creatorUnit?.o2NameCut() ??
                                                '',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .hintColor),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Text(
                                            entry.creatorPerson?.o2NameCut() ??
                                                '',
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .hintColor),
                                            textAlign: TextAlign.end,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                )));
                      },
                      separatorBuilder: (context, index) {
                        return const Divider(
                          height: 10,
                          color: Colors.transparent,
                        );
                      },
                      itemCount: controller.state.resultList.length)),
            )));
  }

  /// 搜索关键字 高亮
  TextSpan highlightText(String text, String highlight, Color color,
      {TextStyle? defaultStyle}) {
    final startIndex = text.indexOf(highlight);
    if (startIndex < 0) {
      return TextSpan(text: text, style: defaultStyle);
    }
    final endIndex = startIndex + highlight.length;
    final TextStyle highlightedStyle = TextStyle(color: color);
    return TextSpan(
      text: text.substring(0, startIndex),
      style: defaultStyle,
      children: <TextSpan>[
        TextSpan(
          text: text.substring(startIndex, endIndex),
          style: highlightedStyle,
        ),
        TextSpan(
          text: text.substring(endIndex),
          style: defaultStyle,
        ),
      ],
    );
  }
}
