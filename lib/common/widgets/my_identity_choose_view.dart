 

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../models/index.dart';


typedef MyIdentityChooseViewOnChange = Function(String identityDn);


class MyIdentityChooseView extends StatefulWidget {
  const MyIdentityChooseView({super.key, required this.identityList, this.onChange});
  final List<O2Identity> identityList;
  final MyIdentityChooseViewOnChange? onChange;

  @override
  State<MyIdentityChooseView> createState() => _MyIdentityChooseViewState();
}

class _MyIdentityChooseViewState extends State<MyIdentityChooseView> {
  String selectIdentityDn = '';

  @override
  Widget build(BuildContext context) {

    List<Widget> all = topTitle();
    all.addAll(identityViewList());
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: all,
    );
  }

  void clickIdentity(O2Identity id) {
    setState(() {
      selectIdentityDn = id.distinguishedName ?? '';
      if (widget.onChange != null) {
        widget.onChange!(selectIdentityDn);
      }
    });
  }

  List<Widget> topTitle() {
    return [
      Text('contact_picker_identity_title'.tr, style: Theme.of(context).textTheme.bodyLarge),
      const SizedBox(height: 10)
    ];
  }

  List<Widget> identityViewList() {
    if (selectIdentityDn.isEmpty && widget.identityList.isNotEmpty) {
      selectIdentityDn = widget.identityList[0].distinguishedName ?? '';
      if (widget.onChange != null) {
        widget.onChange!(selectIdentityDn);
      }
    }
    return widget.identityList.map((element) {
          final dutyList = element.woUnitDutyList ?? [];
          final duty = dutyList.map((e) => e.name ?? '').join(',');
          return InkWell(
              onTap: () => clickIdentity(element),
              child: cardBox(
                  context,
                  element.distinguishedName == selectIdentityDn,
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 5),
                      Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child: Text(element.name ?? '',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                  textAlign: TextAlign.start)),
                          Expanded(
                              flex: 1,
                              child: Text('【${element.unitName}】',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                  textAlign: TextAlign.end))
                        ],
                      ),
                      const SizedBox(height: 5),
                      Row(
                        children: [
                          Text(
                            'contact_picker_identity_org_title'.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge
                                ?.copyWith(fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                              flex: 1,
                              child: Text('${element.unitLevelName}',
                              style: Theme.of(context).textTheme.bodyMedium)
                          )
                        ],
                      ),
                      const SizedBox(height: 5),
                      Row(
                        children: [
                          Text(
                            'contact_picker_identity_duty_title'.tr,
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge
                                ?.copyWith(fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                              flex: 1,
                              child: Text(duty,
                              style: Theme.of(context).textTheme.bodyMedium))
                        ],
                      ),
                    ],
                  )));
        }).toList();
  }


  Widget cardBox(BuildContext context, bool isSelected, Widget child) {
    return Card(
        margin: const EdgeInsets.only(bottom: 10, left: 15, right: 15),
        shape: RoundedRectangleBorder(
            side: isSelected
                ? BorderSide(color: Theme.of(context).colorScheme.primary)
                : BorderSide.none,
            borderRadius: const BorderRadius.all(Radius.circular(10))),
        child: Padding(padding: const EdgeInsets.all(10), child: child));
  }
}