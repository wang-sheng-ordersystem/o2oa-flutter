import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../common/models/index.dart';
import '../../../common/widgets/index.dart';
import 'index.dart';
import 'widgets/widgets.dart';

class YunpanPage extends GetView<YunpanController> {
  const YunpanPage({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView() {
    return Obx(() => Column(
          children: [
            Visibility(
                visible: controller.state.visibleTop,
                child: const SizedBox(height: 20)),
            Visibility(
                visible: controller.state.visibleTop,
                child: Row(
                  children: [
                    itemView(
                        () => controller.clickGoto(CloudDiskFileType.image),
                        'cloud_disk_type_image'.tr,
                        'clouddisk_icon_image.png'),
                    itemView(
                        () => controller.clickGoto(CloudDiskFileType.office),
                        'cloud_disk_type_document'.tr,
                        'clouddisk_icon_document.png'),
                    itemView(
                        () => controller.clickGoto(CloudDiskFileType.music),
                        'cloud_disk_type_music'.tr,
                        'clouddisk_icon_music.png'),
                  ],
                )),
            Visibility(
                visible: controller.state.visibleTop,
                child: const SizedBox(height: 20)),
            Visibility(
                visible: controller.state.visibleTop,
                child: Row(
                  children: [
                    itemView(
                        () => controller.clickGoto(CloudDiskFileType.movie),
                        'cloud_disk_type_video'.tr,
                        'clouddisk_icon_video.png'),
                    itemView(
                        () => controller.clickGoto(CloudDiskFileType.other),
                        'cloud_disk_type_other'.tr,
                        'clouddisk_icon_application.png'),
                    itemView(() => controller.clickGotoShare(),
                        'cloud_disk_share'.tr, 'clouddisk_icon_share.png'),
                  ],
                )),
            Visibility(
                visible: controller.state.visibleTop,
                child: const SizedBox(height: 20)),
            const Expanded(flex: 1, child: YunpanFileListWidget())
          ],
        ));
  }

  Widget itemView(GestureTapCallback onTap, String title, String imageName) {
    return Expanded(
      flex: 1,
      child: GestureDetector(
        onTap: onTap,
        child: Column(children: [
          AssetsImageView(imageName, width: 48, height: 48),
          const SizedBox(height: 5),
          Text(title),
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<YunpanController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Obx(()=>Text(controller.state.title)), actions: [
            PopupMenuButton<int>(
              icon: const Icon(Icons.more_vert, color: Colors.white),
              itemBuilder: (BuildContext context) => [
                PopupMenuItem<int>(
                  value: 0,
                  child: Text('cloud_disk_action_create_folder'.tr),
                ),
                PopupMenuItem<int>(
                  value: 1,
                  child: Text('cloud_disk_action_upload_file'.tr),
                ),
                PopupMenuItem<int>(
                  value: 2,
                  child: Text('cloud_disk_upload_title'.tr),
                )
              ],
              onSelected: (value) {
                switch (value) {
                  case 0:
                    controller.createFolder();
                    break;
                  case 1:
                    controller.uploadFile();
                    break;
                  case 2:
                    controller.openUploadPage();
                    break;
                  default:
                    break;
                }
              },
            ),
          ]),
          body: SafeArea(
              child: Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Stack(
                    children: [
                      _buildView(),
                      // 动画 丢文件到右上角 表示后台上传开始
                      Obx(() => AnimatedPositioned(
                            right: controller.state.animRight,
                            top: controller.state.animTop,
                            width: controller.state.animWidth,
                            height: controller.state.animWidth,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeInCubic,
                            child: Visibility(
                                visible: controller.state.visibleAnimIcon,
                                child: const AssetsImageView(
                                    'icon_file_unkown.png',
                                    fit: BoxFit.fill)),
                            onEnd: () => controller.initAnimElement(),
                          ))
                    ],
                  ))),
        );
      },
    );
  }
}
