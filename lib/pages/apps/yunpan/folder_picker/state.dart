import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class FolderPickerState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

    
  //面包屑
  RxList<FolderBreadcrumbBean> breadcrumbBeans = <FolderBreadcrumbBean>[].obs;

  // 文档列表 包含文件夹和文件
  RxList<CloudDiskItem> cloudDiskItemList = <CloudDiskItem>[].obs;

   
  RxList<CloudDiskItem> cloudDiskItemCheckedList = <CloudDiskItem>[].obs;

}
