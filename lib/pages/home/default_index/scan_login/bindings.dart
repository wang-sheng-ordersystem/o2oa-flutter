import 'package:get/get.dart';

import 'controller.dart';

class ScanLoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ScanLoginController>(() => ScanLoginController());
  }
}
