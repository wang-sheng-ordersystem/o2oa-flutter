

import 'package:app_settings/app_settings.dart';
import 'package:get/get.dart';

import '../../../common/routers/index.dart';
import '../../../common/services/index.dart';
import '../../../common/utils/index.dart';
import '../../../common/widgets/index.dart';
import 'index.dart';

class SettingsController extends GetxController {
  SettingsController();

  final state = SettingsState();

  final eventBus = EventBus();
  final _eventId = 'Settings';

  /// 在 widget 内存中分配后立即调用。
  @override
  void onInit() {
    super.onInit();
  }

  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    eventBus.on(EventBus.avatarUpdateMsg, _eventId, (arg) { 
      OLogger.d('修改头像后刷新');
      state.dn = O2ApiManager.instance.o2User?.distinguishedName ?? '';
    });
    eventBus.on(EventBus.mySignatureUpdateMsg, _eventId, (arg) { 
      OLogger.d('修改签名后刷新');
      state.personSign = O2ApiManager.instance.o2User?.signature ?? '';
    });
    state.personName = O2ApiManager.instance.o2User?.name ?? '';
    state.personSign = O2ApiManager.instance.o2User?.signature ?? '';
    super.onReady();
  }

  /// 在 [onDelete] 方法之前调用。
  @override
  void onClose() {
    eventBus.off(EventBus.avatarUpdateMsg, _eventId);
    eventBus.off(EventBus.mySignatureUpdateMsg, _eventId);
    super.onClose();
  }

  /// dispose 释放内存
  @override
  void dispose() {
    super.dispose();
  }

 
 
  /// 
  /// 消息通知设置
  /// 
  void clickOpenNotifySettings() {
    if (GetPlatform.isMobile) {
      AppSettings.openNotificationSettings();
    }
  }

  void clickLogs() {
    Get.toNamed(O2OARoutes.homeSettingsLogs);
  }

  void clickAbout() {
    Get.toNamed(O2OARoutes.homeSettingsAbout);
  }
  void longPressAbout() async {
    final isDebugger = SharedPreferenceService.to.getBool(SharedPreferenceService.webviewDebuggerKey);
    await SharedPreferenceService.to.putBool(SharedPreferenceService.webviewDebuggerKey, !isDebugger);
    if (isDebugger) {
      Loading.toast('common_debugger_close'.tr);
    } else {
      Loading.toast('common_debugger_open'.tr);
    }
  }
  
  /// 通用
  gotoCommon() {
    Get.toNamed(O2OARoutes.homeSettingsCommon);
  }

  ///
  /// 退出登录
  /// 
  void clickLogout() async {
    OLogger.d("退出登录");
    O2UI.showConfirm(Get.context, 'settings_logout_confirm_message'.tr, okPressed: ()=> O2Utils.logout());
  }
 

  
}
