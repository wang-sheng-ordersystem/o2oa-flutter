import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../../common/models/index.dart';
import 'index.dart';

class OrgPickerPage extends StatefulWidget {
  const OrgPickerPage({Key? key, required this.initData}) : super(key: key);
   final ContactPickerArguments initData;
  @override
  _OrgPickerPageState createState() => _OrgPickerPageState();
}

class _OrgPickerPageState extends State<OrgPickerPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return _OrgPickerViewGetX(initData: widget.initData);
  }
}

class _OrgPickerViewGetX extends GetView<OrgPickerController> {
  const _OrgPickerViewGetX({Key? key, required this.initData}) : super(key: key);
  final ContactPickerArguments initData;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrgPickerController>(
      init: OrgPickerController(initData: initData),
      id: "org_picker",
      builder: (_) {
        return Padding(
            padding: EdgeInsets.only(top: 10.h, left: 10.w, right: 10.w),
            child: ListView( children: [
                breadcrumbView(context),
                const SizedBox(height: 10),
                Obx(() => listBoxView(context, Column(
                    children: controller.orgList.map((element) {
                      var orgName = element.name ?? '';
                      var oneName = orgName;
                      if (orgName.length > 1) {
                        oneName = oneName.substring(0, 1);
                      }
                      return CheckboxListTile(
                        secondary: TextButton(onPressed: (() => controller.clickEnterOrg(element)), child: Text('contact_picker_btn_title_next_level'.tr)),
                        title: Text(orgName),
                        controlAffinity: ListTileControlAffinity.leading,
                        value: controller.isItemChecked(element),
                        onChanged: (id)=>controller.onItemCheckedChange(element, id),
                        // onTap: () => controller.clickEnterOrg(element),
                      );
                    }).toList(),
                  )))
              ],
            ));
      },
    );
  }

  Widget listBoxView(BuildContext context, Widget body) {
      return Container(
          width: double.infinity,
          decoration:  BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              color: Theme.of(context).colorScheme.background),
          child: body);
    }

  Widget breadcrumbView(BuildContext context) {
    return Container(
      height: 48.h,
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: Theme.of(context).colorScheme.background),
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Obx(
            () => Row(
                children: controller.breadcrumbBeans.map((element) {
              var isLast = (element.key == controller.breadcrumbBeans.last.key);
              if (isLast) {
                return TextButton(
                    onPressed: () {},
                    child: Text(element.name,
                        style:
                            Theme.of(context).textTheme.bodyLarge?.copyWith(color: Theme.of(context).colorScheme.primary)));
              }
              return Row(children: [
                TextButton(
                    onPressed: () => controller.clickBreadcrumb(element),
                    child: Text(element.name,
                        style: Theme.of(context).textTheme.bodyLarge)),
                 Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(">",
                        style: Theme.of(context).textTheme.bodyLarge))
              ]);
            }).toList()),
          )),
    );
  }
}
