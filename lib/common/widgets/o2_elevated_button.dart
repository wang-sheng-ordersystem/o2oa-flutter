import 'package:flutter/material.dart';

class O2ElevatedButton extends StatelessWidget {
  const O2ElevatedButton(
    this.onPressed,
    this.child, {
    Key? key,
    this.radius = 20,
    this.isPrimaryBackgroundColor = true,
    this.onLongPress,
    this.focusNode,
    this.autofocus = false,
    this.clipBehavior = Clip.none,
  }) : super(key: key);

  final VoidCallback? onPressed;
  final Widget? child;
  final VoidCallback? onLongPress;
  final FocusNode? focusNode;
  final bool autofocus;
  final Clip clipBehavior;
  final double radius;
  final bool isPrimaryBackgroundColor;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        onLongPress: onLongPress,
        style: ButtonStyle(
          backgroundColor: buttonBackgroundColor(context),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20))), //圆角弧度
        ),
        focusNode: focusNode,
        autofocus: autofocus,
        clipBehavior: clipBehavior,
        child: child);
  }

  MaterialStateProperty<Color?> buttonBackgroundColor(BuildContext context) {
    if (!isPrimaryBackgroundColor) {
      return MaterialStateProperty.resolveWith((states) {
        if (states.contains(MaterialState.disabled)) {
          return null;
        } else {
          return Theme.of(context).colorScheme.background;
        }
      });
    }
    return MaterialStateProperty.resolveWith((states) {
      if (states.contains(MaterialState.disabled)) {
        return null;
      } else {
        return Theme.of(context).colorScheme.primary;
      }
    });
  }
}
