
class O2 {
  static const String o2LogTag = 'O2OA';
  /// log日志存储目录 
  static const String logFilePathName = 'o2_log';

  // 默认token名称
  static const String defaultTokenName = 'x-token';

  // o2 云 服务地址
  static const String o2CloudAPIUrl = "https://collect.o2oa.net:40080/o2_collect_assemble/";

  // 官网 o2oa 服务器
  static const String o2oaWwwUrl = "https://www.o2oa.net/oa/";

  // 用户协议 地址
  static const String userAgreementUrl = 'https://www.o2oa.net/userService.html';
  // 隐私政策 地址
  static const String privacyPolicyUrl = 'https://www.o2oa.net/secret.html';

  // 极光推送appKey
  static const String jPushAppKey = "9aca7cc20fe0cc987cd913ca";
  static const String jPushAppChannel = "o2oa_flutter_channel";
  static const String jPushType = "jpush";


  // 
  static const String rsaEncryptedYes = 'y';
  static const String rsaEncryptedNo = 'n';
  static const String rasPemKeyHeader = '-----BEGIN PUBLIC KEY-----\n';


  // 默认分页
  static const int o2DefaultPageSize = 15;
  static const String o2DefaultPageFirstKey = "(0)";

  static const String o2DefaultUnitParentId = "-1"; // 顶级id

  // webview JavascriptChannel的名称
  static const String webviewChannelNameCommonKey = "o2android";

  static const String webviewUserAgent = "O2OA";


  /// im
  static const String imConversationTypeSingle = "single";
  static const String imConversationTypeGroup = "group";

  /// message type
  static const String o2MessageTypeIMCreate = 'im_create'; // 聊天消息
  static const String o2MessageTypeIMRevoke = 'im_revoke'; // 聊天消息撤回
  static const String o2MeesageTypeIMConversationUpdate = 'im_conv_update'; // 会话更新
  static const String o2MeesageTypeIMConversationDelete = 'im_conv_delete'; // 会话删除

  // custom key
  static const String myCustomNameMyAppList = 'myAppList'; // 自定义数据的key

  static const String myCustomIdea = 'idea'; // 常用意见的 key


  /// 通讯录权限 视图Key
  static const String addressPowerView = 'addressPowerView'; // 自定义数据的key
  
  static const String hiddenMobileNumber = '***********'; // 隐藏手机号码

  static const int BBS_IMAGE_MAX_WIDTH = 720;

  // 极速打卡通知 id
  static const int attendanceFastCheckInNotificationId = 1204;
  
}