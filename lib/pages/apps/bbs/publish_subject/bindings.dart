import 'package:get/get.dart';

import 'controller.dart';

class PublishSubjectBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PublishSubjectController>(() => PublishSubjectController());
  }
}
