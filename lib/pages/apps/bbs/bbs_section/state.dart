import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class BbsSectionState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  RxList<SubjectInfo> subjectList = <SubjectInfo>[].obs;

   // 是否有更多翻页数据
  final _hasMoreData = true.obs;
  set hasMoreData(bool value) => _hasMoreData.value = value;
  bool get hasMoreData => _hasMoreData.value;

  // 是否能发帖
  final _subjectPublishAble = false.obs;
  set subjectPublishAble(bool value) => _subjectPublishAble.value = value;
  bool get subjectPublishAble => _subjectPublishAble.value;

}
