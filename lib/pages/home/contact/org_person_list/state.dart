import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class OrgPersonListState {
  
  
    //面包屑
  final RxList<ContactBreadcrumbBean> breadcrumbBeans = <ContactBreadcrumbBean>[].obs;
  // 组织列表
  final RxList<O2Unit> orgList = <O2Unit>[].obs;
  // 人员列表
  final RxList<O2Person> personList = <O2Person>[].obs;
}
