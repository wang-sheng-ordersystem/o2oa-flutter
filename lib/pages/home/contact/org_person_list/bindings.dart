import 'package:get/get.dart';

import 'controller.dart';

class OrgPersonListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OrgPersonListController>(() => OrgPersonListController());
  }
}
