




///
/// 配置的打卡对象
/// 
class Feature {
    Feature({
        this.signSeq,
        this.signDate,
        this.signTime,
        this.checkinType,
        this.signTimeRecord,
        this.lastRecord,
        this.checkinStatus,
        this.checkinTime,
        this.recordId
    });

    int? signSeq;
    String? signDate; // 需要打卡日期
    String? signTime; // 需要打卡时间
    String? checkinType; // 打卡类型 上午上班打卡...

    // 已打卡的时间
    String? signTimeRecord;
    Record? lastRecord; // 最后一次打卡记录，更新打卡用

    String? checkinStatus; // 未打卡 已打卡
    String? checkinTime; //打卡时间
    String? recordId; //打卡结果的id 更新打卡用

    factory Feature.fromJson(Map<String, dynamic> json) => Feature(
        signSeq: json["signSeq"],
        signDate: json["signDate"],
        signTime: json["signTime"],
        checkinType: json["checkinType"],
        signTimeRecord: json["signTimeRecord"],
        checkinStatus: json["checkinStatus"],
        checkinTime: json["checkinTime"],
        recordId: json["recordId"],
        lastRecord: json["lastRecord"] == null ? null: Record.fromJson( json["lastRecord"]),
    );

    Map<String, dynamic> toJson() => {
        "signSeq": signSeq,
        "signDate": signDate,
        "signTime": signTime,
        "checkinType": checkinType,
        "signTimeRecord": signTimeRecord,
        "checkinStatus": checkinStatus,
        "checkinTime": checkinTime,
        "recordId": recordId,
        "lastRecordId": lastRecord == null ? null : lastRecord!.toJson(),
    };
}


///
/// 打卡记录对象
/// 
class Record {
    Record({
        this.id,
        this.empNo,
        this.empName,
        this.recordDateString,
        this.recordDate,
        this.checkinType,
        this.checkinTime,
        this.signTime,
        this.signDescription,
        this.recordAddress,
        this.longitude,
        this.latitude,
        this.optMachineType,
        this.recordStatus,
        this.workAddress,
        this.isExternal,
        this.createTime,
        this.updateTime,
    });

    String? id;
    String? empNo;
    String? empName;
    String? recordDateString;
    String? recordDate;
    String? checkinType;
    int? checkinTime;
    String? signTime;
    String? signDescription;
    String? recordAddress;
    String? longitude;
    String? latitude;
    String? optMachineType;
    int? recordStatus;
    String? workAddress;
    bool? isExternal;
    String? createTime;
    String? updateTime;

    factory Record.fromJson(Map<String, dynamic> json) => Record(
        id: json["id"],
        empNo: json["empNo"],
        empName: json["empName"],
        recordDateString: json["recordDateString"],
        recordDate: json["recordDate"],
        checkinType: json["checkin_type"],
        checkinTime: json["checkin_time"],
        signTime: json["signTime"],
        signDescription: json["signDescription"],
        recordAddress: json["recordAddress"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        optMachineType: json["optMachineType"],
        recordStatus: json["recordStatus"],
        workAddress: json["workAddress"],
        isExternal: json["isExternal"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "empNo": empNo,
        "empName": empName,
        "recordDateString": recordDateString,
        "recordDate": recordDate,
        "checkin_type": checkinType,
        "checkin_time": checkinTime,
        "signTime": signTime,
        "signDescription": signDescription,
        "recordAddress": recordAddress,
        "longitude": longitude,
        "latitude": latitude,
        "optMachineType": optMachineType,
        "recordStatus": recordStatus,
        "workAddress": workAddress,
        "isExternal": isExternal,
        "createTime": createTime,
        "updateTime": updateTime,
    };
}

///
/// 考勤班次设置信息
/// 
class ScheduleSetting {
    ScheduleSetting({
        this.id,
        this.topUnitName,
        this.unitName,
        this.unitOu,
        this.onDutyTime,
        this.middayRestStartTime,
        this.middayRestEndTime,
        this.offDutyTime,
        this.signProxy,
        this.lateStartTime,
        this.lateStartTimeAfternoon,
        this.absenceStartTime,
        this.leaveEarlyStartTime,
        this.leaveEarlyStartTimeMorning,
        this.createTime,
        this.updateTime,
    });

    String? id;
    String? topUnitName;
    String? unitName;
    String? unitOu;
    String? onDutyTime;
    String? middayRestStartTime;
    String? middayRestEndTime;
    String? offDutyTime;
    int? signProxy;
    String? lateStartTime;
    String? lateStartTimeAfternoon;
    String? absenceStartTime;
    String? leaveEarlyStartTime;
    String? leaveEarlyStartTimeMorning;
    String? createTime;
    String? updateTime;

    factory ScheduleSetting.fromJson(Map<String, dynamic> json) => ScheduleSetting(
        id: json["id"],
        topUnitName: json["topUnitName"],
        unitName: json["unitName"],
        unitOu: json["unitOu"],
        onDutyTime: json["onDutyTime"],
        middayRestStartTime: json["middayRestStartTime"],
        middayRestEndTime: json["middayRestEndTime"],
        offDutyTime: json["offDutyTime"],
        signProxy: json["signProxy"],
        lateStartTime: json["lateStartTime"],
        lateStartTimeAfternoon: json["lateStartTimeAfternoon"],
        absenceStartTime: json["absenceStartTime"],
        leaveEarlyStartTime: json["leaveEarlyStartTime"],
        leaveEarlyStartTimeMorning: json["leaveEarlyStartTimeMorning"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "topUnitName": topUnitName,
        "unitName": unitName,
        "unitOu": unitOu,
        "onDutyTime": onDutyTime,
        "middayRestStartTime": middayRestStartTime,
        "middayRestEndTime": middayRestEndTime,
        "offDutyTime": offDutyTime,
        "signProxy": signProxy,
        "lateStartTime": lateStartTime,
        "lateStartTimeAfternoon": lateStartTimeAfternoon,
        "absenceStartTime": absenceStartTime,
        "leaveEarlyStartTime": leaveEarlyStartTime,
        "leaveEarlyStartTimeMorning": leaveEarlyStartTimeMorning,
        "createTime": createTime,
        "updateTime": updateTime,
    };
}





class ScheduleInfo {
    ScheduleInfo({
        this.records,
        this.scheduleSetting,
        this.feature,
        this.scheduleInfos,
    });

    List<Record>? records;
    ScheduleSetting? scheduleSetting;
    Feature? feature;
    List<Feature>? scheduleInfos;

    factory ScheduleInfo.fromJson(Map<String, dynamic> json) => ScheduleInfo(
        records: json["records"] == null ? [] : List<Record>.from(json["records"]!.map((x) => Record.fromJson(x))),
        scheduleSetting: json["scheduleSetting"] == null ? null : ScheduleSetting.fromJson(json["scheduleSetting"]),
        feature: json["feature"] == null ? null : Feature.fromJson(json["feature"]),
        scheduleInfos: json["scheduleInfos"] == null ? [] : List<Feature>.from(json["scheduleInfos"]!.map((x) => Feature.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "records": records == null ? [] : List<Record>.from(records!.map((x) => x.toJson())),
        "scheduleSetting": scheduleSetting == null ? null : scheduleSetting!.toJson(),
        "feature": feature == null ? null : feature!.toJson(),
        "scheduleInfos": scheduleInfos == null ? [] : List<Feature>.from(scheduleInfos!.map((x) => x.toJson())),
    };
}