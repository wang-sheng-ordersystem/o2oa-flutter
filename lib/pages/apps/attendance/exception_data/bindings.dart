import 'package:get/get.dart';

import 'controller.dart';

class ExceptionDataBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ExceptionDataController>(() => ExceptionDataController());
  }
}
