import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:local_notifier/local_notifier.dart';
import 'package:logger/logger.dart';
import 'package:o2oa_all_platform/environment_config.dart';

import 'common/api/index.dart';
import 'common/services/index.dart';
import 'common/utils/index.dart';
import 'pages/apps/yunpan/cloud_disk_helpler.dart';

/// 全局的服务
class Global {

  /// 初始化一些全局服务
  static Future init() async {
    // 默认系统竖屏
    WidgetsFlutterBinding.ensureInitialized();
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    // 日志
    Logger.level = EnvironmentConfig.isRelease() ? Level.info : Level.verbose;
   
    Loading();
    // 服务类
    Get.put(O2OAWwwService());
    Get.put(ProgramCenterService());
    Get.put(CollectService());
    Get.put(OrgAuthenticationService());
    Get.put(OrganizationControlService());
    Get.put(OrganizationAssembleExpressService());
    Get.put(OrganizationPersonalService());
    Get.put(OrganizationPermissionService());
    Get.put(ProcessSurfaceService());
    Get.put(JpushAssembleControlService());
    Get.put(HotpicAssembleControlService());
    Get.put(CmsAssembleControlService());
    Get.put(MessageCommunicationService());
    Get.put(PortalSurfaceService());
    Get.put(FileAssembleService());
    Get.put(MindMapService());
    Get.put(MeetingAssembleService());
    Get.put(BBSAssembleControlService());
    Get.put(AttendanceAssembleControlService());
    // Get.put(PanAssembleControlService());
    Get.put(QueryAssembleSurfaceService());
    Get.put(CalendarAssembleControlService());
    Get.put(AppPackingClientAssembleControlService());
    // 存储对象
    await Get.putAsync<SharedPreferenceService>(() => SharedPreferenceService().init());

    // 网盘初始化
    CloudDiskHelper().initProgressQueue();
    
    // webview debug使用 Android上可以使用chrome://inspect 调试
    final isDebugger = SharedPreferenceService.to.getBool(SharedPreferenceService.webviewDebuggerKey);
    if (Platform.isAndroid && isDebugger) {
      await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
    }
    // 存储当前设备类型
    if (Platform.isAndroid) {
      SharedPreferenceService.to.putString(SharedPreferenceService.currentDeviceTypeKey, 'android');
    } else if (Platform.isIOS) {
      SharedPreferenceService.to.putString(SharedPreferenceService.currentDeviceTypeKey, 'ios');
    } else if (Platform.isMacOS) {
      SharedPreferenceService.to.putString(SharedPreferenceService.currentDeviceTypeKey, 'macos');
    } else if (Platform.isWindows) {
      SharedPreferenceService.to.putString(SharedPreferenceService.currentDeviceTypeKey, 'windows');
    } else if (Platform.isLinux) {
      SharedPreferenceService.to.putString(SharedPreferenceService.currentDeviceTypeKey, 'linux');
    }

    // pc 通知插件
    await localNotifier.setup(
      appName: 'appName'.tr,
      shortcutPolicy: ShortcutPolicy.requireCreate,
    );

    // 主题切换监听
    AppLifeCycleDelegate();

    // tts 初始化
    FlutterTTSHelper.instance.init();
    
  }

  /// 需要同意协议后才能继续执行的内容
  static Future<void> initLater() async {
    OLogger.d('initLater。。。。。。。。。。。。。。。。。');
    if (GetPlatform.isMobile) {
      // 启动极光推送插件
      O2ApiManager.instance.jpushInit();
    }
  }
}




class AppLifeCycleDelegate with WidgetsBindingObserver {
  static final AppLifeCycleDelegate _appLifeCycleDelegate =
      AppLifeCycleDelegate._inital();
  AppLifeCycleDelegate._inital() {
    WidgetsBinding.instance.addObserver(this);
  }
  factory AppLifeCycleDelegate() {
    return _appLifeCycleDelegate;
  }

  @override
  void didChangePlatformBrightness() {
    super.didChangePlatformBrightness();
    //强制触发 build
    Get.forceAppUpdate();
  }
}