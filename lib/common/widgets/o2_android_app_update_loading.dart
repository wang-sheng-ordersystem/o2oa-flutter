import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

import '../utils/o2_android_app_update.dart';

class AppUpdateLoading extends StatefulWidget {
  const AppUpdateLoading({Key? key}) : super(key: key);

  @override
  State<AppUpdateLoading> createState() => _LoadingState();
}

class _LoadingState extends State<AppUpdateLoading> {
  double progress = 0.0; //

  @override
  void initState() {
    O2AndroidAppUpdateManager().setDownloadListener((p) {
      setState(() {
        progress = p;
      });
      if (p >= 1.0) {
        closeSelf();
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    O2AndroidAppUpdateManager().clearDownloadListener();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 500),
      curve: Curves.fastLinearToSlowEaseIn,
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: Stack(
          children: [
            Center(
              child: GestureDetector(
                onTap: () => closeSelf(),
                child: Container(
                  height: 200,
                  width: 200,
                  padding: const EdgeInsets.all(4),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(100),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10,
                        offset: Offset(0, 10),
                      ),
                    ],
                  ),
                  child: CircularProgressIndicator(
                    value: progress,
                    strokeWidth: 8,
                  ),
                ),
              ),
            ),
            Center(
              child: SpinKitFadingCube(
                  color: Theme.of(context).colorScheme.primary),
            ),
            Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 240),
                TextButton(
                    child: Text(
                      'common_app_update_back_download_btn'.tr,
                      style: const TextStyle().copyWith(color: Colors.white),
                    ),
                    onPressed: () => closeSelf())
              ],
            )),
          ],
        ),
      ),
    );
  }

  void closeSelf() {
    Navigator.pop(context);
  }
}
