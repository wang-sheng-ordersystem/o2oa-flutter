import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../../../common/models/index.dart';
import 'index.dart';

class GroupPickerPage extends StatefulWidget {
  const GroupPickerPage({Key? key, required this.initData}) : super(key: key);
  final ContactPickerArguments initData;
  @override
  _GroupPickerPageState createState() => _GroupPickerPageState();
}

class _GroupPickerPageState extends State<GroupPickerPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return _GroupPickerViewGetX(initData: widget.initData);
  }
}

class _GroupPickerViewGetX extends GetView<GroupPickerController> {
  const _GroupPickerViewGetX({Key? key, required this.initData})
      : super(key: key);
  final ContactPickerArguments initData;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GroupPickerController>(
      init: GroupPickerController(initData: initData),
      id: "group_picker",
      builder: (_) {
        return Padding(
            padding: EdgeInsets.only(top: 10.h, left: 10.w, right: 10.w),
            child: Obx(() => SmartRefresher(
                enablePullDown: true,
                enablePullUp: controller.hasMoreData.value,
                controller: controller.refreshController,
                onRefresh: () => controller.refreshGroupList(),
                onLoading: () => controller.getMoreGroupList(),
                child: ListView.separated(
                    itemBuilder: (context, index) {
                      final group = controller.groupList[index];
                      final groupName = group.name ?? '';
                      var oneName = groupName;
                      if (groupName.length > 1) {
                        oneName = oneName.substring(0, 1);
                      }
                      return CheckboxListTile(
                        secondary: SizedBox(
                          width: 32,
                          height: 32,
                          child: CircleAvatar(
                            radius: 32,
                            backgroundColor:
                                Theme.of(context).colorScheme.primary,
                            child: Text(oneName,
                                style: const TextStyle(color: Colors.white)),
                          ),
                        ),
                        title: Text(groupName),
                        value: controller.isItemChecked(group),
                        onChanged: (check) =>
                            controller.onItemCheckedChange(group, check),
                        // onTap: () => controller.clickEnterOrg(element),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return const Divider(
                        height: 10,
                        color: Colors.transparent,
                      );
                    },
                    itemCount: controller.groupList.length))));
      },
    );
  }
}
