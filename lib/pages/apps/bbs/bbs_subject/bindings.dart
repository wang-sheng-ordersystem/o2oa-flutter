import 'package:get/get.dart';

import 'controller.dart';

class BbsSubjectBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BbsSubjectController>(() => BbsSubjectController());
  }
}
