
import 'package:get/get.dart';

enum AppIndexModule {
  im,
  contact,
  home,
  app,
  settings
}

extension AppIndexModuleExt on AppIndexModule {

  String name() {
    switch(this) {
      case AppIndexModule.im:
        return 'home_tab_im'.tr;
      case AppIndexModule.contact:
        return 'home_tab_contact'.tr;
      case AppIndexModule.home:
        return 'home_tab_index'.tr;
      case AppIndexModule.app:
        return 'home_tab_apps'.tr;
      case AppIndexModule.settings:
        return 'home_tab_settings'.tr;
    }
  }

  String key() {
    switch(this) {
      case AppIndexModule.im:
        return 'im';
      case AppIndexModule.contact:
        return 'contact';
      case AppIndexModule.home:
        return 'home';
      case AppIndexModule.app:
        return 'app';
      case AppIndexModule.settings:
        return 'settings';
    }
  }

  int orderNumber() {
    switch(this) {
      case AppIndexModule.im:
        return 2;
      case AppIndexModule.contact:
        return 3;
      case AppIndexModule.home:
        return 1;
      case AppIndexModule.app:
        return 4;
      case AppIndexModule.settings:
        return 5;
    }
  }
}