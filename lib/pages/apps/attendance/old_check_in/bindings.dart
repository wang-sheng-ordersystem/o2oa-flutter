import 'package:get/get.dart';

import 'controller.dart';

class OldCheckInBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OldCheckInController>(() => OldCheckInController());
  }
}
