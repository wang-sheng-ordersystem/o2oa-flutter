import 'package:get/get.dart';

class GroupMembersState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

  /// 群名
  final _groupName = "".obs;
  set groupName(value) => _groupName.value = value;
  get groupName => _groupName.value;

   /// 群管理员
  final _groupAdminName = "".obs;
  set groupAdminName(value) => _groupAdminName.value = value;
  get groupAdminName => _groupAdminName.value;

  /// 成员列表
  final RxList<String> memberList = <String>[].obs;

  /// 是否更新群成员
  final _canUpdate = false.obs;
  set canUpdate(bool value) => _canUpdate.value = value;
  bool get canUpdate => _canUpdate.value;


  
}
