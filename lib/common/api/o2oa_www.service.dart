

import 'package:get/get.dart';

import '../models/index.dart';
import '../utils/index.dart';
import '../values/index.dart';

class O2OAWwwService extends GetxService {
  
  static O2OAWwwService get to => Get.find();
 

  String baseUrl() {
    return O2.o2oaWwwUrl;
  }

  
  ///官网服务 获取演示环境服务器列表
  Future<List<O2CloudServer>?> executeSampleServerListShell() async {
    try {
      Map<String, dynamic> body = {};
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}x_program_center/jaxrs/invoke/demo_app_get_server_list/execute', body);
      if (response.isSuccess()) {
        final value = ScriptExecuteResponse.fromJson(response.data);
        final list = value.value == null ? [] :  value.value as List;
        return list.map((e) => O2CloudServer.fromJson(e) ).toList();
      }
    } catch  (err, stackTrace) {
      OLogger.e('获取服务器列表失败', err, stackTrace);
    }
    return null;
  }
  /// 官网服务 获取演示环境的账号列表
  Future<WwwGetSampleAccounts?> executeSampleAccountsShell(WwwGetSampleAccountPost body) async {
    try {
      ApiResponse response = await O2HttpClient.instance.post('${baseUrl()}x_program_center/jaxrs/invoke/demo_app_get_login_accounts/execute', body.toJson());
      if (response.isSuccess()) {
        final value = ScriptExecuteResponse.fromJson(response.data);
        return WwwGetSampleAccounts.fromJson(value.value);
      }
    } catch  (err, stackTrace) {
      OLogger.e('获取账号列表失败', err, stackTrace);
    }
    return null;
  }

}