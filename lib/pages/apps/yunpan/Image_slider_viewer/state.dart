import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class ImageSliderViewerState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;

    // 图片文件列表
  RxList<FileInfo> imageFileList = <FileInfo>[].obs;
}
