import 'package:get/get.dart';

import 'controller.dart';

class MeetingFormBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MeetingFormController>(() => MeetingFormController());
  }
}
