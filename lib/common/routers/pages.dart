import 'package:get/get.dart';

import '../../pages/apps/attendance/exception_data/index.dart';
import '../../pages/apps/attendance/main/index.dart';
import '../../pages/apps/attendance/old_check_in/index.dart';
import '../../pages/apps/bbs/bbs_subject/index.dart';
import '../../pages/apps/bbs/bbs_subject/subject_reply/index.dart';
import '../../pages/apps/bbs/index.dart';
import '../../pages/apps/bbs/bbs_section/index.dart';
import '../../pages/apps/bbs/publish_subject/index.dart';
import '../../pages/apps/calendar/calendar_list/index.dart';
import '../../pages/apps/calendar/create_calendar/index.dart';
import '../../pages/apps/calendar/create_calendar_event/index.dart';
import '../../pages/apps/calendar/index.dart';
import '../../pages/apps/cms/cms_app/index.dart';
import '../../pages/apps/cms/cms_category_picker/index.dart';
import '../../pages/apps/cms/index.dart';
import '../../pages/apps/meeting/index.dart';
import '../../pages/apps/meeting/meeting_accept_list/index.dart';
import '../../pages/apps/meeting/meeting_detail/index.dart';
import '../../pages/apps/meeting/meeting_form/index.dart';
import '../../pages/apps/meeting/meeting_room_picker/index.dart';
import '../../pages/apps/mindMap/index.dart';
import '../../pages/apps/process/office_center/index.dart';
import '../../pages/apps/process/office_center/widgets/QuickProcessing/index.dart';
import '../../pages/apps/process/process_picker/index.dart';
import '../../pages/apps/yunpan/Image_slider_viewer/index.dart';
import '../../pages/apps/yunpan/cloud_disk_v3/index.dart';
import '../../pages/apps/yunpan/enterprise_file_list/index.dart';
import '../../pages/apps/yunpan/enterprise_zone/index.dart';
import '../../pages/apps/yunpan/file_type_list/index.dart';
import '../../pages/apps/yunpan/folder_picker/index.dart';
import '../../pages/apps/yunpan/index.dart';
import '../../pages/apps/yunpan/share_list/index.dart';
import '../../pages/apps/yunpan/upload_history_list/index.dart';
import '../../pages/common/create_form/index.dart';
import '../../pages/common/handwriting/index.dart';
import '../../pages/common/inner_webview/index.dart';
import '../../pages/common/input_form/index.dart';
import '../../pages/common/preview_image/index.dart';
import '../../pages/404/page_not_fount.dart';
import '../../pages/bind/index.dart';
import '../../pages/common/scan/index.dart';
import '../../pages/common/video_player/index.dart';
import '../../pages/demo_login/index.dart';
import '../../pages/home/apps/index.dart';
import '../../pages/home/contact/contact_picker/index.dart';
import '../../pages/home/contact/index.dart';
import '../../pages/home/contact/org_person_list/index.dart';
import '../../pages/home/contact/person/index.dart';
import '../../pages/home/contact/person_search/index.dart';
import '../../pages/home/default_index/index.dart';
import '../../pages/home/default_index/scan_login/index.dart';
import '../../pages/home/im/group_members/index.dart';
import '../../pages/home/im/im_chat/index.dart';
import '../../pages/home/im/index.dart';
import '../../pages/home/im/instant_chat/index.dart';
import '../../pages/home/im/speech_assistant_chat/index.dart';
import '../../pages/home/index.dart';
import '../../pages/home/search/index.dart';
import '../../pages/home/settings/about/index.dart';
import '../../pages/home/settings/account_safe/bio_auth/index.dart';
import '../../pages/home/settings/account_safe/empower/empower_create/index.dart';
import '../../pages/home/settings/account_safe/empower/index.dart';
import '../../pages/home/settings/account_safe/index.dart';
import '../../pages/home/settings/account_safe/update_password/index.dart';
import '../../pages/home/settings/index.dart';
import '../../pages/home/settings/log_list/index.dart';
import '../../pages/home/settings/my_profile/edit_avatar/index.dart';
import '../../pages/home/settings/my_profile/index.dart';
import '../../pages/home/settings/settings_common/index.dart';
import '../../pages/home/settings/skin/index.dart';
import '../../pages/login/index.dart';
import '../../pages/login/server_info/index.dart';
import '../../pages/splash/index.dart';
import 'routes.dart';

class O2OAPages {
  static final unknownRoute = GetPage(
    name: O2OARoutes.notFount,
    page: () => const NotFoundPage(),
  );
  static final pages = [
    GetPage(
        name: O2OARoutes.splash,
        page: () => const SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: O2OARoutes.bind,
        page: () => const BindPage(),
        binding: BindBinding()),
    GetPage(
        name: O2OARoutes.login,
        page: () => const LoginPage(),
        binding: LoginBinding()),   
    GetPage(
        name: O2OARoutes.loginServerInfo,
        page: () => const ServerInfoPage(),
        binding: ServerInfoBinding()),  
    GetPage(
        name: O2OARoutes.demoLogin,
        page: () => const DemoLoginPage(),
        binding: DemoLoginBinding()),
    GetPage(
        name: O2OARoutes.scan,
        page: () => const ScanPage(),
        binding: ScanBinding()),
    GetPage(
      name: O2OARoutes.home,
      page: () => const HomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
        name: O2OARoutes.homeSearch,
        page: () => const SearchPage(),
        binding: SearchBinding()),
    GetPage(
        name: O2OARoutes.homeApps,
        page: () => const AppsPage(),
        binding: AppsBinding()),
    GetPage(
        name: O2OARoutes.homeContact,
        page: () => const ContactPage(),
        binding: ContactBinding()),
    GetPage(
        name: O2OARoutes.homeContactList,
        page: () => const OrgPersonListPage(),
        binding: OrgPersonListBinding()),
    GetPage(
        name: O2OARoutes.homeContactPerson,
        page: () => const PersonPage(),
        binding: PersonBinding()),
    GetPage(
        name: O2OARoutes.homeContactSearch,
        page: () => const PersonSearchPage(),
        binding: PersonSearchBinding()),
    GetPage(
        name: O2OARoutes.homeContactPicker,
        page: () => const ContactPickerPage(),
        binding: ContactPickerBinding()),
    GetPage(
        name: O2OARoutes.homeIndex,
        page: () => const DefaultIndexPage(),
        binding: DefaultIndexBinding()),
    GetPage(
        name: O2OARoutes.homeIndexLogin2PC,
        page: () => const ScanLoginPage(),
        binding: ScanLoginBinding()),
    GetPage(
        name: O2OARoutes.homeIm,
        page: () => const ImPage(),
        binding: ImBinding()),
    GetPage(
        name: O2OARoutes.homeImChat,
        page: () => const ImChatPage(),
        binding: ImChatBinding()),
    GetPage(
        name: O2OARoutes.homeImChatSpeechAssistant,
        page: () => const SpeechAssistantChatPage(),
        binding: SpeechAssistantChatBinding()),
    GetPage(
        name: O2OARoutes.homeImChatInstant,
        page: () => const InstantChatPage(),
        binding: InstantChatBinding()),
    GetPage(
        name: O2OARoutes.homeImChatGroupMembers,
        page: () => const GroupMembersPage(),
        binding: GroupMembersBinding()),
    GetPage(
        name: O2OARoutes.homeSettings,
        page: () => const SettingsPage(),
        binding: SettingsBinding()),
    GetPage(
        name: O2OARoutes.homeSettingsMyProfile,
        page: () => const MyProfilePage(),
        binding: MyProfileBinding()),
    GetPage(name: O2OARoutes.homeSettingsSkin, page: () => const SkinPage()),
    GetPage(
        name: O2OARoutes.homeSettingsEditAvatar,
        page: () => const EditAvatarPage()),
    GetPage(
        name: O2OARoutes.homeSettingsAccountSafe,
        page: () => const AccountSafePage(),
        binding: AccountSafeBinding()),
    GetPage(
        name: O2OARoutes.homeSettingsAccountSafeEmpower,
        page: () => const EmpowerPage(),
        binding: EmpowerBinding()),
    GetPage(
        name: O2OARoutes.homeSettingsAccountSafeEmpowerCreate,
        page: () => const EmpowerCreatePage(),
        binding: EmpowerCreateBinding()), 
    GetPage(
        name: O2OARoutes.homeSettingsAccountSafeBiometricAuth,
        page: () => const BioAuthPage(),
        binding: BioAuthBinding()),
    GetPage(
        name: O2OARoutes.homeSettingsLogs,
        page: () => const LogListPage(),
        binding: LogListBinding()),
    GetPage(
        name: O2OARoutes.homeSettingsAbout,
        page: () => const AboutPage(),
        binding: AboutBinding()),
    GetPage(
        name: O2OARoutes.homeSettingsAccountSafeUpdatePwd,
        page: () => const UpdatePasswordPage()),   
    GetPage(
        name: O2OARoutes.homeSettingsCommon,
        page: () => const SettingsCommonPage(),
        binding: SettingsCommonBinding()),
    // GetPage(
    //     name: O2OARoutes.processWebview,
    //     page: () => const ProcessWebviewPage(),
    //     // binding: ProcessWebviewBinding()
    // ),

    GetPage(
        name: O2OARoutes.previewImage,
        page: () => const PreviewImagePage(),
        binding: PreviewImageBinding()), 
  
    GetPage(
        name: O2OARoutes.commonVideoPlayer,
        page: () => const VideoPlayerPage(),
        binding: VideoPlayerBinding()), 
    GetPage(
        name: O2OARoutes.commonHandwriting,
        page: () => const HandwritingPage(),
        binding: HandwritingBinding()),
    // GetPage(
    //     name: O2OARoutes.portal,
    //     page: () => const PortalPage(),
    //     binding: PortalBinding()),

    // app
    GetPage(
        name: O2OARoutes.appProcessPicker,
        page: () => const ProcessPickerPage(),
        binding: ProcessPickerBinding()),
    GetPage(
        name: O2OARoutes.appProcessOfficeCenter,
        page: () => const OfficeCenterPage(),
        binding: OfficeCenterBinding()), 
    GetPage(
        name: O2OARoutes.appProcessQuickprocessing,
        page: () => const QuickprocessingPage(),
        binding: QuickprocessingBinding()),
    GetPage(
        name: O2OARoutes.appMeeting,
        page: () => const MeetingPage(),
        binding: MeetingBinding()),
    GetPage(
        name: O2OARoutes.appMeetingDetail,
        page: () => const MeetingDetailPage(),
        binding: MeetingDetailBinding()),
    GetPage(
        name: O2OARoutes.appMeetingForm,
        page: () => const MeetingFormPage(),
        binding: MeetingFormBinding()),
    GetPage(
        name: O2OARoutes.appMeetingRoomPicker,
        page: () => const MeetingRoomPickerPage(),
        binding: MeetingRoomPickerBinding()), 
    GetPage(
        name: O2OARoutes.appMeetingListAccept,
        page: () => const MeetingAcceptListPage(),
        binding: MeetingAcceptListBinding()),
    GetPage(
        name: O2OARoutes.appYunpan,
        page: () => const YunpanPage(),
        binding: YunpanBinding()),
    GetPage(
        name: O2OARoutes.appCloudDiskV3,
        page: () => const CloudDiskV3Page(),
        binding: CloudDiskV3Binding()),
    GetPage(
        name: O2OARoutes.appCloudDiskFolderPicker,
        page: () => const FolderPickerPage(),
        binding: FolderPickerBinding()),
    GetPage(
        name: O2OARoutes.appCloudDiskListFileByType,
        page: () => const FileTypeListPage(),
        binding: FileTypeListBinding()),
    GetPage(
        name: O2OARoutes.appCloudDiskShareList,
        page: () => const ShareListPage(),
        binding: ShareListBinding()), 
    GetPage(
        name: O2OARoutes.appCloudDiskV3EnterpriseZone,
        page: () => const EnterpriseZonePage(),
        binding: EnterpriseZoneBinding()), 
    GetPage(
        name: O2OARoutes.appCloudDiskV3EnterpriseFileList,
        page: () => const EnterpriseFileListPage(),
        binding: EnterpriseFileListBinding()), 
    GetPage(
        name: O2OARoutes.appCloudDiskUploadList,
        page: () => const UploadHistoryListPage(),
        binding: UploadHistoryListBinding()), 
    GetPage(
        name: O2OARoutes.appCloudDiskImageSlideViewer,
        page: () => const ImageSliderViewerPage(),
        binding: ImageSliderViewerBinding()),
    GetPage(
        name: O2OARoutes.appBBS,
        page: () => const BbsPage(),
        binding: BbsBinding()),
    GetPage(
        name: O2OARoutes.bbsSubject,
        page: () => const BbsSubjectPage(),
        binding: BbsSubjectBinding()),
    GetPage(
        name: O2OARoutes.appBBSSection,
        page: () => const BbsSectionPage(),
        binding: BbsSectionBinding()),
    GetPage(
        name: O2OARoutes.appBBSSubjectReply,
        page: () => const SubjectReplyPage(),
        binding: SubjectReplyBinding()), 
    GetPage(
        name: O2OARoutes.appBBSSubjectPublish,
        page: () => const PublishSubjectPage(),
        binding: PublishSubjectBinding()),
    GetPage(
        name: O2OARoutes.appCms,
        page: () => const CmsPage(),
        binding: CmsBinding()),
    GetPage(
        name: O2OARoutes.appCmsApplication,
        page: () => const CmsAppPage(),
        binding: CmsAppBinding()),
    // GetPage(
    //     name: O2OARoutes.cmsDocument,
    //     page: () => const CmsDocumentPage(),
    //     binding: CmsDocumentBinding()),
    GetPage(
        name: O2OARoutes.appCmsCategoryPicker,
        page: () => const CmsCategoryPickerPage(),
        binding: CmsCategoryPickerBinding()),
    // 输入框页面
    GetPage(
        name: O2OARoutes.commonInput,
        page: () => const InputFormPage(),
        binding: InputFormBinding()),
    // 输入框页面
    GetPage(
        name: O2OARoutes.commonCreateForm,
        page: () => const CreateFormPage(),
        binding: CreateFormBinding()),
    // webview  网页查看器
    GetPage(
        name: O2OARoutes.commonInnerWebview,
        page: () => const InnerWebviewPage(),
        binding: InnerWebviewBinding()),

    // 考勤
    GetPage(
        name: O2OARoutes.appAttendance,
        page: () => const MainPage(),
        binding: MainBinding()),  
   GetPage(
        name: O2OARoutes.appAttendanceOld,
        page: () => const OldCheckInPage(),
        binding: OldCheckInBinding()),
    GetPage(
        name: O2OARoutes.appAttendanceExceptionDataPage,
        page: () => const ExceptionDataPage(),
        binding: ExceptionDataBinding()),
    // 日程
    GetPage(
        name: O2OARoutes.appCalendar,
        page: () => const CalendarPage(),
        binding: CalendarBinding()),
    GetPage(
        name: O2OARoutes.appCalendarEventCreate,
        page: () => const CreateCalendarEventPage(),
        binding: CreateCalendarEventBinding()),
    GetPage(
        name: O2OARoutes.appCalendarList,
        page: () => const CalendarListPage(),
        binding: CalendarListBinding()),
    GetPage(
        name: O2OARoutes.appCalendarCreate,
        page: () => const CreateCalendarPage(),
        binding: CreateCalendarBinding()),
    GetPage(
      name: O2OARoutes.appMindMap,
      page: () => const MindMapHomePage(),
    ),
    GetPage(
      name: O2OARoutes.appMindMapView,
      page: () => const MindMapView(),
    ),
    // GetPage(
    //     name: O2OARoutes.baiduMap,
    //     page: () => const BaiduMapPage(),
    //     binding: BaiduMapBinding()),
  ];
}
