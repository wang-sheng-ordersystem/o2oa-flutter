import 'package:get/get.dart';

import 'controller.dart';

class InnerWebviewBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<InnerWebviewController>(() => InnerWebviewController());
  }
}
