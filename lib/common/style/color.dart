import 'package:flutter/material.dart';

class AppColor {


  /// 黑色半透明
  static const Color blackTransparentBackground = Color.fromARGB(123, 0, 0, 0);
  /// 白色是半透明
  static const Color whiteTransparentBackground = Color.fromARGB(175,255,255, 255);// afffffff
  /// 主要文本颜色
  static const Color primaryText = Color(0xFF333333);
  static const Color primaryTextDark = Color.fromARGB(255, 255, 255, 255);

  /// 次要文本颜色
  static const Color secondaryText = Color(0xFF666666);
  static const Color secondaryTextDark = Color.fromARGB(255, 225, 225, 225);
  
  /// hint 颜色
  static const Color hintText = Color(0xFF999999);

  /// 高亮颜色
  static const Color primaryColor = Color(0xFFFB4747);
  static const Color primarySelectColor = Color(0x6BFB4747);
  /// 前景色 一般的字体颜色
  static const Color accentColor = Color(0xFF333333);
  static const Color accentColorDark = Color.fromARGB(255, 222, 220, 220);

  /// 次要颜色
  static const Color secondaryColor = Color(0xFFFBA6A6);

  /// 警告颜色
  static const Color warnColor = Color(0xFFFFB822);

  /// 边框颜色
  static const Color borderColor = Color(0xFFDEE3FF);

  static const Color pinkColor = Color(0xFFF77866);

  static const Color yellowColor = Color(0xFFFFB822);


  static const Color o2Blue = Color(0xFF008BE6); // 标签用的蓝色

  /// o2 皮肤颜色
  static const Color o2SkinBlue = Color(0xFF4A90E2);
  static const Color o2SkinRed = Color(0xFFD94141);
  static const Color o2SkinOrange = Color(0xFFED8824);
  static const Color o2SkinGreen = Color(0xFF60BF78);
  static const Color o2SkinCyan = Color(0xFF30BFBF);
  static const Color o2SkinPurple = Color(0xFFBF3995);
  static const Color o2SkinDarkGreen = Color(0xFF4D6240);
  static const Color o2SkinTan = Color(0xFF995E2E);
  static const Color o2SkinNavy = Color(0xFF323159);
  static const Color o2SkinGray = Color(0xFF666666);

  static MaterialColor o2RedSwatch =  MaterialColor(accentColor.value,
    const <int, Color>{
      50:  accentColor,
      100: accentColor,
      200: accentColor,
      300: accentColor,
      400: accentColor,
      500: accentColor,
      600: accentColor,
      700: accentColor,
      800: accentColor,
      900: accentColor,
    },
  );

  /// 脑图使用
  static const Color o2Dark = Color.fromARGB(255, 58, 60, 65);

  static const Color dividerColor = Colors.black26;


  static const Color meetingWaitColor = Color.fromARGB(255, 73, 144, 226); // 已预约
  static const Color meetingProcessingColor = Color.fromARGB(255, 102, 204, 127); // 会议中
  static const Color meetingAcceptColor = Color.fromARGB(255, 246, 166, 35); // 会议邀请
  static const Color meetingCompletedColor = Color.fromARGB(255, 204, 204, 204); // 已结束

  static const Color meetingRoomIdleColor = Color.fromARGB(255, 102, 204, 127); // 空闲
  static const Color meetingRoomBusyColor = Color(0xFFFB4747); // 会议中
  static const Color meetingRoomDisableColor = Color.fromARGB(255, 222, 220, 220); // 禁用




}
