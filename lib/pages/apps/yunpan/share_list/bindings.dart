import 'package:get/get.dart';

import 'controller.dart';

class ShareListBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ShareListController>(() => ShareListController());
  }
}
