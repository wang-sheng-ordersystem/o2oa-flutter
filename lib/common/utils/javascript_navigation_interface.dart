
///
/// jsapi 处理页面功能的接口类
abstract class JsNavigationInterface {

  /// 设置页面标题
  void setNavigationTitle(String title);
  /// 关闭当前页面
  void closeWindow();
  /// 网页返回
  void goBack();

  
}


