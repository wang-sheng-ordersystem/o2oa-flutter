import 'package:get/get.dart';

import 'controller.dart';

class OfficeCenterBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OfficeCenterController>(() => OfficeCenterController());
  }
}
