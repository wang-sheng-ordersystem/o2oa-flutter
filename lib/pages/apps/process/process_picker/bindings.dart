import 'package:get/get.dart';

import 'controller.dart';

class ProcessPickerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ProcessPickerController>(() => ProcessPickerController());
  }
}
