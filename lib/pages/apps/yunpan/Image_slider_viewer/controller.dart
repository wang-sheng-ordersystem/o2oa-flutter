import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../common/models/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class ImageSliderViewerController extends GetxController {
  ImageSliderViewerController();

  final state = ImageSliderViewerState();
  final pageController = PageController();
 
  /// 在 onInit() 之后调用 1 帧。这是进入的理想场所
  @override
  void onReady() {
    O2UI.hideStatusBar();
    final args = Get.arguments;
    if (args!= null) {
      if (args['imageList'] is List<FileInfo>) {
        state.imageFileList
        ..clear()
        ..addAll(args['imageList'] as List<FileInfo>);
      }
      int currentPage = 0;
      if (args['currentFileId'] is String) {
        String currentFileId = args['currentFileId'] as String;
        final index = state.imageFileList.indexWhere((element) => element.id == currentFileId);
        if (index >=0 ) {
          currentPage = index;
          pageController.jumpToPage(index);
        }
      }
      pageChanged(currentPage);
    }
    super.onReady();
  }

  @override
  void onClose() {
    O2UI.showStatusBar();
    super.onClose();
  }
 
  void pageChanged(int page) {
    state.title = '${page+1}/${state.imageFileList.length}';
  }
}
