import 'package:get/get.dart';

import 'controller.dart';

class CmsDocumentBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CmsDocumentController>(() => CmsDocumentController());
  }
}
