

import 'package:flutter/material.dart';

import '../../services/index.dart';

enum LanguageList {
  hans, // 简体
  en, // 英语
  hant, // 繁体
  es // 西班牙
}

extension LanguageListExt on LanguageList {

  static LanguageList getCurrentLanguage() {
    String key = SharedPreferenceService.to.getString(SharedPreferenceService.languageSaveKey, defaultValue: LanguageList.hans.getKey());
    for (var element in LanguageList.values) {
      if (element.getKey() == key) {
        return element;
      }
    }
    return LanguageList.hans;
  }

  /// pc 上设置的语言环境
  static LanguageList getFromPcKey(String keyfromPc) {
    switch(keyfromPc) {
      case 'zh-CN':
        return LanguageList.hans;
      case 'en':
        return LanguageList.en;
      case 'es':
        return LanguageList.es;
      default:
        return getCurrentLanguage();
    }
  }

  String getDisplayName() {
    switch(this) {
      case LanguageList.hans:
      return '简体';
      case LanguageList.en:
      return 'English';
      case LanguageList.hant:
      return '繁體';
      case LanguageList.es:
      return 'Español';
    }
  }

  String getKey() {
    switch(this) {
      case LanguageList.hans:
      return 'hans';
      case LanguageList.en:
      return 'en';
      case LanguageList.hant:
      return 'hant';
      case LanguageList.es:
      return 'es';
    }
  }

  Locale getLocale() {
    switch(this) {
      case LanguageList.hans:
      return const Locale('zh', 'CN');
      case LanguageList.en:
      return const Locale('en', 'US');
      case LanguageList.hant:
      return const Locale('zh', 'HK');
      case LanguageList.es:
      return const Locale('es', 'ES');
    }
  }
  
}