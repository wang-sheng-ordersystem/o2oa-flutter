import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/api/index.dart';
import '../../../common/models/index.dart';
import '../../../common/style/index.dart';
import '../../../common/utils/index.dart';
import '../../../common/widgets/index.dart';
import 'index.dart';

class AppsPage extends GetView<AppsController> {
  const AppsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AppsController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
              title: Obx(() => Text(controller.state.title)),
              actions: [
                TextButton(
                    onPressed: controller.clickEditMyAppTopBtn,
                    child: Obx(() => Text(
                          controller.state.isEdit ? 'save'.tr : 'edit'.tr,
                          style: AppTheme.whitePrimaryTextStyle,
                        )))
              ]),
          body: SafeArea(
            child: Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: ListView(
                  children: [
                    ProgramCenterService.to.appPageImageView(),
                    myAppBoxView(context),
                    const SizedBox(height: 10),
                    Container(child: appListView(context))
                  ],
                )),
          ),
        );
      },
    );
  }

  Widget appListView(BuildContext context) {
    return Column(
      children: [
        Obx(() => appBoxView(
            context, 'app_type_native'.tr, controller.state.nativeList)),
        const SizedBox(height: 10),
        Obx(() => appBoxView(
            context, 'app_type_portal'.tr, controller.state.portalList))
      ],
    );
  }

  ///
  /// 应用列表方块
  ///
  Widget appBoxView(
      BuildContext context, String title, List<AppFrontData?> appList) {
    return Container(
      color: Theme.of(context).colorScheme.background,
      width: double.infinity,
      child: Padding(
          padding:
              const EdgeInsets.only(top: 10, bottom: 5, left: 15, right: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(title, style: AppTheme.textBodyLarge(context)),
              const SizedBox(height: 5),
              Padding(
                  padding: const EdgeInsets.all(10),
                  child: GridView.count(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      crossAxisCount: 4,
                      mainAxisSpacing:
                          5.0, //设置上下之间的间距(Axis.horizontal设置左右之间的间距)
                      crossAxisSpacing:
                          0.0, //设置左右之间的间距(Axis.horizontal设置上下之间的间距)
                      childAspectRatio: 1, //设置宽高比
                      children: appList.map((element) {
                        if (element == null) {
                          return Container();
                        }
                        return appItemView(context, element, false);
                      }).toList()))
            ],
          )),
    );
  }

  ///
  /// 我的应用列表方块
  ///
  Widget myAppBoxView(BuildContext context) {
    return Obx(() => Container(
          color: Theme.of(context).colorScheme.background,
          width: double.infinity,
          child: Padding(
              padding: const EdgeInsets.only(
                  top: 10, bottom: 5, left: 15, right: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('app_type_my'.tr,
                      style: AppTheme.textBodyLarge(context)),
                  const SizedBox(height: 5),
                  Padding(
                      padding: const EdgeInsets.all(10),
                      child: controller.state.myAppList.isEmpty
                          ? Container()
                          : GridView.count(
                              shrinkWrap: true,
                              crossAxisCount: 4,
                              mainAxisSpacing:
                                  0.0, //设置上下之间的间距(Axis.horizontal设置左右之间的间距)
                              crossAxisSpacing:
                                  5.0, //设置左右之间的间距(Axis.horizontal设置上下之间的间距)
                              childAspectRatio: 1, //设置宽高比
                              children:
                                  controller.state.myAppList.map((element) {
                                if (element == null) {
                                  return Container();
                                }
                                return appItemView(context, element, true);
                              }).toList()))
                ],
              )),
        ));
  }

  ///
  /// 单个应用View
  /// @param isMyApp 是否主页应用上显示，编辑的时候右上角小图标不一样
  ///
  Widget appItemView(BuildContext context, AppFrontData element, bool isMyApp) {
    return Obx(() => controller.state.isEdit
        ? GestureDetector(
            onTap: () => isMyApp
                ? controller.removeApp2MyList(element)
                : (controller.isInMyApp(element)
                    ? controller.removeApp2MyList(element)
                    : controller.addApp2MyList(element)),
            child: Stack(
              children: [
                _appItemView(context, element),
                Positioned(
                    right: 0,
                    top: 0,
                    child: AssetsImageView(
                      isMyApp
                          ? 'icon_app_del.png'
                          : (controller.isInMyApp(element)
                              ? 'icon_app_del.png'
                              : 'icon_app_add.png'),
                      width: 12,
                      height: 12,
                    )),
              ],
            ))
        : GestureDetector(
            onTap: () => controller.openApp(element),
            child: _appItemView(context, element),
          ));
  }

  /// 应用 Item
  Widget _appItemView(BuildContext context, AppFrontData element) {
    var showName = element.displayName ?? '';
    if (showName.trim().isEmpty) {
      showName = element.name ?? '';
    }
    return Column(children: [
      // Icon图标，原生应用是本地图标，门户应用是url
      _appItemImageWithBadges(element),
      Expanded(
          flex: 1,
          child: Center(
              child: Text(showName,
                  softWrap: true,
                  style: AppTheme.textBodyMedium(context))))
    ]);
  }

  /// 应用图标 加上角标
  /// 编辑的时候不显示角标
  Widget _appItemImageWithBadges(AppFrontData element) {
    return Obx(() => controller.state.isEdit
        ? _appItemImage(element)
        : O2UI.badgeView((element.showNumber ?? 0), _appItemImage(element)));
  }

  /// 应用图标
  Widget _appItemImage(AppFrontData element) {
    Map<String, String> headers = {};
    headers[O2ApiManager.instance.tokenName] =
        O2ApiManager.instance.o2User?.token ?? '';
    return (element.type == O2AppTypeEnum.native)
        ? AssetsImageView(
            element.nativeEnum?.assetPath ?? 'app_task.png',
            width: 36.w,
            height: 36.w,
          )
        : Image(
            image: NetworkImage(element.portalIconUrl!, headers: headers),
            alignment: Alignment.center,
            width: 36.w,
            height: 36.w,
            fit: BoxFit.fill);
  }
}
