import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../common/style/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class CloudDiskV3Page extends GetView<CloudDiskV3Controller> {
  const CloudDiskV3Page({Key? key}) : super(key: key);

  // 主视图
  Widget _buildView(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            InkWell(
                onTap: () => controller.gotoPerson(),
                child: zoneView(context, 0)),
            const SizedBox(height: 10),
            InkWell(
                onTap: () => controller.gotoEnterprise(),
                child: zoneView(context, 1)),
          ],
        ));
  }

  Widget zoneView(BuildContext context, int type) {
    return Container(
      height: 75.h,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(12), // 调整圆角半径以改变椭圆形状
        color: type == 0
            ? "#E4ECF7".hexToColor()
            : '#E4F2E7'.hexToColor(), // 设置背景颜色
      ),
      padding: const EdgeInsets.all(10),
      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        AssetsImageView(
            type == 0 ? 'icon_cloud_v3_person.png' : 'icon_cloud_v3_qy.png',
            width: 50.h,
            height: 50.h),
        const SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
                type == 0
                    ? 'cloud_disk_v3_my_file'.tr
                    : 'cloud_disk_v3_enterprise_file'.tr,
                style: TextStyle(color: Colors.black87, fontSize: 16.sp)),
            const Spacer(),
            Text(
                type == 0
                    ? 'cloud_disk_v3_personal_zone'.tr
                    : 'cloud_disk_v3_inner_zone'.tr,
                style: TextStyle(color: AppColor.hintText, fontSize: 12.sp))
          ],
        )
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CloudDiskV3Controller>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Obx(()=>Text(controller.state.title))),
          body: SafeArea(
            child: _buildView(context),
          ),
        );
      },
    );
  }
}
