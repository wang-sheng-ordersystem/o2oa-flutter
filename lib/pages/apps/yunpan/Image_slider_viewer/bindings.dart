import 'package:get/get.dart';

import 'controller.dart';

class ImageSliderViewerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ImageSliderViewerController>(() => ImageSliderViewerController());
  }
}
