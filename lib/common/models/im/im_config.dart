class IMConfigData {
  IMConfigData({this.enableClearMsg, this.enableRevokeMsg});

  bool? enableClearMsg;
  bool? enableRevokeMsg;

  factory IMConfigData.fromJson(Map<String, dynamic> json) => IMConfigData(
      enableClearMsg: json['enableClearMsg'],
      enableRevokeMsg: json['enableRevokeMsg']);
  Map<String, dynamic> toJson() =>
      {"enableClearMsg": enableClearMsg, "enableRevokeMsg": enableRevokeMsg};
}
