import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';

import '../../../../../../common/models/index.dart';
import '../../../../../../common/style/index.dart';
import '../../../../../../common/widgets/index.dart';
import 'index.dart';

class ImageLoaderPage extends GetView<ImageLoaderController> {
  const ImageLoaderPage({Key? key, required this.fileInfo, required this.tag})
      : super(key: key);
  final FileInfo fileInfo;
  @override
  final String? tag;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ImageLoaderController>(
      init: ImageLoaderController(fileInfo),
      tag: tag,
      builder: (_) {
        return Obx(() => Container(
            constraints: BoxConstraints.expand(
              height: MediaQuery.of(context).size.height,
            ),
            child: controller.fileLocalPath.value.isEmpty
                ? const Center(child: CircularProgressIndicator())
                : Stack(children: <Widget>[
                    Positioned(
                        top: 0,
                        left: 0,
                        bottom: 0,
                        right: 0,
                        child: PhotoView(
                          imageProvider:
                              FileImage(File(controller.fileLocalPath.value)),
                          initialScale: PhotoViewComputedScale.contained,
                          minScale: PhotoViewComputedScale.contained,
                          maxScale: 5.0,
                        )),
                    Positioned(
                      // 右下角 下载图片到相册的按钮
                      right: 10.w,
                      bottom: 10.w,
                      child: O2UI.blackTransparentCard(
                          TextButton(
                            onPressed: () => controller.saveToAlbum(),
                            child: Text(
                                'cloud_disk_save_image_album_btn_title'.tr,
                                style: AppTheme.whitePrimaryTextStyle),
                          ),
                          width: 96.w,
                          height: 36.w),
                    )
                  ])));
      },
    );
  }
}
