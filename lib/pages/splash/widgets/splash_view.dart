import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/api/index.dart';
import '../../../common/style/index.dart';
import '../../../common/widgets/assets_image_view.dart';
import '../index.dart';

/// splash view
class SplashView extends GetView<SplashController> {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const Positioned.fill(
          child: AssetsImageView(
            'launch_background.png',
            fit: BoxFit.cover,
          ),
        ),
        Positioned.fill(
          child:Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 100.h),
            Text(
              'appName'.tr,
              style: TextStyle(color: AppColor.primaryText, fontSize: 32.sp),
            ),
            SizedBox(height: 24.h),
            Obx(() =>  controller.state.showLaunchLogo ? ProgramCenterService.to.launchLogoImageView() : const SizedBox(height: 128))
            
          ],
        )),
        Positioned(
            right: 20,
            bottom: 64.h,
            child: Obx(() {
              return Text(
                '${controller.state.tips}',
                style: TextStyle(color: Theme.of(context).colorScheme.primary),
              );
            })),
        Positioned(
            bottom: 32.h,
            left: 0,
            right: 0,
            child: Obx(() => Center(
                    child: Text(
                  controller.state.copyright,
                  style:
                      TextStyle(color: AppColor.primaryText, fontSize: 14.sp),
                ))))
      ],
    );
  }
}
