import 'package:get/get.dart';

import 'controller.dart';

class BindBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<BindController>(() => BindController());
  }
}
