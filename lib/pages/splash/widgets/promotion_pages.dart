import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../common/style/index.dart';
import '../../../common/widgets/index.dart';
import '../controller.dart';

class PromotionPages extends GetView<SplashController> {
  const PromotionPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Stack(alignment: Alignment.center, children: [
          const Positioned.fill(
            child: AssetsImageView(
              'launch_background.png',
              fit: BoxFit.cover,
            ),
          ),
          Positioned.fill(
              child: controller.state.promotionPageList.isEmpty
                  ? Container(height: 0)
                  : CarouselSlider.builder(
                      itemCount: controller.state.promotionPageList.length,
                      itemBuilder: (context, index, realIndex) {
                        final url = controller.state.promotionPageList[index];
                        return Image(
                            image: NetworkImage(url),
                            alignment: Alignment.center,
                            height: double.infinity,
                            width: double.infinity,
                            fit: BoxFit.fill);
                      },
                      options: CarouselOptions(
                          height: double.infinity,
                          viewportFraction: 1,
                          enableInfiniteScroll: false,
                          onPageChanged: ((index, reason) {
                            controller.state.currentPromotionPage = index;
                          })))),
          Positioned(
            top: 24.h,
            right: 24.h,
            child: evevatedButton(
              'splash_promotion_skip'.tr,
              () => controller.closePromotionPage(),
            ),
          ),
          Positioned(
            bottom: 48.h,
            child: Visibility(
              visible: controller.state.currentPromotionPage !=
                  (controller.state.promotionPageList.length - 1),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: controller.state.promotionPageList
                    .asMap()
                    .entries
                    .map((entry) {
                  return Container(
                    width: 12.0,
                    height: 12.0,
                    margin: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 4.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: (Theme.of(context).brightness == Brightness.dark
                                ? Colors.white
                                : Colors.black)
                            .withOpacity(
                                controller.state.currentPromotionPage ==
                                        entry.key
                                    ? 0.9
                                    : 0.4)),
                  );
                }).toList(),
              ),
            ),
          ),
          Positioned(
            bottom: 48.h,
            child: Visibility(
                visible: controller.state.currentPromotionPage ==
                    (controller.state.promotionPageList.length - 1),
                child: evevatedButton(
                  'splash_promotion_enter'.tr,
                  () => controller.closePromotionPage(),
                )),
          )
        ]));
  }

  Widget evevatedButton(String title, VoidCallback onPressed) {
    return ElevatedButton(
        onPressed: onPressed,
        style: ButtonStyle(
          backgroundColor:
              MaterialStateProperty.all(AppColor.blackTransparentBackground),
          elevation: MaterialStateProperty.all(0),
          shadowColor:
              MaterialStateProperty.all(AppColor.blackTransparentBackground),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45.0),
            ),
          ), //圆角弧度
        ),
        child: Text(title,
            style: TextStyle(
                color: Colors.white,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold)));
  }
}
