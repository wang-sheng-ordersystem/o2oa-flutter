
///
///一些环境相关的参数
///
class EnvironmentConfig {
  
  // 读取命令行参数 --dart-define=APP_MODE=inner
  static const o2AppMode = String.fromEnvironment('APP_MODE'); 

  // 读取命令行参数 --dart-define=IS_DEMO=true
  static const o2AppIsDemo = bool.fromEnvironment('IS_DEMO');

  // 读取命令行参数 --dart-define=CHECK_PRIVACY=true
  static const checkPrivacy = bool.fromEnvironment('CHECK_PRIVACY');

  ///是否是直连 模式 
  static bool isDirectConnectMode() {
    return o2AppMode == 'inner';
  }

  /// 是否演示版本 app
  static bool isDemoApp() {
    return o2AppIsDemo;
  }

  /// 是否要显示隐私政策
  static bool needCheckPrivacyAgree() {
    return !isDirectConnectMode() || checkPrivacy;
  }

  /// 是否正式版本
  static bool isRelease() {
    return const bool.fromEnvironment("dart.vm.product");
  }
}