import 'package:get/get.dart';

class PortalState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  ///  门户是否是单独打开的一个页面
  final _isPage = true.obs;
  set isPage(bool value) => _isPage.value = value;
  bool get isPage => _isPage.value;

  /// 是否显示 AppBar
  final _hiddenAppBar = false.obs;
  set hiddenAppBar(bool value) => _hiddenAppBar.value = value;
  bool get hiddenAppBar => _hiddenAppBar.value;

  // 访问地址
  final _url = "".obs;
  set url(value) => _url.value = value;
  String get url => _url.value;

}
