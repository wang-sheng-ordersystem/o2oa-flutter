import 'dart:typed_data';

import 'package:get/get.dart';

import '../../../../common/models/index.dart';

class ImChatState {
  // title
  final _title = "".obs;
  set title(value) => _title.value = value;
  get title => _title.value;


  // 消息列表
  final RxList<IMMessage> msgList = <IMMessage>[].obs;

  // 是否正在播放语音
  final _playingVoiceId = "".obs;
  set playingVoiceId(String value) => _playingVoiceId.value = value;
  String get playingVoiceId => _playingVoiceId.value;
  

  /// 底部工具栏显示类型 0:不显示 1:语音 2:表情 3:更多工具
  final _toolBoxType = 0.obs;
  set toolBoxType(int type) => _toolBoxType.value = type;
  int get toolBoxType => _toolBoxType.value;

  /// 流程图标缓存
  final RxMap<String, Uint8List> processAppIconMap =  <String, Uint8List>{}.obs;

  /// 是否有更多数据
  final _hasMore = true.obs;
  set hasMore(bool value) => _hasMore.value = value;
  bool get hasMore => _hasMore.value;

  /// 顶部菜单
  final RxList<IMChatMenu> menuList = <IMChatMenu>[IMChatMenu('im_chat_action_menu_report'.tr, IMChatMenuType.report)].obs;

}
