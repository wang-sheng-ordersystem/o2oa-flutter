class O2OARoutes {
  static const String splash = '/';
  static const String notFount = '/404';
  static const String bind = '/bind'; // 绑定页面
  static const String home = '/home'; // 首页
  static const String homeApps = '/home/apps'; // 应用
  static const String homeContact = '/home/contact'; // 通讯录
  static const String homeContactPerson = '/home/contact/person'; // 通讯录
  static const String homeContactList = '/home/contact/list'; // 通讯录
  static const String homeContactSearch = '/home/contact/search'; // 通讯录
  static const String homeContactPicker = '/home/contact/picker'; // 通讯录选择器
  static const String homeIndex = '/home/index'; // 默认首页
  static const String homeIndexLogin2PC = '/home/index/logintopc'; // 默认首页
  static const String homeIm = '/home/im'; // 聊天
  static const String homeImChat = '/home/im/chat'; // 聊天
  static const String homeImChatGroupMembers =
      '/home/im/group/members'; // 群成员管理
  static const String homeImChatInstant = '/home/im/instant'; // 系统消息
  static const String homeImChatSpeechAssistant = '/home/im/speech/assistant'; // 语音助手
  static const String homeSettings = '/home/settings'; // 设置
  static const String homeSettingsSkin = '/home/settings/skin'; // 外观设置
  static const String homeSettingsAbout = '/home/settings/about'; // 关于
  static const String homeSettingsLogs = '/home/settings/logs'; // 日志
  static const String homeSettingsMyProfile = '/home/settings/profile'; // 个人信息
  static const String homeSettingsEditAvatar =
      '/home/settings/profile/avatar'; // 修改头像
  static const String homeSettingsAccountSafe =
      '/home/settings/accoutsafe'; // 账号安全
  static const String homeSettingsAccountSafeUpdatePwd =
      '/home/settings/accoutsafe/password'; // 修改密码
  // 外出授权
  static const String homeSettingsAccountSafeEmpower = '/home/settings/accoutsafe/empower/list';
  static const String homeSettingsAccountSafeEmpowerCreate = '/home/settings/accoutsafe/empower/create';
  // 生物识别认证
  static const String homeSettingsAccountSafeBiometricAuth = '/home/settings/accoutsafe/biometric/auth';
  // 通用
  static const String homeSettingsCommon = '/home/settings/common';

  static const String homeSearch = '/home/search'; // 搜索

  static const String commonInput = '/common/input'; // input输入框页面
  static const String commonCreateForm = '/common/form/create';
  static const String commonInnerWebview = '/common/inner/webview'; // 内部 webview
  static const String commonVideoPlayer = '/common/video/player'; //  视频播放
  static const String commonHandwriting = '/common/handwriting'; //  手写板

  static const String login = '/login'; // 登录页面
  static const String loginServerInfo = '/login/server/info'; // 服务器信息配置
  static const String demoLogin = '/login/demo'; // 演示版本登录

  static const String processWebview = '/process/webview'; // webview页面
  static const String previewFile = '/previewfile'; // 文件预览
  static const String previewImage = '/previewimage'; // 图片预览
  static const String portal = '/portal'; // 门户
  static const String scan = '/scan'; // 扫描二维码
  static const String baiduMap = '/map/baidu'; // 百度地图

  static const String appProcessOfficeCenter =
      '/apps/process/office/center'; // 办公中心
  static const String appTask = '$appProcessOfficeCenter?type=task'; // 待办
  static const String appTaskcompleted =
      '$appProcessOfficeCenter?type=taskcompleted'; // 已办
  static const String appRead = '$appProcessOfficeCenter?type=read'; // 待阅
  static const String appReadcompleted =
      '$appProcessOfficeCenter?type=readcompleted'; // 已阅
  static const String appProcessPicker = '/apps/process/picker'; // 流程选择
  static const String appProcessQuickprocessing = '/apps/process/quick/processing'; // 快速处理

  static const String appBBS = '/apps/bbs'; // 企业论坛
  static const String bbsSubject = '/apps/bbs/subject'; // 企业论坛
  static const String appMeeting = '/apps/meeting'; // 会议管理
  static const String appYunpan = '/apps/yunpan'; // 企业网盘
  static const String appCloudDiskV3 = '/apps/clouddiskv3'; // 企业网盘v3 需要安装应用
  static const String appCloudDiskFolderPicker = '/apps/clouddisk/folder/picker'; // 文件夹选择
  static const String appCloudDiskListFileByType = '/apps/clouddisk/file/list/type'; // 文件列表
  static const String appCloudDiskShareList = '/apps/clouddisk/share/list'; //  文件分享
  static const String appCloudDiskV3EnterpriseZone = '/apps/clouddisk/v3/zone'; //  文件分享
  static const String appCloudDiskV3EnterpriseFileList = '/apps/clouddisk/v3/list/file'; // 企业网盘
  static const String appCloudDiskUploadList = '/apps/clouddisk/upload/list'; // 上传页面
  static const String appCloudDiskImageSlideViewer = '/apps/clouddisk/image/list/view'; //  图片查看器

  static const String appCms = '/apps/cms'; // 信息中心
  static const String appCmsCategoryPicker = '/apps/cms/category/picker';
  static const String cmsDocument = '/apps/cms/document'; // 信息中心文档打开
  static const String appCmsApplication = '/apps/cms/application';
  static const String appAttendance = '/apps/attendance'; // 考勤
  static const String appAttendanceOld = '/apps/attendance/old'; // 考勤
  static const String appAttendanceExceptionDataPage = '/apps/attendance/exception/data'; // 考勤 异常数据
  static const String appCalendar = '/apps/calendar'; // 日程安排
  static const String appCalendarEventCreate = '/apps/calendar/event/create'; //  创建日程
  static const String appCalendarList = '/apps/calendar/list'; // 日历列表
  static const String appCalendarCreate = '/apps/calendar/create'; // 日历创建
  static const String appMindMap = '/apps/mindMap'; // 脑图
  static const String appMindMapView = '/apps/mindMap/view'; // 脑图

  static const String appMeetingDetail = '/apps/meeting/detail'; // 会议详情
  static const String appMeetingListAccept = '/apps/meeting/list/accept'; // 会议邀请
  static const String appMeetingForm = '/apps/meeting/form'; // 会议编辑页
  static const String appMeetingRoomPicker = '/apps/meeting/room/picker'; // 选择会议室

  static const String appBBSSection = '/apps/bbs/section'; // 企业论坛板块
  static const String appBBSSubjectReply = '/apps/bbs/subject/reply';
  static const String appBBSSubjectPublish= '/apps/bbs/subject/publish'; // 发帖

}
