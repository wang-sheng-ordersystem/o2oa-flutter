
class MeetingRoomBuildModel {

  String? id;
  String? name;
  String? address;
  List<MeetingRoom>? roomList;

  MeetingRoomBuildModel({ this.id, this.name, this.address });

  MeetingRoomBuildModel.fromJson(Map<String, dynamic> json){
      id = json['id'];
      name = json['name'];
      address = json['address'];
      roomList = json['roomList'] == null ? null : List<MeetingRoom>.from(json['roomList'].map((x)=> MeetingRoom.fromJson(x)));
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['address'] = address;
    data['roomList'] = roomList == null ? null : roomList!.map((e) => e.toJson()).toList();
    return data;
  }
}


class MeetingRoom {
    MeetingRoom({
        this.id,
        this.pinyin,
        this.pinyinInitial,
        this.name,
        this.building,
        this.floor,
        this.roomNumber,
        this.phoneNumber,
        this.device,
        this.capacity,
        this.available,
        this.idle,
        this.createTime,
        this.updateTime,
        this.buildName
    });

    String? id;
    String? pinyin;
    String? pinyinInitial;
    String? name;
    String? building;
    int? floor;
    String? roomNumber;
    String? phoneNumber;
    String? device;
    int? capacity;
    bool? available;
    bool? idle;
    String? createTime;
    String? updateTime;
    String? buildName; // 分组使用

    factory MeetingRoom.fromJson(Map<String, dynamic> json) => MeetingRoom(
        id: json["id"] ,
        pinyin: json["pinyin"],
        pinyinInitial: json["pinyinInitial"],
        name: json["name"],
        building: json["building"] ,
        floor: json["floor"],
        roomNumber: json["roomNumber"] ,
        phoneNumber: json["phoneNumber"] ,
        device: json["device"],
        capacity: json["capacity"],
        available: json["available"],
        idle: json["idle"],
        createTime: json["createTime"] ,
        updateTime: json["updateTime"],
    );

    Map<String, dynamic> toJson() => {
        "id": id ,
        "pinyin": pinyin ,
        "pinyinInitial": pinyinInitial,
        "name": name ,
        "building": building ,
        "floor": floor,
        "roomNumber": roomNumber ,
        "phoneNumber": phoneNumber,
        "device": device ,
        "capacity": capacity ,
        "available": available ,
        "idle": idle ,
        "createTime": createTime ,
        "updateTime": updateTime,
    };
}