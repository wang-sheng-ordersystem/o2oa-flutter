class MyCalendarList {
  List<CalendarInfo>? myCalendars;
  List<CalendarInfo>? unitCalendars;
  List<CalendarInfo>? followCalendars;

  MyCalendarList({this.myCalendars, this.unitCalendars, this.followCalendars});

  factory MyCalendarList.fromJson(Map<String, dynamic> json) => MyCalendarList(
      myCalendars: json["myCalendars"] == null
          ? []
          : List<CalendarInfo>.from(
              json["myCalendars"]!.map((e) => CalendarInfo.fromJson(e))),
      unitCalendars: json["unitCalendars"] == null
          ? []
          : List<CalendarInfo>.from(
              json["unitCalendars"]!.map((e) => CalendarInfo.fromJson(e))),
      followCalendars: json["followCalendars"] == null
          ? []
          : List<CalendarInfo>.from(
              json["followCalendars"]!.map((e) => CalendarInfo.fromJson(e))));
  Map<String, dynamic> toJson() => {
        "myCalendars": myCalendars,
        "unitCalendars": unitCalendars,
        "followCalendars": followCalendars,
      };
}

class CalendarInfo {
  bool? manageable;
  bool? publishable;
  String? id;
  String? name;
  String? type;
  String? target; // 所属组织
  String? color;
  String? description;
  String? source;
  String? createor;
  bool? isPublic;
  String? createTime;
  String? updateTime;
  List<String>? manageablePersonList;
  List<String>? publishableGroupList;
  List<String>? publishablePersonList;
  List<String>? publishableUnitList;
  List<String>? viewableGroupList;
  List<String>? viewablePersonList;
  List<String>? viewableUnitList;

  String? groupedTag;

  CalendarInfo({
    this.manageable,
    this.publishable,
    this.id,
    this.name,
    this.type,
    this.target,
    this.color,
    this.description,
    this.source,
    this.createor,
    this.isPublic,
    this.createTime,
    this.updateTime,
    this.manageablePersonList,
    this.publishableGroupList,
    this.publishablePersonList,
    this.publishableUnitList,
    this.viewableGroupList,
    this.viewablePersonList,
    this.viewableUnitList,

    this.groupedTag,
  });

  factory CalendarInfo.fromJson(Map<String, dynamic> json) => CalendarInfo(
        manageable: json["manageable"],
        publishable: json["publishable"],
        id: json["id"],
        name: json["name"],
        type: json["type"],
        target: json["target"],
        color: json["color"],
        description: json["description"],
        source: json["source"],
        createor: json["createor"],
        isPublic: json["isPublic"],
        createTime: json["createTime"],
        updateTime: json["updateTime"],
        manageablePersonList: json["manageablePersonList"] == null ? [] : List<String>.from(json["manageablePersonList"]!.map((x) => x)),
        publishableGroupList: json["publishableGroupList"] == null ? [] : List<String>.from(json["publishableGroupList"]!.map((x) => x)),
        publishablePersonList: json["publishablePersonList"] == null ? [] : List<String>.from(json["publishablePersonList"]!.map((x) => x)),
        publishableUnitList: json["publishableUnitList"] == null ? [] : List<String>.from(json["publishableUnitList"]!.map((x) => x)),
        viewableGroupList: json["viewableGroupList"] == null ? [] : List<String>.from(json["viewableGroupList"]!.map((x) => x)),
        viewablePersonList: json["viewablePersonList"] == null ? [] : List<String>.from(json["viewablePersonList"]!.map((x) => x)),
        viewableUnitList: json["viewableUnitList"] == null ? [] : List<String>.from(json["viewableUnitList"]!.map((x) => x)),


        groupedTag: json["groupedTag"],
      );

  Map<String, dynamic> toJson() => {
        "manageable": manageable,
        "publishable": publishable,
        "id": id,
        "name": name,
        "type": type,
        "target": target,
        "color": color,
        "description": description,
        "source": source,
        "createor": createor,
        "isPublic": isPublic,
        "createTime": createTime,
        "updateTime": updateTime,
        "manageablePersonList": manageablePersonList == null ? [] : List<String>.from(manageablePersonList!.map((x) => x)),
        "publishableGroupList": publishableGroupList == null ? [] : List<String>.from(publishableGroupList!.map((x) => x)),
        "publishablePersonList": publishablePersonList == null ? [] : List<String>.from(publishablePersonList!.map((x) => x)),
        "publishableUnitList": publishableUnitList == null ? [] : List<String>.from(publishableUnitList!.map((x) => x)),
        "viewableGroupList": viewableGroupList == null ? [] : List<String>.from(viewableGroupList!.map((x) => x)),
        "viewablePersonList": viewablePersonList == null ? [] : List<String>.from(viewablePersonList!.map((x) => x)),
        "viewableUnitList": viewableUnitList == null ? [] : List<String>.from(viewableUnitList!.map((x) => x)),

        "groupedTag": groupedTag,
      };
}
