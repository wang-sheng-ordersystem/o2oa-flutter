import Cocoa
import FlutterMacOS
import UserNotifications

@NSApplicationMain
class AppDelegate: FlutterAppDelegate {
  override func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
      NSApp.setActivationPolicy(.accessory) //关闭窗口后台运行
      return false
  }

    override func applicationDidBecomeActive(_ notification: Notification) {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        //NSLog("清空所有的通知！！！！")
    }
}
