import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:o2oa_all_platform/common/extension/string_extension.dart';

import '../../../../common/models/index.dart';
import '../../../../common/routers/index.dart';
import '../../../../common/style/index.dart';
import '../../../../common/widgets/index.dart';
import 'index.dart';

class MeetingAcceptListPage extends GetView<MeetingAcceptListController> {
  const MeetingAcceptListPage({Key? key}) : super(key: key);

  static Future<void> open() async {
    await Get.toNamed(O2OARoutes.appMeetingListAccept);
  }
 
  @override
  Widget build(BuildContext context) {
    return GetBuilder<MeetingAcceptListController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text("meeting_accept_list".tr)),
          body: SafeArea(
            child: Obx(() => 
              controller.state.meetingList.length > 0 ?
              _meetingListView(context)
              :
              O2UI.noResultView(context)
            ),
          ),
        );
      },
    );
  }


  Widget _meetingListView(BuildContext context) {
    return Obx(() => ListView.builder(
        itemCount: controller.state.meetingList.length,
        itemBuilder: ((context, index) {
          final item = controller.state.meetingList[index];
          final time =
              '${item.startTime?.substring(11, 16)} - ${item.completedTime?.substring(11, 16)}';
          final members =
              item.inviteMemberList?.map((e) => e.o2NameCut()).join(',') ??
                  '';
          final status = item.status;
          var statusText = 'meeting_status_completed'.tr;
          var statusColor = AppColor.meetingCompletedColor;
          if (status == 'wait') {
            statusText = 'meeting_status_wait'.tr;
            statusColor = AppColor.meetingWaitColor;
            if (item.myWaitAccept == true) {
              statusText = 'meeting_status_wait_accept'.tr;
              statusColor = AppColor.meetingAcceptColor;
            }
          } else if (status == 'processing') {
            statusText = 'meeting_status_processing'.tr;
            statusColor = AppColor.meetingProcessingColor;
          }
          return InkWell(
              onTap: () => _tapMeeting(item, context),
              child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16, right: 16, top: 10, bottom: 10),
                  child: Row(
                    children: [
                      Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(time),
                              const SizedBox(height: 10),
                              Text(item.subject ?? ''),
                              const SizedBox(height: 10),
                              O2UI.oneLineText(
                                  'meeting_member_label'.trArgs([members])),
                            ],
                          )),
                      Expanded(
                          flex: 1,
                          child: SizedBox(
                              height: 72,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Container(
                                    height: 24,
                                    width: 64,
                                    decoration: BoxDecoration(
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(12)),
                                        color: statusColor),
                                    child: Center(
                                        child: Text(
                                      statusText,
                                      style: const TextStyle(
                                          color: Colors.white, fontSize: 12),
                                    )),
                                  ),
                                  const Spacer(),
                                  O2UI.oneLineText(item.woRoom?.name ?? ''),
                                ],
                              )))
                    ],
                  )));
        })));
  }

  void _tapMeeting(MeetingInfoModel info, BuildContext context) {
    final status = info.status;
    List<Widget> menus = [];
    if (status == 'wait' && info.myWaitAccept == true) {
        // 添加 接受邀请、拒绝邀请
        menus.add(ListTile(
          onTap: () {
            Navigator.of(context).pop();
            controller.acceptInviteMeeting(info);
          },
          leading: const Icon(Icons.check),
          title: Text('meeting_menu_invited_accept_label'.tr,
              style: Theme.of(context).textTheme.bodyMedium),
        ));
        menus.add(ListTile(
          onTap: () {
            Navigator.of(context).pop();
            controller.rejectInviteMeeting(info);
          },
          leading: const Icon(Icons.close),
          title: Text('meeting_menu_invited_reject_label'.tr,
              style: Theme.of(context).textTheme.bodyMedium),
        ));
    }
    if (menus.isNotEmpty) {
      menus.add(ListTile(
        onTap: () {
          Navigator.of(context).pop();
          controller.openMeetingView(info);
        },
        leading: const Icon(Icons.remove_red_eye),
        title: Text('meeting_menu_view_label'.tr,
            style: Theme.of(context).textTheme.bodyMedium),
      ));
      O2UI.showBottomSheetWithCancel(context, menus);
    } else {
      controller.openMeetingView(info);
    }
  }
}
