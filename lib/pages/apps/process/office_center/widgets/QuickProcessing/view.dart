import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

import '../../../../../../common/models/index.dart';
import '../../../../../../common/routers/index.dart';
import '../../../../../../common/utils/index.dart';
import '../../../../../../common/widgets/index.dart';
import 'index.dart';

/// 流程待办批量快速处理
class QuickprocessingPage extends GetView<QuickprocessingController> {
  const QuickprocessingPage({Key? key}) : super(key: key);

  /// 打开快速处理页面
  static Future<dynamic> startQuickProcess(List<TaskData> taskList) async {
    if (taskList.isEmpty) {
      Loading.toast('process_task_quick_process_empty_list'.tr);
      return null;
    }
    return await Get.toNamed(O2OARoutes.appProcessQuickprocessing,
        arguments: {"taskList": taskList});
  }

  // 底部操作条
  Widget _toolBar(BuildContext context) {
    return SizedBox(
      height: 64,
      child: Row(children: [
        Expanded(child: _cancelBtn(context)),
        Expanded(child: _submitBtn(context)),
      ]),
    );
  }

  /// 表单页面
  Widget _formView(BuildContext context) {
    return ListView(children: [
      _formTextLabel(context, 'process_task_quick_process_choose_route'.tr),
      _routeChooseView(context),
      Row(
        children: [
          Expanded(
              flex: 1,
              child: _formTextLabel(
                  context, 'process_task_quick_process_input_opinion'.tr)),
          TextButton(
              onPressed: () => controller.openHandwriting(),
              child: Text('process_task_quick_process_handwriting'.tr))
        ],
      ),
      _opinionContentView(context),
      _handwritingOpinionView(context)
    ]);
  }

  /// 填写意见
  /// 如果有常用意见，在边上显示列表
  Widget _opinionContentView(BuildContext context) {
    return SizedBox(
      height: (1 / 5).sh,
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: TextField(
                autofocus: false,
                maxLines: null,
                minLines: null,
                expands: true,
                controller: controller.opinionInputController,
                decoration: InputDecoration(
                    isDense: true,
                    hintText:
                        'process_task_quick_process_input_opinion_placeholder'
                            .tr,
                    hintStyle: Theme.of(context).textTheme.bodySmall,
                    filled: true,
                    fillColor: Theme.of(context).colorScheme.background,
                    border: InputBorder.none),
                style: Theme.of(context).textTheme.bodyLarge,
                textInputAction: TextInputAction.done,
              )),
          myIdeaList(context)
        ],
      ),
    );
  }

  /// 手写意见图片展现
  Widget _handwritingOpinionView(BuildContext context) {
    return Obx(() {
      final filePath = controller.state.handwritingImagePath.value;
      final isShow = filePath != null && filePath.isNotEmpty;
      return Visibility(
        visible: isShow,
        child: SizedBox(
            height: 128,
            child: Row(
              children: [
                filePath == null ? Container() : Image.file(File(filePath)),
                Expanded(
                  flex: 1,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                        icon: const Icon(Icons.delete),
                        onPressed: () => controller.deleteHandwritingFile()),
                  ),
                )
              ],
            )),
      );
    });
  }

  /// 常用意见
  Widget myIdeaList(BuildContext context) {
    return Obx(() => Visibility(
          visible: controller.state.ideaList.isNotEmpty,
          child: SizedBox(
            width: (1 / 3).sw,
            child: ListView(
              children: controller.state.ideaList
                  .map((element) => ListTile(
                        onTap: () => controller.addOpinion(element),
                        visualDensity:
                            const VisualDensity(horizontal: 0, vertical: -4),
                        minVerticalPadding: 1,
                        title: Text('• $element',
                            style: Theme.of(context).textTheme.bodySmall),
                      ))
                  .toList(),
            ),
          ),
        ));
  }

  /// 路由选择
  Widget _routeChooseView(BuildContext context) {
    return Obx(() => Container(
        color: Theme.of(context).colorScheme.background,
        padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
        child: Column(
            children: (controller.state.task.value?.routeNameList ?? [])
                .map((element) => ListTile(
                      onTap: () => controller.chooseRoute(element),
                      leading: SizedBox(
                        width: 22,
                        height: 22,
                        child: controller.state.routeName == element
                            ? Icon(Icons.check_circle_outline_outlined,
                                color: Theme.of(context).colorScheme.primary)
                            : Icon(Icons.circle_outlined,
                                color: Theme.of(context)
                                    .textTheme
                                    .bodySmall
                                    ?.color),
                      ),
                      title: Text(element),
                    ))
                .toList())));
  }

  /// 标题
  Widget _formTextLabel(BuildContext context, String text) {
    return Padding(
        padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
        child: Text(text, style: Theme.of(context).textTheme.titleSmall));
  }

  /// 取消按钮
  Widget _cancelBtn(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 0),
      child: SizedBox(
          width: double.infinity,
          height: 44,
          child: O2ElevatedButton(
              () => controller.cancel(),
              Text(
                'cancel'.tr,
                style: Theme.of(context).textTheme.bodyLarge,
              ),
              isPrimaryBackgroundColor: false)),
    );
  }

  /// 提交按钮
  Widget _submitBtn(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 15),
      child: SizedBox(
          width: double.infinity,
          height: 44,
          child: O2ElevatedButton(
              () => controller.submitProcess(),
              Text(
                'process_task_quick_process_submit'.tr,
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge
                    ?.copyWith(color: Colors.white),
              ))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<QuickprocessingController>(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(title: Text('process_task_quick_process'.tr)),
          body: SafeArea(
            child: Container(
              color: Theme.of(context).scaffoldBackgroundColor,
              child: Column(children: [
                Expanded(flex: 1, child: _formView(context)),
                _toolBar(context)
              ]),
            ),
          ),
        );
      },
    );
  }
}
