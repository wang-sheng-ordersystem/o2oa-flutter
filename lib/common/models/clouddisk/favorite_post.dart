class CloudDiskFavoritePost{
    String? name;
    String? folder;
    String? orderNumber; //orderNumber:排序号,升序排列,为空在最后

  CloudDiskFavoritePost({this.name, this.folder, this.orderNumber});


  Map<String, dynamic> toJson() => {
    "name": name,
    "folder": folder,
    "orderNumber": orderNumber,
  };
}