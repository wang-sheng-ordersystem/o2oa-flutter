import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:photo_view/photo_view.dart';

import '../../../common/routers/index.dart';
import '../../../common/style/index.dart';
import '../../../common/utils/index.dart';
import '../../../common/widgets/o2_stateless_widget.dart';
import 'index.dart';

class PreviewImagePage extends GetView<PreviewImageController> {
  const PreviewImagePage({Key? key}) : super(key: key);


  static void open(String path, String fileName) async {
    if (path.isEmpty) {
      Loading.toast('preview_file_path_not_empty'.tr);
      return;
    }
    if (fileName.isEmpty) {
      Loading.toast('preview_file_file_name_not_empty'.tr);
      return;
    }
    try {
      // 判断是否支持文件预览
      ['.png', '.jepg', '.jpg'].firstWhere((element) => path.toLowerCase().endsWith(element));
      await Get.toNamed(O2OARoutes.previewImage, arguments: {"path": path, "fileName": fileName});
    } catch(e) {
      OLogger.e("不支持的文件格式", e);
      Loading.toast('preview_file_not_support_file_type'.tr);
    }
    
  }
   

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PreviewImageController>(
      builder: (_) {
        return Scaffold(
            body: Container(
            constraints: BoxConstraints.expand(
                height: MediaQuery.of(context).size.height,
            ),
            child: Stack(
                children: <Widget>[
                    Positioned(
                        top: 0,
                        left: 0,
                        bottom: 0,
                        right: 0,
                        child: Obx(() => controller.state.path.isEmpty ?  
                        const CircularProgressIndicator()
                        :
                        PhotoView(
                            imageProvider: FileImage(File(controller.state.path))
                        )),
                    ),
                    Positioned( // 关闭按钮
                        left: 10.w,
                        top: MediaQuery.of(context).padding.top,
                        child: IconButton(
                            icon: const Icon(Icons.close,size: 30,color: Colors.white,),
                            onPressed: (){
                                Get.back();
                            },
                        ),
                    ),
                    Positioned(
                      //  下载图片到相册的按钮
                      right: 10.w,
                      bottom: 10.w,
                      child: O2UI.blackTransparentCard(
                          TextButton(
                            onPressed: () => controller.saveToAlbum(),
                            child: Text(
                                'cloud_disk_save_image_album_btn_title'.tr,
                                style: AppTheme.whitePrimaryTextStyle),
                          ),
                          width: 96.w,
                          height: 36.w),
                    )
                ],
            ),
        ),
        );
      },
    );
  }
}
